<?php

use Phpmig\Migration\Migration;

class HousingEstate extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `housing_estate` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NOT NULL DEFAULT '' comment '小区名称',
              `communityId` INT(10) NOT NULL DEFAULT '0' comment '社区id',
              `address` VARCHAR(255) NOT NULL DEFAULT '' comment '地址',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '小区/住宅区表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `housing_estate`;
        ');
    }
}
