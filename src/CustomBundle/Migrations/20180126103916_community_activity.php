<?php

use Phpmig\Migration\Migration;

class CommunityActivity extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `community_activity` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '创建人',
              `communityId` INT(10) NOT NULL DEFAULT '0' comment '所属社区',
              `type` VARCHAR(255) NOT NULL DEFAULT '' comment '活动类别',
              `title` VARCHAR(255) NOT NULL DEFAULT '' comment '活动主题',
              `content` text comment '活动描述',
              `remark` text comment '活动内容',
              `invited` text comment '被邀请人',
              `site` VARCHAR(255) NOT NULL DEFAULT '' comment '活动地点',
              `participant` TEXT comment '活动实际参与人',
              `startTime` INT(10) NOT NULL DEFAULT '0' comment '开始时间',
              `endTime` INT(10) NOT NULL DEFAULT '0' comment '结束时间',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '社区活动表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `community_activity`;
        ');
    }
}
