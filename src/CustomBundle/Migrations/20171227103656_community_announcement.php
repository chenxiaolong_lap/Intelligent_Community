<?php

use Phpmig\Migration\Migration;

class CommunityAnnouncement extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `community_announcement` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '公告创建人',
              `title` VARCHAR(255) NOT NULL DEFAULT '' comment '标题',
              `communityId` INT(10) NOT NULL DEFAULT '0' comment '所属社区',
              `targetType` VARCHAR(255) NOT NULL DEFAULT '' comment '公告类别',
              `startTime` INT(10) NOT NULL DEFAULT '0' comment '公告开始时间',
              `endTime` INT(10) NOT NULL DEFAULT '0' comment '公告结束时间',
              `body` TEXT  comment '公告内容',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '社区公告表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `community_announcement`;
        ');
    }
}
