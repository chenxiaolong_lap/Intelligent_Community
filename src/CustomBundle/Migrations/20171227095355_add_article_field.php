<?php

use Phpmig\Migration\Migration;

class AddArticleField extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];

        if (!$this->isFieldExist('article', 'communityId')) {
            $connection->exec("ALTER TABLE `article` ADD COLUMN `communityId` INT(10) NOT NULL default '0' COMMENT '所属社区资讯 id为0时默认为全国地区资讯' AFTER `userId`");
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }

    protected function isFieldExist($table, $filedName)
    {
        $biz = $this->getContainer();

        $sql = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $biz['db']->fetchAssoc($sql);

        return empty($result) ? false : true;
    }
}
