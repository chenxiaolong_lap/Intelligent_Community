<?php

use Phpmig\Migration\Migration;

class AddUserField extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];

        if (!$this->isFieldExist('user', 'communityRole')) {
            $connection->exec("ALTER TABLE `user` ADD COLUMN `communityRole` VARCHAR(255) NOT NULL default '|COMMUNITY_USER|' COMMENT 'COMMUNITY_LEADER(社区领导)，COMMUNITY_MANAGE(社区管理者)' AFTER `roles`");
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }

    protected function isFieldExist($table, $filedName)
    {
        $biz = $this->getContainer();

        $sql = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $biz['db']->fetchAssoc($sql);

        return empty($result) ? false : true;
    }
}
