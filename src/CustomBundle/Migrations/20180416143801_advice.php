<?php

use Phpmig\Migration\Migration;

class Advice extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `advice` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '创建人',
              `communityId` INT (10) NOT NULL DEFAULT '0' comment '建议社区id',
              `content` text comment '建议内容',
              `status` enum('saw','nosee') NOT NULL DEFAULT 'nosee' comment '查看状态',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '社区建议表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `advice`;
        ');
    }
}
