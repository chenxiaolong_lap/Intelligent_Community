<?php

use Phpmig\Migration\Migration;

class ClinicReservationRecord extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `clinic_reservation_record` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '预约人',
              `clinicId` INT (10) NOT NULL DEFAULT '0' comment '预约诊所id',
              `doctorIds` tinytext comment '接诊医生',
              `nurseIds` tinytext comment '护士',
              `status` enum('waiting','underway','finsh') DEFAULT 'waiting' comment '状态',
              `symptom` text comment '症状',
              `updateUserIds` text comment '修改人id 以顺序先后',
              `reservationTime` INT (10) unsigned NOT NULL DEFAULT '0' comment '预约时间',
              `feedback` text comment '病人反馈信息',
              `feedbackTime` INT (10) unsigned NOT NULL DEFAULT '0' comment '反馈时间',
              `estimate` text comment '病人评价',
              `estimateTime` INT (10) unsigned NOT NULL DEFAULT '0' comment '评价时间',
              `scoreId` tinyint(1) NOT NULL DEFAULT '0' comment '评分Id',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '诊所表预约记录';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `clinic_reservation_record`;
        ');
    }
}
