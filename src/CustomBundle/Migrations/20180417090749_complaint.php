<?php

use Phpmig\Migration\Migration;

class Complaint extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `complaint` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '创建人',
              `communityId` INT (10) NOT NULL DEFAULT '0' comment '建议社区id',
              `content` text comment '投诉内容',
              `status` enum('nosee','pending','processed') NOT NULL DEFAULT 'nosee' comment '处理状态',
              `result` text comment '处理结果',
              `processTime` INT(10) unsigned NOT NULL DEFAULT '0' comment '处理时间',
              `principalId` INT (10) NOT NULL DEFAULT '0' comment '负责人',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '社区投诉表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `complaint`;
        ');
    }
}
