<?php

use Phpmig\Migration\Migration;

class AddUserProfile extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];

        if (!$this->isFieldExist('user_profile', 'province')) {
            $connection->exec("ALTER TABLE `user_profile` ADD COLUMN `province` varchar(32) NOT NULL default '' COMMENT '所在省' AFTER `birthday`");
        }

        if (!$this->isFieldExist('user_profile', 'district')) {
            $connection->exec("ALTER TABLE `user_profile` ADD COLUMN `district` varchar(32) NOT NULL default '' COMMENT '所在区' AFTER `city`");
        }

        if (!$this->isFieldExist('user_profile', 'community')) {
            $connection->exec("ALTER TABLE `user_profile` ADD COLUMN `community` varchar(255) NOT NULL default '' COMMENT '所在社区' AFTER `district`");
        }

        if (!$this->isFieldExist('user_profile', 'housingEstate')) {
            $connection->exec("ALTER TABLE `user_profile` ADD COLUMN `housingEstate` varchar(255) NOT NULL default '' COMMENT '所在小区/住宅区' AFTER `community`");
        }

        if (!$this->isFieldExist('user_profile', 'address')) {
            $connection->exec("ALTER TABLE `user_profile` ADD COLUMN `address` varchar(255) NOT NULL default '' COMMENT '详细地址' AFTER `housingEstate`");
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }

    protected function isFieldExist($table, $filedName)
    {
        $biz = $this->getContainer();

        $sql = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $biz['db']->fetchAssoc($sql);

        return empty($result) ? false : true;
    }
}
