<?php

use Phpmig\Migration\Migration;

class Clinic extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `clinic` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '创建人',
              `name` VARCHAR (255) NOT NULL DEFAULT '' comment '诊所名称',
              `communityId` INT(10) NOT NULL DEFAULT '0' comment '社区id',
              `address` VARCHAR(255) NOT NULL DEFAULT '' comment '地址',
              `businessLicense` VARCHAR(255) NOT NULL DEFAULT '' comment '营业许可证',
              `legalPersonName` VARCHAR (64) NOT NULL DEFAULT '' comment '法定人姓名',
              `legalPersonIdcard` VARCHAR(255) NOT NULL DEFAULT '' comment '法定人身份证',
              `doctorIds` text comment '医生',
              `nurseIds` text comment '护士',
              `manageIds` text comment '管理人员',
              `phone` VARCHAR(64) NOT null DEFAULT '' comment '办公电话',
              `email` VARCHAR (64) NOT NULL DEFAULT '' comment '办公邮箱',
              `introduction` text comment '诊所介绍',
              `slogan` VARCHAR (255) NOT NULL DEFAULT '' comment '宣传语',
              `status` enum('unapprove','approving','approved','rejected') DEFAULT 'unapprove' comment '认证状态',
              `smallMark` VARCHAR (255) NOT NULL DEFAULT '' comment '小标志图',
              `midMark` VARCHAR (255) NOT NULL DEFAULT '' comment '中标志图',
              `largeMark` VARCHAR (255) NOT NULL DEFAULT '' comment '大标志图',
              `reason` text comment '驳回原因',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '诊所表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `clinic`;
        ');
    }
}
