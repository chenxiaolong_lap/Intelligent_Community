<?php

use Phpmig\Migration\Migration;

class Slideshow extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `community_slideshow` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '创建人',
              `communityId` INT(10) NOT NULL DEFAULT '0' comment '所属社区',
              `pictureUrl` VARCHAR(255) NOT NULL DEFAULT '' comment '图片url',
              `picturePath` VARCHAR(255) NOT NULL DEFAULT '' comment '图片路径',
              `backgroundColor` VARCHAR(255) NOT NULL DEFAULT '' comment '背景填充色',
              `displayType` varchar(32) NOT NULL default '' COMMENT 'limitWide限宽显示 tile平铺显示',
              `description` TEXT comment '图片描述',
              `serial_number` INT(10) NOT NULL DEFAULT '0' comment '轮播图序号',
              `status` tinyint(1) NOT NULL DEFAULT '1' comment '是否开启',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '社区公告表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `community_slideshow`;
        ');
    }
}
