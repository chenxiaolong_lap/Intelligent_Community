<?php

use Phpmig\Migration\Migration;

class Score extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `score` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL DEFAULT '0' comment '创建人',
              `targetType` VARCHAR (64) NOT NULL DEFAULT '' comment '评分对象类型',
              `targetId` INT(10) NOT NULL DEFAULT '0' comment '评分对象id',
              `score` INT(10) NOT NULL DEFAULT '0' comment '得分',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '评分表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `score`;
        ');
    }
}
