<?php

use Phpmig\Migration\Migration;

class Property extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `property` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NOT NULL DEFAULT '' comment '名称',
              `housingEstateId` INT(10) NOT NULL DEFAULT '0' comment '小区Id',
              `servicePhone` VARCHAR(36) NOT NULL DEFAULT '' comment '服务电话',
              `directorIds` VARCHAR(255) NOT NULL DEFAULT '{}' comment '物业管理人员{ids:[1,2]} 存放用户id json格式',
              `arrivalTime` INT(10) NOT NULL DEFAULT '0' comment '进驻时间',
              `expiryTime` INT(10) NOT NULL DEFAULT '0' comment '服务到期时间',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              `updatedTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 comment '小区物业公司表';
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `property`;
        ');
    }
}
