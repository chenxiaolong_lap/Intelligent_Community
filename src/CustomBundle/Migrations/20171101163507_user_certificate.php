<?php

use Phpmig\Migration\Migration;

class UserCertificate extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec("
            CREATE TABLE IF NOT EXISTS `certificate` (
              `id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` INT(10) NOT NULL COMMENT '用户ID',
              `certificateNumber` VARCHAR(64) NOT NULL COMMENT '证书编号',
              `degree` VARCHAR(24) COMMENT '文化程度',
              `level` VARCHAR(20)  COMMENT '级别',
              `job` varchar(20) COMMENT '职业名称',
              `theoreticalScore` float(10,1) unsigned NOT NULL DEFAULT '0.0' COMMENT '理论知识考核成绩',
              `operationalScore` float(10,1) unsigned NOT NULL DEFAULT '0.0' COMMENT '操作知识考核成绩',
              `assessmentResult` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '评定结果',
              `certificationTime` INT(10)  NOT NULL COMMENT '发证日期{查询结果转化为时间戳}',
              `institution` VARCHAR (64) COMMENT '职业技能鉴定机构名称',
              `reportUnit` VARCHAR(64)  NOT NULL COMMENT '数据上报单位',
              `reportTime` INT(10) NOT NULL COMMENT '数据上报时间',
              `createdTime` INT(10) unsigned NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              UNIQUE(`certificateNumber`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        $connection->exec('
            DROP TABLE `certificate`;
        ');
    }
}
