<?php

namespace CustomBundle\Common;


class CurlToolkit
{
    public static function sendRequest($method, $url, $headers = array(), $params = array())
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        if ('POST' == strtoupper($method)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } else {
            if (!empty($params)) {
                $url = $url.(strpos($url, '?') ? '&' : '?').http_build_query($params);
            }
        }
        curl_setopt($curl, CURLOPT_URL, $url);

        $response = curl_exec($curl);
        curl_close($curl);

        if (strpos($response, 'http')) {
            $response = self::formatData($response);
        }

        $response = json_decode($response, true);

        return $response;
    }

    private static function formatData($data)
    {
        $beforeData = strstr($data, "\"result\":", true);
        $afterData = strstr($data, "http:");
        $callback = strstr($afterData, ",\"lastUpdateTime\"", true);
        $afterCallBack = strstr($afterData, ",\"lastUpdateTime\"");

        $data = "{$beforeData}"."\"result\":"."\"{$callback}\""."{$afterCallBack}";

        return $data;
    }
}