<?php

namespace CustomBundle\Common;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class FileToolkit
{
    /*
     * 下载小文件*/
    public static function downLoadFile($fileUrl)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . basename($fileUrl));
        $response->headers->set('Content-length', filesize($fileUrl));
        $response->setContent(file_get_contents($fileUrl));

        return $response;
    }

    public static function batchDownloadFiles(array $filePathArray, $downloadFileName = null)
    {
        if (empty($filePathArray)) {
            throw new \RuntimeException('Parameter error');
        }

        if (empty($downloadFileName)) {
            $dir = './files/default/';
            $dir .= date('Y') . '/' . date('m-d');
            $fileSystem = new Filesystem();
            $fileSystem->mkdir($dir, 0755);
            $downloadFileName = $dir . '/download.zip';
        }

        $zip = new \ZipArchive();
        //新建zip压缩包
        $zip->open($downloadFileName, \ZipArchive::OVERWRITE | \ZipArchive::CREATE);
        //文件加进去压缩
        foreach ($filePathArray as $key => $filePath) {
            if (file_exists($filePath)) {
                $zip->addFile($filePath, basename($filePath));
            }
        }
        //打包zip
        $zip->close();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . basename($downloadFileName));
        $response->headers->set('Content-length', filesize($downloadFileName));
        $response->setContent(file_get_contents($downloadFileName));

        return $response;
    }

    /*
     * readfile()实现文件下载以文件流的形式*/
    public static function downloadFileByStream($fileUri)
    {
        if (empty($fileUri)) {
            throw new \RuntimeException('fileuri is null');
        }

        if (!file_exists($fileUri)) {
            throw new \RuntimeException('file not exist');
        }

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($fileUri) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fileUri));
        ob_clean();
        flush();
        readfile($fileUri);

        return;
    }
}