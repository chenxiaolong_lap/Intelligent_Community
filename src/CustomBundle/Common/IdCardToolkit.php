<?php

namespace CustomBundle\Common;

use AppBundle\Common\CurlToolkit;

class IdCardToolkit
{
    const url = 'http://api.k780.com:88/?app=idcard.get&&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json';

    ##$params = array('idcard' => num)
    ##该功能不能用于商业行为中
    public static function request($params = array())
    {
        $result = CurlToolkit::request('GET', self::url, $params, array());

        return $result;
    }

}