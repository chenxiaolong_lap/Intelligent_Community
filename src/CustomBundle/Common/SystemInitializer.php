<?php

namespace CustomBundle\Common;

use Topxia\Service\Common\ServiceKernel;
use Symfony\Component\Console\Output\OutputInterface;
use Biz\User\CurrentUser;

class SystemInitializer
{
    protected $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;

        $this->_initFile();
        $this->_initNavigations();
        $this->_initRole();
    }

    protected function _initFile()
    {
        $this->output->write('  初始化文件分组');

        $groups = $this->getFileService()->getAllFileGroups();

        foreach ($groups as $group) {
            $this->getFileService()->deleteFileGroup($group['id']);
        }

        $this->getFileService()->addFileGroup(array(
            'name' => '默认文件组',
            'code' => 'default',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '缩略图',
            'code' => 'thumb',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '课程',
            'code' => 'course',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '用户',
            'code' => 'user',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '课程私有文件',
            'code' => 'course_private',
            'public' => 0,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '资讯',
            'code' => 'article',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '临时目录',
            'code' => 'tmp',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '全局设置文件',
            'code' => 'system',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '小组',
            'code' => 'group',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '编辑区',
            'code' => 'block',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '班级',
            'code' => 'classroom',
            'public' => 1,
        ));

        $this->getFileService()->addFileGroup(array(
            'name' => '社区',
            'code' => 'community',
            'public' => 1,
        ));

        $this->output->writeln(' ...<info>成功</info>');
    }

    protected function _initNavigations()
    {
        $this->output->write('  初始化导航');

        $this->getNavigationService()->createNavigation(array(
            'name' => '常见问题',
            'url' => 'page/questions',
            'sequence' => 2,
            'isNewWin' => 0,
            'isOpen' => 1,
            'type' => 'top',
        ));

        $this->getNavigationService()->createNavigation(array(
            'name' => '关于我们',
            'url' => 'page/aboutus',
            'sequence' => 2,
            'isNewWin' => 0,
            'isOpen' => 1,
            'type' => 'top',
        ));

        $this->getNavigationService()->createNavigation(array(
            'name' => '诊所申请',
            'url' => 'clinic/apply',
            'sequence' => 2,
            'isNewWin' => 0,
            'isOpen' => 1,
            'type' => 'top',
        ));

        $this->output->writeln(' ...<info>成功</info>');
    }

    protected function _initRole()
    {
        $this->output->write('  初始化角色');
        $this->getRoleService()->refreshRoles();
        $this->output->writeln(' ...<info>成功</info>');
    }

    protected function getRoleService()
    {
        return ServiceKernel::instance()->getBiz()->service('CustomBundle:Role:RoleService');
    }

    protected function getNavigationService()
    {
        return ServiceKernel::instance()->getBiz()->service('Content:NavigationService');
    }

    private function getUserService()
    {
        return ServiceKernel::instance()->getBiz()->service('User:UserService');
    }

    private function getFileService()
    {
        return ServiceKernel::instance()->getBiz()->service('Content:FileService');
    }
}