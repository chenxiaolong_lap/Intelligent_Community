<?php

namespace CustomBundle\Common;

class CertificateToolkit
{
    const URL = 'http://wx.qddfxfpx.com/ajaxapp/ostaapi.ashx';
    const ip = '101.200.191.129';

    public static function request($params = array())
    {
        $post_data = json_encode($params, true);
        $header = array(
            'CLIENT-IP:'.self::ip,
            'X-FORWARDED-FOR:'.self::ip,
        );

        $result = CurlToolkit::sendRequest('POST', self::URL, $header, $post_data);

        return $result;
    }
}
