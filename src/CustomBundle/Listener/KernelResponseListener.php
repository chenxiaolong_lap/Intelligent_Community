<?php

namespace CustomBundle\Listener;

use AppBundle\Listener\KernelResponseListener as BaseKernelResponseListener;

class KernelResponseListener extends BaseKernelResponseListener
{
    protected function getRouteWhiteList()
    {
        return array(
            '/fill/userinfo', '/login', '/logout', '/login_check', '/register/mobile/check',
            '/register/email/check', '/login/bind/weixinmob/newset',
            '/login/bind/weixinmob/existbind', '/login/bind/weixinweb/newset',
            '/login/bind/qq/newset', '/login/bind/weibo/newset', '/login/bind/renren/newset',
            '/login/bind/qq/exist', '/login/bind/weibo/exist', '/login/bind/renren/exist',
            '/login/bind/weixinweb/exist', '/login/bind/weixinmob/exist',
            '/login/bind/weixinmob/choose', '/login/bind/weixinmob/changetoexist',
            '/login/bind/qq/new', '/login/bind/weibo/new', '/login/bind/renren/new',
            '/login/bind/weixinmob/new', '/login/bind/weixinweb/new',
            '/partner/discuz/api/notify', '/partner/phpwind/api/notify', '/partner/login', '/partner/logout',
            '/login/weixinmob', '/login/bind/weixinmob/existbind',
            '/captcha_num', '/register/captcha/check', '/edu_cloud/sms_send',
            '/edu_cloud/sms_check/sms_bind', '/settings/nickname', '/settings/nickname/check', '/settings/profile/avatar/crop/modal', 
            '/file/upload', '/file/img/crop', '/fill/userinfo/community/match', '/fill/userinfo/housing_estate/match', '/fill/userinfo/address/match',
        );
    }
}
