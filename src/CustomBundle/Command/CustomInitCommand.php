<?php

namespace CustomBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Command\BaseCommand;

class CustomInitCommand extends BaseCommand
{
    const SOURCE_SYSTEM = 'MAIN';

    protected function configure()
    {
        $this->setName('custom:init')
            ->setDescription('初始化custom配置');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<comment>初始化custom配置：</comment>");

        $this->_initJob($input, $output);

        $output->writeln("<comment>初始化成功</comment>");
    }

    protected function _initJob(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('...<info>注册证书同步定时任务</info>');
        $count = self::getSchedulerService()->countJobs(array('name' => 'SyncCertificateForcedJob', 'source' => self::SOURCE_SYSTEM));
        if ($count == 0) {
            self::getSchedulerService()->register(array(
                'name' => 'SyncCertificateForcedJob',
                'source' => self::SOURCE_SYSTEM,
                'expression' => '0 0 * * *',
                'class' => 'CustomBundle\Biz\Certificate\Job\SyncCertificateForcedJob',
                'args' => array(),
                'priority' => 100,
            ));
        }

        $count = self::getSchedulerService()->countJobs(array('name' => 'SyncCertificateJob', 'source' => self::SOURCE_SYSTEM));
        if ($count == 0) {
            self::getSchedulerService()->register(array(
                'name' => 'SyncCertificateJob',
                'source' => self::SOURCE_SYSTEM,
                'expression' => '0 3 * * *',
                'class' => 'CustomBundle\Biz\Certificate\Job\SyncCertificateJob',
                'args' => array(),
                'priority' => 100,
            ));
        }

        $output->writeln('...<info>证书同步定时任务注册成功</info>');
    }

    /**
     * @return \Codeages\Biz\Framework\Scheduler\Service\SchedulerService
     */
    private function getSchedulerService()
    {
        return $this->getServiceKernel()->createService('Scheduler:SchedulerService');
    }
}