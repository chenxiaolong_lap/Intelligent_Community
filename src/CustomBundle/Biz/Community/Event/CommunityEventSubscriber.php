<?php

namespace CustomBundle\Biz\Community\Event;

use Codeages\PluginBundle\Event\EventSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Codeages\Biz\Framework\Event\Event;

class CommunityEventSubscriber extends EventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            'community_update.user_update' => 'updateCommunityToUpdateUser',
        );
    }

    public function updateCommunityToUpdateUser(Event $event)
    {
        $arguments = $event->getSubject();

        if (empty($arguments['community'])) {
            return;
        }

        $community = $arguments['community'];
        $usersCount = $this->getUserService()->searchUserProfileCount(
            array(
                'province' => $community['province'],
                'city' => $community['city'],
                'district' => $community['district'],
                'community' => $community['name'],
            )
        );

        if (empty($usersCount)) {
            return;
        }

        $pageSize = 100;
        $totalPage = ceil($usersCount / $pageSize);
        for ($i = 1; $i <= $totalPage; $i++) {
            $users = $this->getUserService()->searchUserProfiles(
                array(
                    'province' => $community['province'],
                    'city' => $community['city'],
                    'district' => $community['district'],
                    'community' => $community['name'],
                ),
                array(),
                ($i - 1) * $pageSize,
                $pageSize
            );

            foreach ($users as $user) {
                $this->getUserService()->updateUserProfile($user['id'], $arguments['updateFields']);
            }
        }
    }

    protected function getUserService()
    {
        return $this->getBiz()->service('CustomBundle:User:UserService');

    }

    protected function getCommunityService()
    {
        return $this->getBiz()->service('CustomBundle:Community:CommunityService');
    }
}