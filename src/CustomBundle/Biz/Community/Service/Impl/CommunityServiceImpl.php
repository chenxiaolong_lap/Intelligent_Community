<?php

namespace CustomBundle\Biz\Community\Service\Impl;

use Codeages\Biz\Framework\Event\Event;
use CustomBundle\Biz\Community\Service\CommunityService;
use AppBundle\Common\ArrayToolkit;
use Biz\BaseService;

class CommunityServiceImpl  extends BaseService implements CommunityService
{
    public function createCommunity($fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        return $this->getCommunityDao()->create($fields);
    }

    public function updateCommunity($id, $fields)
    {
        $community = $this->getCommunity($id);
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        if ((isset($fields['name']) && $fields['name'] != $community['name']) ||
            (isset($fields['province']) && $fields['province'] != $community['province']) ||
            (isset($fields['city']) && $fields['city'] != $community['city']) ||
            (isset($fields['district']) && $fields['district'] != $community['district'])) {
            $updateFields = array(
                'province' => !empty($fields['province']) ? $fields['province'] : $community['province'],
                'city' => !empty($fields['city']) ? $fields['city'] : $community['city'],
                'district' => !empty($fields['district']) ? $fields['district'] :$community['district'],
                'community' => !empty($fields['name']) ? $fields['name'] : $community['name']
            );

            $this->dispatchEvent('community_update.user_update', new Event(array(
                'community' => $community,
                'updateFields' => $updateFields
            )));
        }

        $updateCommunity = $this->getCommunityDao()->update($id, $fields);

        return $updateCommunity;
    }

    public function getCommunity($id)
    {
        return $this->getCommunityDao()->get($id);
    }

    public function deleteCommunity($id)
    {
        return $this->getCommunityDao()->delete($id);
    }

    public function countCommunity($conditions)
    {
        return $this->getCommunityDao()->count($conditions);
    }

    public function searchCommunity($conditions, $orderBys, $start, $limit)
    {
        return $this->getCommunityDao()->search($conditions, $orderBys, $start, $limit);
    }

    public function getCommunityByRegionAndName($province, $city, $district, $name)
    {
        if (empty($province) || empty($name)) {
            return array();
        }

        return $this->getCommunityDao()->getByCommunityAndName($province, $city, $district, $name);
    }

    public function findCommunitiesByLikeName($name)
    {
        if (empty($name)) {
            return array();
        }

        return $this->getCommunityDao()->findByLikeName($name);
    }

    public function getCommunityByName($name)
    {
        if (empty($name)) {
            return null;
        }

        return $this->getCommunityDao()->getByName($name);
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'name',
                'province',
                'city',
                'district',
                'address',
                'officePhone'
            )
        );
    }

    protected function filterFields($fields)
    {
        return  ArrayToolkit::filter(
            $fields,
            array(
                'name' => '',
                'province' => '',
                'city' => '',
                'district' => '',
                'address' => '',
                'officePhone' => '',
            )
        );
    }

    protected function getCommunityDao()
    {
        return $this->createDao('CustomBundle:Community:CommunityDao');
    }

    protected function getUserService()
    {
        return $this->createService('User:UserService');
    }
}