<?php

namespace CustomBundle\Biz\Community\Service;

interface CommunityService
{
    public function createCommunity($fields);

    public function updateCommunity($id, $fields);

    public function getCommunity($id);

    public function deleteCommunity($id);

    public function countCommunity($conditions);

    public function searchCommunity($conditions, $orderBys, $start, $limit);

    public function getCommunityByRegionAndName($province, $city, $district, $name);

    public function findCommunitiesByLikeName($name);

    public function getCommunityByName($name);
}