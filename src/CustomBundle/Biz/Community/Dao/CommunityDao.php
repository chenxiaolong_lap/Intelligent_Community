<?php

namespace CustomBundle\Biz\Community\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface CommunityDao extends GeneralDaoInterface
{
    public function getByCommunityAndName($province, $city, $district, $name);

    public function findByLikeName($name);

    public function getByName($name);
}