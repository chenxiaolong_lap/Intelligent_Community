<?php

namespace CustomBundle\Biz\Community\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\Community\Dao\CommunityDao;

class CommunityDaoImpl extends GeneralDaoImpl implements CommunityDao
{
    protected $table = 'community';

    public function getByCommunityAndName($province, $city, $district, $name)
    {
        $paramters = array(
            'province' => $province,
            'city' => $city,
            'district' => $district,
            'name' => $name,
        );

        $paramters = array_filter($paramters);

        return $this->getByFields($paramters);
    }

    public function findByLikeName($name)
    {
        $sql = "SELECT * FROM {$this->table} WHERE `name` like ? GROUP BY province,city,district,`name` LIMIT 10;";

        return $this->db()->fetchAll($sql, array('%'.$name.'%'));
    }

    public function getByName($name)
    {
        return $this->getByFields(array('name' => $name));
    }

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'id = :id',
                'id IN (: ids)',
                'name LIKE :name',
                'province = :province',
                'city = :city',
                'district = :district',
                'officePhone LIKE :officePhone',
            )
        );
    }

}