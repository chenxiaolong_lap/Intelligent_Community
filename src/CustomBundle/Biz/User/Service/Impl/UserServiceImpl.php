<?php

namespace CustomBundle\Biz\User\Service\Impl;

use AppBundle\Common\ArrayToolkit;
use Biz\User\Service\Impl\UserServiceImpl as BaseUserServiceImpl;
use Biz\User\Service\Impl\UserSerialize;
use AppBundle\Common\SimpleValidator;
use Codeages\Biz\Framework\Event\Event;

class UserServiceImpl extends BaseUserServiceImpl
{
    public function updateUserProfile($id, $fields)
    {
        $user = $this->getUser($id);

        if (empty($user)) {
            throw $this->createNotFoundException('user not found');
        }

        $fields = ArrayToolkit::filter($fields, array(
            'truename' => '',
            'gender' => 'secret',
            'iam' => '',
            'idcard' => '',
            'birthday' => null,
            'city' => '',
            'mobile' => '',
            'qq' => '',
            'school' => '',
            'class' => '',
            'company' => '',
            'job' => '',
            'signature' => '',
            'title' => '',
            'about' => '',
            'weibo' => '',
            'weixin' => '',
            'site' => '',
            'isWeiboPublic' => '',
            'isWeixinPublic' => '',
            'isQQPublic' => '',
            'intField1' => null,
            'intField2' => null,
            'intField3' => null,
            'intField4' => null,
            'intField5' => null,
            'dateField1' => null,
            'dateField2' => null,
            'dateField3' => null,
            'dateField4' => null,
            'dateField5' => null,
            'floatField1' => null,
            'floatField2' => null,
            'floatField3' => null,
            'floatField4' => null,
            'floatField5' => null,
            'textField1' => '',
            'textField2' => '',
            'textField3' => '',
            'textField4' => '',
            'textField5' => '',
            'textField6' => '',
            'textField7' => '',
            'textField8' => '',
            'textField9' => '',
            'textField10' => '',
            'varcharField1' => '',
            'varcharField2' => '',
            'varcharField3' => '',
            'varcharField4' => '',
            'varcharField5' => '',
            'varcharField6' => '',
            'varcharField7' => '',
            'varcharField8' => '',
            'varcharField9' => '',
            'varcharField10' => '',
            'province' => '',
            'district' => '',
            'community' => '',
            'housingEstate' => '',
            'address' => '',
        ));

        if (empty($fields)) {
            return $this->getProfileDao()->get($id);
        }

        if (!empty($fields['community'])) {
            if (strstr($fields['community'], '/')){
                $communityRegion = explode('/', strstr($fields['community'], '--', true));
                $fields['community'] = substr(strstr($fields['community'], '--'), 2);
            }
        }

        if (isset($fields['title'])) {
            $this->getUserDao()->update($id, array('title' => $fields['title']));
            $this->dispatchEvent('user.update', new Event(array('user' => $user, 'fields' => $fields)));
        }

        unset($fields['title']);

        if (!empty($fields['gender']) && !in_array($fields['gender'], array('male', 'female', 'secret'))) {
            throw $this->createInvalidArgumentException('Invalid Gender');
        }

        if (!empty($fields['birthday']) && !SimpleValidator::date($fields['birthday'])) {
            throw $this->createInvalidArgumentException('Invalid Birthday');
        }

        if (!empty($fields['mobile']) && !SimpleValidator::mobile($fields['mobile'])) {
            throw $this->createInvalidArgumentException('Invalid Mobile');
        }

        if (!empty($fields['qq']) && !SimpleValidator::qq($fields['qq'])) {
            throw $this->createInvalidArgumentException('Invalid QQ');
        }

        if (!empty($fields['about'])) {
            $currentUser = $this->biz['user'];
            $trusted = $currentUser->isAdmin();
            $fields['about'] = $this->purifyHtml($fields['about'], $trusted);
        }

        if (!empty($fields['site']) && !SimpleValidator::site($fields['site'])) {
            throw $this->createInvalidArgumentException('个人空间不正确，更新用户失败');
        }
        if (!empty($fields['weibo']) && !SimpleValidator::site($fields['weibo'])) {
            throw $this->createInvalidArgumentException('微博地址不正确，更新用户失败');
        }
        if (!empty($fields['blog']) && !SimpleValidator::site($fields['blog'])) {
            throw $this->createInvalidArgumentException('地址不正确，更新用户失败');
        }

        if (empty($fields['isWeiboPublic'])) {
            $fields['isWeiboPublic'] = 0;
        } else {
            $fields['isWeiboPublic'] = 1;
        }

        if (empty($fields['isWeixinPublic'])) {
            $fields['isWeixinPublic'] = 0;
        } else {
            $fields['isWeixinPublic'] = 1;
        }

        if (empty($fields['isQQPublic'])) {
            $fields['isQQPublic'] = 0;
        } else {
            $fields['isQQPublic'] = 1;
        }

        foreach ($fields as $key => $value) {
            if (in_array($key, array('city', 'district')) || $value === 0) {
                continue;
            }

            if (empty($value)) {
                unset($fields[$key]);
            }
        }

        $userProfile = $this->getProfileDao()->update($id, $fields);

        //社区创建
        $community = $this->getCommunityService()->getCommunityByRegionAndName($userProfile['province'], $userProfile['city'], $userProfile['district'], $userProfile['community']);

        if (empty($community)) {
            $community = $this->getCommunityService()->createCommunity(array(
                'name' => $userProfile['community'],
                'province' => $userProfile['province'],
                'city' => $userProfile['city'],
                'district' => $userProfile['district']
            ));
        }
        //小区创建
        $housingEstate = $this->getHousingEstateService()->getHousingEstateByCommunityIdAndName($community['id'], $userProfile['housingEstate']);

        if (empty($housingEstate)) {
            $address = $userProfile['province'].$userProfile['city'].$userProfile['district'].$userProfile['address'];
            $housingEstate = $this->getHousingEstateService()->createHousingEstate(array(
                'name' => $userProfile['housingEstate'],
                'communityId' => $community['id'],
                'address' => $address,
            ));
        }

        $this->dispatchEvent('profile.update', new Event(array('user' => $user, 'fields' => $fields)));

        return $userProfile;
    }

    public function matchUsersByNicknameOrTruename($query)
    {
        if (empty($query)) {
            return array();
        }

        return $this->getUserDao()->matchByNicknameOrTruename($query);
    }

    public function findAddressesByLikeAddress($address)
    {
        if (empty($address)) {
            return array();
        }

        return $this->getProfileDao()->findAddressesByLikeAddress($address);
    }

    public function findUsersByLikeNickname($nickname)
    {
        if (empty($nickname)) {
            return array();
        }

        return $this->getUserDao()->findByLikeNickname($nickname);
    }

    public function findUserByLikeTruename($name)
    {
        if (empty($name)) {
            return array();
        }

        return $this->getProfileDao()->findByLikeTruename($name);
    }

    public function setCommunityRole($id, $communityRoles)
    {
        $user = $this->getUser($id);

        if (empty($user)) {
            throw $this->createNotFoundException("User#{$id} Not Found");
        }

        $currentUser = $this->getCurrentUser();

        if (!in_array('ROLE_SUPER_ADMIN', $currentUser['roles']) && !in_array('ROLE_ADMIN', $currentUser['roles']) && !in_array('COMMUNITY_MANAGE', $currentUser['communityRole'])) {
            throw $this->createInvalidArgumentException('Invalid Roles');
        }

        $user = $this->getUserDao()->update($id, array('communityRole' => $communityRoles));

        return $user;
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getHousingEstateService()
    {
        return $this->createService('CustomBundle:HousingEstate:HousingEstateService');
    }

    protected function getUserDao()
    {
        return $this->createDao('CustomBundle:User:UserDao');
    }
}