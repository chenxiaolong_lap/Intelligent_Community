<?php

namespace CustomBundle\Biz\User\Dao\Impl;

use Biz\User\Dao\Impl\UserProfileDaoImpl as BaseUserProfileDaoImpl;

class UserProfileDaoImpl extends BaseUserProfileDaoImpl
{
    public function findAddressesByLikeAddress($address)
    {
        $sql = "SELECT * FROM {$this->table} WHERE address like ? GROUP BY address LIMIT 10";

        return $this->db()->fetchAll($sql, array('%'.$address.'%'));
    }

    public function findByLikeTruename($name)
    {
        if (empty($name)) {
            return array();
        }

        $sql = "SELECT * FROM {$this->table} WHERE truename like ? ";

        return $this->db()->fetchAll($sql, array('%'.$name.'%'));
    }

    public function declares()
    {
        return array(
            'orderbys' => array('id'),
            'conditions' => array(
                'mobile LIKE :mobile',
                'truename LIKE :truename',
                'idcard LIKE :idcard',
                'id IN (:ids)',
                'mobile = :tel',
                'mobile <> :mobileNotEqual',
                'qq LIKE :qq',
                'province = :province',
                'city = :city',
                'district = :district',
                'community = :community',
                'housingEstate = :housingEstate',
                'community LIKE :communityLike',
                'housingEstate LIKE :housingEstateLike',
            ),
        );
    }
}