<?php

namespace CustomBundle\Biz\User\Dao\Impl;

use Biz\User\Dao\Impl\UserDaoImpl as BaseUserDaoImpl;

class UserDaoImpl extends BaseUserDaoImpl
{
    public function findByLikeNickname($nickname)
    {
        $sql = "SELECT id,nickname FROM {$this->table} WHERE nickname LIKE ? LIMIT 10";

        return $this->db()->fetchAll($sql, array('%'.$nickname.'%'));
    }

    public function matchByNicknameOrTruename($queryString)
    {
        if (empty($queryString)) {
            return array();
        }

        $sql = "SELECT user.id as id,user.nickname as nickname, user_profile.truename as truename,user.smallAvatar as smallAvatar FROM user,user_profile WHERE user.id = user_profile.id AND (user.nickname LIKE ? OR user_profile.truename LIKE ? ) LIMIT 0,10;";

        return $this->db()->fetchAll($sql, array('%'.$queryString.'%', '%'.$queryString.'%'));
    }

    public function declares()
    {
        return array(
            'serializes' => array(
                'roles' => 'delimiter',
                'communityRole' => 'delimiter'
            ),
            'orderbys' => array(
                'id',
                'createdTime',
                'updatedTime',
                'promotedTime',
                'promoted',
                'promotedSeq',
                'nickname',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'mobile = :mobile',
                'promoted = :promoted',
                'roles LIKE :roles',
                'roles = :role',
                'UPPER(nickname) LIKE :nickname',
                'id =: id',
                'loginIp = :loginIp',
                'createdIp = :createdIp',
                'approvalStatus = :approvalStatus',
                'UPPER(email) LIKE :email',
                'level = :level',
                'createdTime >= :startTime',
                'createdTime <= :endTime',
                'updatedTime >= :updatedTime_GE',
                'approvalTime >= :startApprovalTime',
                'approvalTime <= :endApprovalTime',
                'loginTime >= :loginStartTime',
                'loginTime <= :loginEndTime',
                'locked = :locked',
                'level >= :greatLevel',
                'UPPER(verifiedMobile) LIKE :verifiedMobile',
                'type LIKE :type',
                'id IN ( :userIds)',
                'inviteCode = :inviteCode',
                'inviteCode != :NoInviteCode',
                'id NOT IN ( :excludeIds )',
                'orgCode PRE_LIKE :likeOrgCode',
                'orgCode = :orgCode',
            ),
        );
    }
}