<?php

namespace CustomBundle\Biz\Complaint\Service;

interface ComplaintService
{
    public function createComplaint($fields);

    public function updateComplaint($id, $fields);

    public function deleteComplaint($id);

    public function getComplaint($id);

    public function countComplaints($conditions);

    public function searchComplaints($conditions, $orderbys, $start, $limit);

    public function findComplaintsByCommunityId($communityId);
}