<?php

namespace CustomBundle\Biz\Complaint\Service\Impl;

use AppBundle\Common\ArrayToolkit;
use Biz\BaseService;
use CustomBundle\Biz\Complaint\Service\ComplaintService;

class ComplaintServiceImpl extends BaseService implements ComplaintService
{
    public function createComplaint($fields)
    {
        $user = $this->getCurrentUser();
        $this->partFields($fields);
        $this->filterFields($fields);

        if (empty($fields['communityId'])) {
            throw $this->createInvalidArgumentException('参数错误');
        }

        if (!isset($fields['userId'])) {
            $fields['userId'] = $user['id'];
        }

        $community = $this->getCommunityService()->getCommunity($fields['communityId']);

        if (empty($community)) {
            throw $this->createNotFoundException("没有查询到#{$fields['communityId']}的社区");
        }

        return $this->getComplaintDao()->create($fields);
    }

    public function updateComplaint($id, $fields)
    {
        $this->partFields($fields);
        $this->filterFields($fields);

        $complaint = $this->getComplaint($id);

        if (isset($fields['result'])) {
            if (empty($fields['processTime'])) {
                $fields['processTime'] = time();
            }
        }

        if (isset($fields['principalId']) && $fields['principalId'] == $complaint['userId']) {
            throw $this->createServiceException('对不起，不能处理自己提交的投诉');
        }

        return $this->getComplaintDao()->update($id, $fields);
    }

    public function getComplaint($id)
    {
        return $this->getComplaintDao()->get($id);
    }

    public function deleteComplaint($id)
    {
        if (empty($this->getComplaint($id))) {
            throw $this->createNotFoundException("没有查询到#{$id}的投诉");
        }

        return $this->getComplaintDao()->delete($id);
    }

    public function countComplaints($conditions)
    {
        return $this->getComplaintDao()->count($conditions);
    }

    public function searchComplaints($conditions, $orderbys, $start, $limit)
    {
        return $this->getComplaintDao()->search($conditions, $orderbys, $start, $limit);
    }

    public function findComplaintsByCommunityId($communityId)
    {
        if (empty($communityId)) {
            throw $this->createInvalidArgumentException('参数错误');
        }

        return $this->getComplaintDao()->findByCommunityId($communityId);
    }

    protected function partFields(&$fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'userId',
                'communityId',
                'content',
                'status',
                'result',
                'processTime',
                'principalId',
            )
        );
    }

    protected function filterFields(&$fields)
    {
        return ArrayToolkit::filter(
            $fields,
            array(
                'userId' => '0',
                'communityId' => '0',
                'content' => '',
                'status' => 'nosee',
                'result' => '',
                'processTime' => '0',
                'principalId' => '0',
            )
        );
    }

    protected function getComplaintDao()
    {
        return $this->createDao('CustomBundle:Complaint:ComplaintDao');
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}