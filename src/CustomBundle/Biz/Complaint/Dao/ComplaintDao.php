<?php

namespace CustomBundle\Biz\Complaint\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface ComplaintDao extends GeneralDaoInterface
{
    public function findByCommunityId($communityId);
}