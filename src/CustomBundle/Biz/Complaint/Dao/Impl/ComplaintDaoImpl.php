<?php

namespace CustomBundle\Biz\Complaint\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\Complaint\Dao\ComplaintDao;

class ComplaintDaoImpl extends GeneralDaoImpl implements ComplaintDao
{
    protected $table = 'complaint';

    public function findByCommunityId($communityId)
    {
        return $this->findByFields(array('communityId' => $communityId));
    }

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'id = :id',
                'id IN (: ids)',
                'userId IN (:userIds)',
                'communityId = :communityId',
                'status = :status'
            )
        );
    }
}