<?php

namespace CustomBundle\Biz\Advice\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface AdviceDao extends GeneralDaoInterface
{
    public function findByCommunityId($communityId);
}