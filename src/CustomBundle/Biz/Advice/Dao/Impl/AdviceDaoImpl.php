<?php

namespace CustomBundle\Biz\Advice\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\Advice\Dao\AdviceDao;

class AdviceDaoImpl extends GeneralDaoImpl implements AdviceDao
{
    protected $table = 'advice';

    public function findByCommunityId($communityId)
    {
        return $this->findByFields(array('communityId' => $communityId));
    }

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'id = :id',
                'id IN (: ids)',
                'userId IN (:userIds)',
                'communityId = :communityId',
                'status = :status'
            )
        );
    }
}