<?php

namespace CustomBundle\Biz\Advice\Service;


interface AdviceService
{
    public function createAdvice($fields);

    public function findAdvicesByCommunityId($communityId);

    public function searchAdvices($conditions, $orderbys, $start, $limit);

    public function countAdvices($conditions);

    public function getAdvice($id);

    public function updateAdvice($id, $fields);
}