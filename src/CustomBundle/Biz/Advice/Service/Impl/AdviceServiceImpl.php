<?php

namespace CustomBundle\Biz\Advice\Service\Impl;

use AppBundle\Common\ArrayToolkit;
use Biz\BaseService;
use CustomBundle\Biz\Advice\Service\AdviceService;

class AdviceServiceImpl extends BaseService implements AdviceService
{
    public function createAdvice($fields)
    {
        $user = $this->getCurrentUser();
        $this->partFields($fields);
        $this->filterFields($fields);

        $community = $this->getCommunityService()->getCommunity($fields['communityId']);

        if (empty($community)) {
            $community = $this->getCommunityService()->getCommunityByRegionAndName(
                $user['province'],
                $user['city'],
                $user['district'],
                $user['community']
            );
        }

        return $this->getAdviceDao()->create($fields);
    }

    public function findAdvicesByCommunityId($communityId)
    {
        $user = $this->getCurrentUser();

        $community = $this->getCommunityService()->getCommunity($communityId);

        if (empty($community)) {
            $community = $this->getCommunityService()->getCommunityByRegionAndName(
                $user['province'],
                $user['city'],
                $user['district'],
                $user['community']
            );
        }

        return $this->getAdviceDao()->findByCommunityId($community['id']);
    }

    public function searchAdvices($conditions, $orderbys, $start, $limit)
    {
        return $this->getAdviceDao()->search($conditions, $orderbys, $start, $limit);
    }

    public function countAdvices($conditions)
    {
        return $this->getAdviceDao()->count($conditions);
    }

    public function getAdvice($id)
    {
        if (empty($id)) {
            return null;
        }

        return $this->getAdviceDao()->get($id);
    }

    public function updateAdvice($id, $fields)
    {
        $this->partFields($fields);
        $this->filterFields($fields);

        return $this->getAdviceDao()->update($id, $fields);
    }

    public function partFields(&$fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'userId',
                'communityId',
                'content',
                'status',
            )
        );
    }

    public function filterFields(&$fields)
    {
        return ArrayToolkit::filter(
            $fields,
            array(
                'userId' => '0',
                'communityId' => '0',
                'content' => '',
                'status' => 'nosee',
            )
        );
    }

    protected function getAdviceDao()
    {
        return $this->createDao('CustomBundle:Advice:AdviceDao');
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}