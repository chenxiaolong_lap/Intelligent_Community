<?php

namespace CustomBundle\Biz;

use CustomBundle\Biz\Import\CommunityUsersImporter;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CustomServiceProvider implements ServiceProviderInterface
{
    public function register(Container $biz)
    {
        $biz['importer.community-users'] = function ($biz) {
            return new CommunityUsersImporter($biz);
        };
    }
}
