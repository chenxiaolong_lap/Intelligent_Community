<?php

namespace CustomBundle\Biz\Article\Service\Impl;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\SimpleValidator;
use Biz\Article\Service\Impl\ArticleServiceImpl as BaseArticleServiceImpl;

class ArticleServiceImpl extends BaseArticleServiceImpl
{
    protected function filterArticleFields($fields, $mode = 'update')
    {
        $user = $this->getCurrentUser();
        $match = preg_match('/<\s*img.+?src\s*=\s*[\"|\'](.*?)[\"|\']/i', $fields['body'], $matches);
        $article['picture'] = $match ? $matches[1] : '';

        $article['thumb'] = $fields['thumb'];
        $article['originalThumb'] = $fields['originalThumb'];
        $article['title'] = $this->purifyHtml($fields['title']);
        $article['body'] = $this->purifyHtml($fields['body'], true);
        $article['featured'] = empty($fields['featured']) ? 0 : 1;
        $article['promoted'] = empty($fields['promoted']) ? 0 : 1;
        $article['sticky'] = empty($fields['sticky']) ? 0 : 1;

        $article['categoryId'] = $fields['categoryId'];
        $article['source'] = $this->purifyHtml($fields['source']);
        $article['sourceUrl'] = empty($fields['sourceUrl']) ? '' : $fields['sourceUrl'];
        $article['publishedTime'] = strtotime($fields['publishedTime']);

        if (!empty($article['sourceUrl']) && !SimpleValidator::site($article['sourceUrl'])) {
            throw $this->createInvalidArgumentException('来源地址不正确！');
        }

        if (empty($fields['body'])) {
            throw $this->createServiceException('资讯内容不能为空');
        }

        $fields = $this->fillOrgId($fields);
        if (isset($fields['orgId'])) {
            $article['orgCode'] = $fields['orgCode'];
            $article['orgId'] = $fields['orgId'];
        }
        if (!empty($fields['tags']) && !is_array($fields['tags'])) {
            $fields['tags'] = explode(',', $fields['tags']);
            $article['tagIds'] = ArrayToolkit::column($this->getTagService()->findTagsByNames($fields['tags']), 'id');
        } else {
            $article['tagIds'] = array();
        }

        if (isset($fields['communityId'])) {
            $article['communityId'] = $fields['communityId'];
        }

        if ($mode == 'add') {
            $article['status'] = 'published';
            $article['userId'] = $user->id;
        }

        return $article;
    }
}