<?php

namespace CustomBundle\Biz\Article\Dao\Impl;

use Biz\Article\Dao\Impl\ArticleDaoImpl as BaseArticleDaoImpl;

class ArticleDaoImpl extends BaseArticleDaoImpl
{
    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
                'publishedTime',
                'sticky',
                'hits',
                'updatedTime',
            ),
            'serializes' => array(
                'tagIds' => 'delimiter',
            ),
            'timestamps' => array('createdTime', 'updatedTime'),
            'conditions' => array(
                'status = :status',
                'id IN (:articleIds)',
                'categoryId = :categoryId',
                'featured = :featured',
                'promoted = :promoted',
                'sticky = :sticky',
                'title LIKE :keywords',
                'picture != :pictureNull',
                'updatedTime >= :updatedTime_GE',
                'categoryId IN (:categoryIds)',
                'orgCode PRE_LIKE :likeOrgCode',
                'id != :idNotEqual',
                'id = :articleId',
                'thumb != :thumbNotEqual',
                'orgCode = :orgCode',
                'communityId = :communityId'
            ),
            'serializes' => array(
                'tagIds' => 'delimiter',
            ),
        );
    }
}