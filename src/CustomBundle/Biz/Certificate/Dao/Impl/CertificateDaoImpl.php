<?php

namespace CustomBundle\Biz\Certificate\Dao\Impl;

use CustomBundle\Biz\Certificate\Dao\CertificateDao;
use Codeages\Biz\Framework\Dao\GeneralDaoImpl;

class CertificateDaoImpl extends GeneralDaoImpl implements CertificateDao
{
    protected $table = 'certificate';

    public function getByCertificateNumberAndUserId($certificateNumber, $userId)
    {
        return $this->getByFields(array('certificateNumber' => $certificateNumber, 'userId' => $userId));
    }

    public function findByUserId($userId)
    {
        return $this->findByFields(array('userId' => $userId));
    }

    public function findCountByUserIds(array $userIds)
    {
        if (empty($userIds)) {
            return array();
        }

        $marks = str_repeat('?,', count($userIds) - 1).'?';
        $sql = "SELECT userId,COUNT(*) AS count FROM {$this->table()} WHERE userId IN ({$marks}) group by userId;";

        return $this->db()->fetchAll($sql, $userIds);
    }

    public function findByUserIds($userIds)
    {
        if (empty($userIds)) {
            return array();
        }

        return $this->findInField('userId', $userIds);
    }

    public function countByJob($job)
    {
        $sql = "SELECT COUNT(DISTINCT(userId)) AS count FROM {$this->table()} WHERE job = ?";

        return $this->db()->fetchColumn($sql, array($job));
    }

    public function countByConditions($conditions)
    {
        $sql = "SELECT COUNT(DISTINCT(c.userId)) AS count FROM user_profile u,certificate c WHERE u.id = c.userId ";

        $paramters = array();

        if (!empty($conditions['province'])) {
            $sql      .= " AND u.province = ? ";
            $paramters = array_merge($paramters, array($conditions['province']));
        }

        if (!empty($conditions['city'])) {
            $sql      .= " AND u.city = ? ";
            $paramters = array_merge($paramters, array($conditions['city']));
        }

        if (!empty($conditions['district'])) {
            $sql      .= " AND u.district = ? ";
            $paramters = array_merge($paramters, array($conditions['district']));
        }

        if (!empty($conditions['job'])) {
            $sql      .= " AND c.job = ? ";
            $paramters = array_merge($paramters, array($conditions['job']));
        }

        return $this->db()->fetchColumn($sql, $paramters);
    }

    public function declares()
    {
        return array(
            'orderbys' => array('id'),
            'timestamps' => array(
                'createdTime',
            ),
            'conditions' => array(
                'id IN ( :ids)',
                'userId IN ( :userIds)',
                'job = :job',
                'job LIKE :jobLike',
                'certificateNumber LIKE :certificateNumber',
                'createdTime >= :startTime',
                'createdTime <= :endTime',
            ),
        );
    }
}
