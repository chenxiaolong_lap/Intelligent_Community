<?php

namespace CustomBundle\Biz\Certificate\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;
interface CertificateDao extends GeneralDaoInterface
{
    public function getByCertificateNumberAndUserId($certificateNumber, $userId);

    public function findByUserId($userId);

    public function findCountByUserIds(array $userIds);

    public function findByUserIds($userIds);

    public function countByJob($job);

    public function countByConditions($conditions);
}
