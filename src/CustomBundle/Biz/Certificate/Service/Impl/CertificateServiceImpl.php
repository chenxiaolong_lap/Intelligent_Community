<?php

namespace CustomBundle\Biz\Certificate\Service\Impl;

use CustomBundle\Biz\Certificate\Service\CertificateService;
use Biz\BaseService;
use AppBundle\Common\ArrayToolkit;
use CustomBundle\Common\CertificateToolkit;

class CertificateServiceImpl extends BaseService implements CertificateService
{
    public function getCertificate($id)
    {
        return $this->getCertificateDao()->get($id);
    }

    public function createCertificate($fields)
    {
        $fields = ArrayToolkit::parts(
            $fields,
            array(
                'userId',
                'certificateNumber',
                'degree',
                'level',
                'job',
                'theoreticalScore',
                'operationalScore',
                'assessmentResult',
                'certificationTime',
                'institution',
                'reportUnit',
                'reportTime',
            )
        );

        return $this->getCertificateDao()->create($fields);
    }

    public function getCertificateByCertificateNumberAndUserId($certificateNumber, $userId)
    {
        return $this->getCertificateDao()->getByCertificateNumberAndUserId($certificateNumber, $userId);
    }

    public function findCertificatesByUserId($userId)
    {
        return $this->getCertificateDao()->findByUserId($userId);
    }

    public function findCountCertificateByUserIds($userIds)
    {
        return $this->getCertificateDao()->findCountByUserIds($userIds);
    }

    public function findCertificatesByUserIds($userIds){
        return $this->getCertificateDao()->findByUserIds($userIds);
    }

    public function countCertificate($conditions)
    {
        return $this->getCertificateDao()->count($conditions);
    }

    public function searchCertificates($conditions, $orderBys, $start, $limit)
    {
        return $this->getCertificateDao()->search($conditions, $orderBys, $start, $limit);
    }

    public function countByJob($job)
    {
        return $this->getCertificateDao()->countByJob($job);
    }

    public function countByConditions($conditions)
    {
        if (empty($conditions['job'])) {
            return 0;
        }
        
        $conditions = ArrayToolkit::parts($conditions, array(
            'province',
            'city',
            'district',
            'job'
        ));

        return $this->getCertificateDao()->countByConditions($conditions);
    }

    public function syncCertificates($userId, $certificates)
    {
        if (empty($certificates)) {
            return;
        }

        $profile = $this->getUserService()->getUserProfile($userId);

        foreach ($certificates as $certificate) {
            $fields      = $this->conversionFields($certificate);
            $fields      = array_merge($fields, array('userId' => $userId));

            $certificate = $this->getCertificateByCertificateNumberAndUserId(
                $fields['certificateNumber'], 
                $profile['id']
            );

            if (empty($certificate)) {
                $this->createCertificate($fields);
            }
        }

        $jobs = ArrayToolkit::column($certificates, '职业名称');

        if (empty($profile['certificated'])) {
            $profile['certificated']     = 1;
            $profile['certificatedTime'] = time();
        }

        if (in_array('建（构）筑物消防员', $jobs)) {
            $profile['fireCertificated'] = 1;
        }

        $this->getUserService()->updateUserProfile($profile['id'], $profile);
    }

    protected function conversionFields($fields)
    {
        $templateFields = array();

        $templateFields['certificateNumber'] = $fields['证书编号'];
        $templateFields['degree'] = !empty($fields['文化程度']) ? $fields['文化程度'] : '';
        $templateFields['level'] = !empty($fields['级别']) ? $fields['级别'] : '';
        $templateFields['job'] = !empty($fields['职业名称']) ? $fields['职业名称'] : '';
        $templateFields['theoreticalScore'] = !empty($fields['理论知识考核成绩']) ? $fields['理论知识考核成绩'] : '';
        $templateFields['operationalScore'] = !empty($fields['操作知识考核成绩']) ? $fields['操作知识考核成绩'] : '';
        $templateFields['assessmentResult'] = !empty($fields['评定成绩']) ? $fields['评定成绩'] : '';
        $templateFields['certificationTime'] = !empty($fields['发证日期']) ? strtotime($fields['发证日期']) : '0';
        $templateFields['institution'] = !empty($fields['职业技能鉴定机构名称']) ? $fields['职业技能鉴定机构名称'] : '';
        $templateFields['reportUnit'] = !empty($fields['数据上报单位']) ? $fields['数据上报单位'] : '';
        $templateFields['reportTime'] = !empty($fields['数据上报时间']) ? strtotime($fields['数据上报时间']): '0';

        return $templateFields;
    }

    public function obtainCertificate($conditions)
    {
        $params = $this->makeConditions($conditions);
        $params = ArrayToolkit::parts($params, array('cid', 'name', 'callback'));

        $result = CertificateToolkit::request($params);

        if ($result['errcode'] == 0) {
            if ($result['msg'] != 'ok') {
                $response = array('status' => 'warn', 'message' => $result['msg']);
            } elseif (!empty($result['result']) && !empty($result['result']['certificateList'])) {
                $this->syncCertificates($conditions['userId'], $result['result']['certificateList']);

                $response = array('status' => 'success', 'message' => '更新成功');
            } else {
                $response = array('status' => 'warn', 'message' => '未查询到该用户证书');
            }
        } else {
            $response = array('status' => 'fail', 'message' => '查询失败');
        }

        return $response;
    }

    public function makeConditions($conditions)
    {
        if (!ArrayToolkit::requireds($conditions, array('userId', 'cid', 'name'))) {
            throw $this->createInvalidArgumentException('缺少必要参数');
        }

        $token    = $this->getTokenService()->makeToken('certificate.callback', array(
            'times'    => 1,
            'userId'   => $conditions['userId'],
            'duration' => 60 * 10,
        ));

        $site = $this->getSettingService()->get('site');
        $url  = empty($site['url']) ? $site['url'] : rtrim($site['url'], ' \/');

        if (!empty($url)) {
            $callback = $url.$this->getRouter()->generate('certificate_callback',
                array('token' => $token['token'])
            );
        } else {
            $callback = $this->getRouter()->generate('certificate_callback',
                array('token' => $token['token']),
                true
            );
        }

        return array_merge($conditions, array('callback' => $callback));
    }

    protected function getRouter()
    {
        global $kernel;

        return $kernel->getContainer()->get('router');
    }

    protected function getSettingService()
    {
        return $this->createService('System:SettingService');
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }

    protected function getTokenService()
    {
        return $this->createService('User:TokenService');
    }

    protected function getUserService()
    {
        return $this->createService('User:UserService');
    }

    protected function getCertificateDao()
    {
        return $this->createDao('CustomBundle:Certificate:CertificateDao');
    }
}
