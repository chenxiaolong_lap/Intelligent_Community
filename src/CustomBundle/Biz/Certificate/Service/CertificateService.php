<?php

namespace CustomBundle\Biz\Certificate\Service;


interface CertificateService
{
    public function getCertificate($id);

    public function createCertificate($fields);

    public function syncCertificates($userId, $certificates);

    public function getCertificateByCertificateNumberAndUserId($certificateNumber, $userId);

    public function findCertificatesByUserId($userId);

    public function findCountCertificateByUserIds($userIds);

    public function findCertificatesByUserIds($userIds);

    public function searchCertificates($conditions, $orderBys, $start, $limit);

    public function countCertificate($conditions);

    public function countByJob($job);

    public function countByConditions($conditions);

    public function obtainCertificate($conditions);

    public function makeConditions($conditions);
}
