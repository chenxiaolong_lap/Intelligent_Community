<?php

namespace CustomBundle\Biz\Certificate\Job;

use Codeages\Biz\Framework\Scheduler\AbstractJob;
use AppBundle\Common\ExceptionPrintingToolkit;
use CustomBundle\Common\CertificateToolkit;
use AppBundle\Common\ArrayToolkit;

class SyncCertificateJob extends AbstractJob
{
    public function execute()
    {
        try {
            $page  = $this->args['page'];
            $start = $this->args['start'];
            $limit = $this->args['limit'];

            $profiles = $this->getUserService()->searchUserProfiles(
                array(),
                array('id' => 'DESC'),
                $start,
                $limit
            );

            if (empty($profiles)) {
                $this->updateJob(array('args' => array(
                    'page'  => 1,
                    'start' => 0,
                    'limit' => $limit
                )));
                return;
            } else {
                $this->updateJob(array('args' => array(
                    'page'  => $page + 1,
                    'start' => $page * $limit,
                    'limit' => $limit
              )));
            }

            foreach ($profiles as $profile) {
                if (!empty($profile['truename'] && !empty($profile['idcard']))) {
                    $params = $this->getCertificateService()->makeConditions(array(
                        'cid'    => $profile['idcard'],
                        'name'   => $profile['truename'],
                        'userId' => $profile['id'],
                    ));
                    $params = ArrayToolkit::parts($params, array(
                        'cid', 
                        'name', 
                        'callback'
                    ));

                    CertificateToolkit::request($params);
                }
            }

            $this->getLogService()->info('sync_certificate', 'scheduler', '同步职业资格证书');
        } catch (\Exception $e) {
            $this->getLogService()->error('sync_certificate', 'scheduler', '同步职业资格证书', ExceptionPrintingToolkit::printTraceAsArray($e));
        }
    }

    protected function updateJob($fields)
    {
        $jobs = $this->getJobDao()->search(
            array('name' => 'SyncCertificateJob'), 
            array(), 
            0, 
            1
        );

        foreach ($jobs as $job) {
            $this->getJobDao()->update($job['id'], $fields);
        }
    }

    protected function getJobDao()
    {
        return $this->biz->dao('Scheduler:JobDao');
    }

    protected function getCertificateService()
    {
        return $this->biz->service('CustomBundle:Certificate:CertificateService');
    }

    protected function getUserService()
    {
        return $this->biz->service('User:UserService');
    }

    private function getLogService()
    {
        return $this->biz->service('System:LogService');
    }
}
