<?php

namespace CustomBundle\Biz\Certificate\Job;

use Codeages\Biz\Framework\Scheduler\AbstractJob;
use AppBundle\Common\ExceptionPrintingToolkit;
use CustomBundle\Common\CertificateToolkit;

class SyncCertificateForcedJob extends AbstractJob
{
    public function execute()
    {
        try {
            $pageSize = 100;

            $count = $this->getUserService()->searchUserProfileCount(array());
            $totalPage = ceil($count / $pageSize);

            for ($i = 1; $i <= $totalPage; $i++) {
                $profiles = $this->getUserService()->searchUserProfiles(
                    array(),
                    array('id' => 'DESC'),
                    ($i - 1) * $pageSize,
                    $pageSize
                );
                foreach ($profiles as $profile) {
                    if (!empty($profile['truename'] && !empty($profile['idcard']))) {
                        $params = array(
                            'cid' => $profile['idcard'],
                            'name' => $profile['truename'],
                            'forced' => true,
                        );

                        CertificateToolkit::request($params);
                    }
                }
            }

            $this->getLogService()->info('sync_certificate', 'sync_certificate_forced', '强制同步职业资格证书', array());
        } catch (\Exception $e) {
            $this->getLogService()->error('sync_certificate', 'sync_certificate_forced', '强制同步职业资格证书', ExceptionPrintingToolkit::printTraceAsArray($e));
        }
    }

    protected function getUserService()
    {
        return $this->biz->service('User:UserService');
    }

    private function getLogService()
    {
        return $this->biz->service('System:LogService');
    }
}
