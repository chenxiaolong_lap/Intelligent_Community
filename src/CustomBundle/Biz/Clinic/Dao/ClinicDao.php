<?php

namespace CustomBundle\Biz\Clinic\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface ClinicDao extends GeneralDaoInterface
{
    public function findByCommunityId($communityId);
}