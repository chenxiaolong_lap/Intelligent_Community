<?php

namespace CustomBundle\Biz\Clinic\Dao\Impl;

use CustomBundle\Biz\Clinic\Dao\ClinicDao;
use Codeages\Biz\Framework\Dao\GeneralDaoImpl;

class ClinicDaoImpl extends GeneralDaoImpl implements ClinicDao
{
    protected $table = 'clinic';

    public function findByCommunityId($communityId)
    {
        if (empty($communityId)) {
            return array();
        }

        return $this->findByFields(array('communityId' => $communityId));
    }

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'serializes' => array(
                'doctorIds' => 'delimiter',
                'nurseIds' => 'delimiter',
                'manageIds' => 'delimiter',
            ),
            'conditions' => array(
                'id = :id',
                'phone LIKE :phone',
                'email LIKE :email',
                'status = :status',
                'doctorIds FIND_IN_SET (: doctorIds)',
                'name LIKE :name',
                'communityId = :communityId',
                'communityId IN (:communityIds)'
            )
        );
    }
}