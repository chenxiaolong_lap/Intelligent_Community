<?php

namespace CustomBundle\Biz\Clinic\Service;


interface ClinicService
{
    public function createClinic($fields);

    public function approveClinic($id, $status, $reason = null);

    public function changeMark($data);

    public function getClinic($id);

    public function countClinic($conditions);

    public function searchClinics($conditions, $orderBys, $start, $limit);

    public function findClinicsByCommunityId($communityId);

    public function bespeakClinic($fields, $clinicId);
}