<?php

namespace CustomBundle\Biz\Clinic\Service\Impl;

use Biz\BaseService;
use CustomBundle\Biz\Clinic\Service\ClinicService;
use AppBundle\Common\ArrayToolkit;

class ClinicServiceImpl extends BaseService implements ClinicService
{
    public function createClinic($fields)
    {
        $user = $this->getCurrentUser();
        $fields = array_merge($fields, array('userId' => $user['id']));
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        if ($user['approvalStatus'] == 'unapprove' || $user['approvalStatus'] == 'approve_fail') {
            throw $this->createAccessDeniedException('对不起申请诊所认证需要用户实名认证');
        }

        if (empty($fields['businessLicense']) || empty($fields['legalPersonName']) || empty($fields['legalPersonIdcard'])) {
            throw $this->createInvalidArgumentException('对不起诊所应该具备完整的营业资格证,法定人信息.');
        }

        if (empty($fields['nurseIds']) || empty($fields['doctorIds']) || count($fields['doctorIds']) < 1 || count($fields['nurseIds']) < 1) {
            throw $this->createInvalidArgumentException('诊所应具备至少一个医师,一个护士');
        }

        foreach ($fields['doctorIds'] as $userId) {
            if (empty($this->getUserService()->getUser($userId))) {
                throw $this->createNotFoundException("对不起,用户#{$userId}不存在");
            }
        }

        foreach ($fields['nurseIds'] as $userId) {
            if (empty($this->getUserService()->getUser($userId))) {
                throw $this->createNotFoundException("对不起,用户#{$userId}不存在");
            }
        }

        $fields['manageIds'] = array($user['id']);

        $clinic = $this->getClinicDao()->create($fields);

        $this->getLogService()->info('clinic', 'create-approve', "用户#{$user['id']}创建并申请认证<{$fields['name']}>诊所");
        $this->getNotificationService()->notify(
            $user['id'],
            'clinic-approve-apply',
            array(
                'message' => "您所创建并申请认证的《{$fields['name']}》诊所,正在等待认证.",
                'clinic' => $clinic
            ));
    }

    public function updateClinic($id, $fields)
    {
        $clinic = $this->getClinic($id);

        if (empty($clinic)) {
            throw createNotFoundException("对不起，没有查询到该诊所");
        }

        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        if (empty($fields['businessLicense']) || empty($fields['legalPersonName']) || empty($fields['legalPersonIdcard'])) {
            throw $this->createInvalidArgumentException('对不起诊所应该具备完整的营业资格证,法定人信息.');
        }

        if (empty($fields['nurseIds']) || empty($fields['doctorIds']) || count($fields['doctorIds']) < 1 || count($fields['nurseIds']) < 1) {
            throw $this->createInvalidArgumentException('诊所应具备至少一个医师,一个护士');
        }

        foreach ($fields['doctorIds'] as $userId) {
            if (empty($this->getUserService()->getUser($userId))) {
                throw $this->createNotFoundException("对不起,用户#{$userId}不存在");
            }
        }

        foreach ($fields['nurseIds'] as $userId) {
            if (empty($this->getUserService()->getUser($userId))) {
                throw $this->createNotFoundException("对不起,用户#{$userId}不存在");
            }
        }

        $clinic = $this->getClinicDao()->update($id, $fields);

        return $clinic;
    }

    public function approveClinic($id, $status, $reason = null)
    {
        $user = $this->getCurrentUser();
        $clinic = $this->getClinicDao()->get($id);

        if (empty($clinic)) {
            throw $this->createNotFoundException("对不起,#{$id}的诊所不存在");
        }

        if ($status == 'rejected') {
            if (empty($reason)) {
                throw $this->createServiceException('对不起,驳回时,需要填写驳回原因.');
            }

            $this->getClinicDao()->update($id, array('status' => $status, 'reason' => $reason));

            $this->getLogService()->info('clinic', 'approve-result', "用户#{$user['id']}申请认证<{$clinic['name']}>诊所,审核认证没有通过");
            $this->getNotificationService()->notify(
                $user['id'],
                'clinic-approve-result',
                array(
                    'status' => 'rejected',
                    'clinic' => $clinic,
                    'approveTime' => time(),
                    'reason' => $reason
                )
            );
        }

        if ($status == 'approved') {
            $this->getClinicDao()->update($id, array('status' => $status));

            $this->getLogService()->info('clinic', 'approve-result', "用户#{$user['id']}申请认证<{$clinic['name']}>诊所,审核认证通过");
            $this->getNotificationService()->notify(
                $user['id'],
                'clinic-approve-result',
                array(
                    'status' => 'approved',
                    'clinic' => $clinic,
                    'approveTime' => time()
                )
            );
        }
    }

    public function changeMark($data)
    {
        $fileIds = ArrayToolkit::column($data, 'id');
        $files = $this->getFileService()->getFilesByIds($fileIds);

        $data = ArrayToolkit::index($data, 'type');
        $files = ArrayToolkit::index($files, 'id');

        $marks = array();

        foreach ($data as $key => $value) {
                $marks["{$key}Mark"] = $files[$value['id']]['uri'];
        }

        return $marks;
    }

    public function countClinic($conditions)
    {
        return $this->getClinicDao()->count($conditions);
    }

    public function searchClinics($conditions, $orderBys, $start, $limit)
    {
       return $this->getClinicDao()->search($conditions, $orderBys, $start, $limit);
    }

    public function findClinicsByCommunityId($communityId)
    {
        if (empty($communityId)) {
            return array();
        }

        return $this->getClinicDao()->findByCommunityId($communityId);
    }

    public function bespeakClinic($fields, $clinicId)
    {
        $user = $this->getCurrentUser();
        if (empty($clinicId)) {
            throw $this->createInvalidArgumentException("参数错误，#{$clinicId}不能为空");
        }

        if (empty($fields['clinicId'])) {
            $fields['clinicId'] = $clinicId;
        }

        if (empty($fields['userId'])) {
            $fields['userId'] = $user['id'];
        }

        return $this->getClinicReservationRecordService()->createReservationRecord($fields);
    }

    protected function getFileService()
    {
        return $this->createService('Content:FileService');
    }

    public function getClinic($id)
    {
        return $this->getClinicDao()->get($id);
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'name',
                'userId',
                'communityId',
                'businessLicense',
                'address',
                'legalPersonName',
                'legalPersonIdcard',
                'doctorIds',
                'nurseIds',
                'introduction',
                'slogan',
                'smallMark',
                'midMark',
                'largeMark',
                'phone',
                'email',
            )
        );
    }

    protected function filterFields($fields)
    {
        return  ArrayToolkit::filter(
            $fields,
            array(
                'name' => '',
                'userId' => '',
                'communityId' => '',
                'businessLicense' => '',
                'address' => '',
                'legalPersonName' => '',
                'legalPersonIdcard' => '',
                'doctorIds' => array(),
                'nurseIds' => array(),
                'introduction' => '',
                'slogan' => '',
                'smallMark' => '',
                'midMark' => '',
                'largeMark' => '',
                'phone' => '',
                'email' => '',
            )
        );
    }

    protected function getClinicDao()
    {
        return $this->createDao('CustomBundle:Clinic:ClinicDao');
    }

    protected function getNotificationService()
    {
        return $this->createService('User:NotificationService');
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }

    protected function getUserService()
    {
        return $this->createService('User:UserService');
    }

    protected function getClinicReservationRecordService()
    {
        return $this->createService('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }
}