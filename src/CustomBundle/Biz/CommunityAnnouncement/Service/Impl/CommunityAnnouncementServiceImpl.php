<?php

namespace CustomBundle\Biz\CommunityAnnouncement\Service\Impl;

use AppBundle\Common\ArrayToolkit;
use CustomBundle\Biz\CommunityAnnouncement\Service\CommunityAnnouncementService;
use Biz\BaseService;

class CommunityAnnouncementServiceImpl extends BaseService implements CommunityAnnouncementService
{
    public function getCommunityAnnouncement($id)
    {
        return $this->getCommunityAnnouncementDao()->get($id);
    }

    public function createCommunityAnnouncement($fields)
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        if (!in_array('COMMUNITY_MANAGE', $user['communityRole'])) {
            throw $this->createAccessDeniedException();
        }

        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        if (empty($community)) {
            throw $this->createNotFoundException('对不起没有发现你所在的社区，请确认用户信息完整！');
        }

        $fields = array_merge($fields, array('userId' => $user['id'], 'communityId' => $community['id']));

        if (empty($fields['body'])) {
            throw $this->createServiceException('公告内容不能为空！');
        }

        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);
        $fields = $this->conversionFields($fields);

        return $this->getCommunityAnnouncementDao()->create($fields);
    }

    public function updateCommunityAnnouncement($id, $fields)
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        if (!in_array('COMMUNITY_MANAGE', $user['communityRole'])) {
            throw $this->createAccessDeniedException();
        }

        $communityAnnouncement = $this->getCommunityAnnouncement($id);

        $fields = array_merge($communityAnnouncement, $fields);
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);
        $fields = $this->conversionFields($fields);

        if (empty($fields['body'])) {
            throw $this->createServiceException('公告内容不能为空！');
        }

        return $this->getCommunityAnnouncementDao()->update($id, $fields);
    }

    public function deleteCommunityAnnouncement($id)
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        if (!in_array('COMMUNITY_MANAGE', $user['communityRole'])) {
            throw $this->createAccessDeniedException();
        }

        return $this->getCommunityAnnouncementDao()->delete($id);
    }

    public function countCommunityAnnouncement($conditions)
    {
        return $this->getCommunityAnnouncementDao()->count($conditions);
    }

    public function searchCommunityAnnouncement($conditions, $orderBys, $start, $limit)
    {
        return $this->getCommunityAnnouncementDao()->search($conditions, $orderBys, $start, $limit);
    }

    public function findCommunityAnnouncementsByCommunityId($communityId, $startTime)
    {
        return $this->getCommunityAnnouncementDao()->findByCommunityId($communityId, $startTime);
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'communityId',
                'userId',
                'targetType',
                'startTime',
                'endTime',
                'body',
                'title',
                'site',
            )
        );
    }

    protected function filterFields($fields)
    {
        return ArrayToolkit::filter(
            $fields,
            array(
                'communityId' => '0',
                'userId' => '0',
                'targetType' => '',
                'startTime' => '0',
                'endTime' => '0',
                'body' => '',
                'title' => '',
                'site' => '',
            )
        );
    }

    public function conversionFields($fields)
    {
        $fields['startTime'] = strtotime($fields['startTime']);
        $fields['endTime'] = strtotime($fields['endTime']);

        return $fields;
    }

    public function getCommunityAnnouncementDao()
    {
        return $this->createDao('CustomBundle:CommunityAnnouncement:CommunityAnnouncementDao');
    }

    public function getUserService()
    {
        return $this->createService('User:UserService');
    }

    public function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}