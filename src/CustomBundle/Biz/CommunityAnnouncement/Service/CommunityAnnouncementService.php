<?php

namespace CustomBundle\Biz\CommunityAnnouncement\Service;


interface CommunityAnnouncementService
{
    public function getCommunityAnnouncement($id);

    public function createCommunityAnnouncement($fields);

    public function updateCommunityAnnouncement($id, $fields);

    public function deleteCommunityAnnouncement($id);

    public function countCommunityAnnouncement($conditions);

    public function searchCommunityAnnouncement($conditions, $orderBys, $start, $limit);

    public function findCommunityAnnouncementsByCommunityId($communityId, $startTime);
}