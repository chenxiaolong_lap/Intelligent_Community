<?php

namespace CustomBundle\Biz\CommunityAnnouncement\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\CommunityAnnouncement\Dao\CommunityAnnouncementDao;

class CommunityAnnouncementDaoImpl extends GeneralDaoImpl implements CommunityAnnouncementDao
{
    protected $table = 'community_announcement';

    public function findByCommunityId($communityId, $startTime)
    {
        $sql = "SELECT * FROM {$this->table} WHERE communityId = ? AND endTime >= ?";

        return $this->db()->fetchAll($sql, array($communityId, $startTime));
    }

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
                'updatedTime',
                'startTime',
                'endTime'
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'id = :id',
                'id IN (: ids)',
                'userId = :userId',
                'communityId = :communityId',
                'title LIKE :title',
                'startTime > :startTime_GT',
                'endTime < :endTime_LT',
                'endTime >= :endTime_GE',
                'startTime <= :startTime_LE',
                'targetType = :targetType'
            )
        );
    }
}