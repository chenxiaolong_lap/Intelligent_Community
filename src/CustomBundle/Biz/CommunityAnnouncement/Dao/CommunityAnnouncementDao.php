<?php

namespace CustomBundle\Biz\CommunityAnnouncement\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface CommunityAnnouncementDao extends GeneralDaoInterface
{
    public function findByCommunityId($communityId, $startTime);
}