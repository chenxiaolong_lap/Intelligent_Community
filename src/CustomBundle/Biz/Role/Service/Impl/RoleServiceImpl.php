<?php

namespace CustomBundle\Biz\Role\Service\Impl;

use Biz\Role\Service\Impl\RoleServiceImpl as BaseRoleServiceImpl;
use Biz\Role\Util\PermissionBuilder;
use AppBundle\Common\Tree;

class RoleServiceImpl extends BaseRoleServiceImpl
{
    public function refreshRoles()
    {
        $permissions = PermissionBuilder::instance()->loadPermissionsFromAllConfig();
        $tree = Tree::buildWithArray($permissions, null, 'code', 'parent');

        $getSuperAdminRoles = $tree->column('code');
        $adminForbidRoles = array(
            'admin_user_avatar',
            'admin_user_change_password',
            'admin_my_cloud',
            'admin_cloud_video_setting',
            'admin_edu_cloud_sms',
            'admin_edu_cloud_search_setting',
            'admin_setting_cloud_attachment',
            'admin_setting_cloud',
            'admin_system',
        );

        $getAdminForbidRoles = array();
        foreach ($adminForbidRoles as $adminForbidRole) {
            $adminRole = $tree->find(function ($tree) use ($adminForbidRole) {
                return $tree->data['code'] === $adminForbidRole;
            });

            if (is_null($adminRole)) {
                continue;
            }

            $getAdminForbidRoles = array_merge($adminRole->column('code'), $getAdminForbidRoles);
        }

        $getTeacherRoles = $tree->find(function ($tree) {
            return $tree->data['code'] === 'web';
        });
        $getTeacherRoles = $getTeacherRoles->column('code');

        $roles = array(
            'ROLE_USER' => array(),
            'ROLE_ADMIN' => array_diff($getSuperAdminRoles, $getAdminForbidRoles),
            'ROLE_SUPER_ADMIN' => $getSuperAdminRoles,
        );

        foreach ($roles as $key => $value) {
            $userRole = $this->getRoleDao()->getByCode($key);

            if (empty($userRole)) {
                $this->initCreateRole($key, array_values($value));
            } else {
                $this->getRoleDao()->update($userRole['id'], array('data' => array_values($value)));
            }
        }
    }

    protected function initCreateRole($code, $role)
    {
        $userRoles = array(
            'ROLE_SUPER_ADMIN' => array('name' => '超级管理员', 'code' => 'ROLE_SUPER_ADMIN'),
            'ROLE_ADMIN' => array('name' => '管理员', 'code' => 'ROLE_ADMIN'),
            'ROLE_USER' => array('name' => '用户', 'code' => 'ROLE_USER'),
        );
        $userRole = $userRoles[$code];

        $userRole['data'] = $role;
        $userRole['createdTime'] = time();
        $userRole['createdUserId'] = $this->getCurrentUser()->getId();
        $this->getLogService()->info('role', 'init_create_role', '初始化三个角色"'.$userRole['name'].'"', $userRole);

        return $this->getRoleDao()->create($userRole);
    }
}