<?php

namespace CustomBundle\Biz\Property\Service\Impl;

use Biz\BaseService;
use CustomBundle\Biz\Property\Service\PropertyService;
use AppBundle\Common\ArrayToolkit;

class PropertyServiceImpl extends BaseService implements PropertyService
{
    public function createProperty($fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);
        $fields = $this->conversionFields($fields);

        return $this->getPropertyDao()->create($fields);
    }

    public function updateProperty($id, $fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        return $this->getPropertyDao()->update($id, $fields);
    }

    public function deleteProperty($id, $fields)
    {
        return $this->getPropertyDao()->delete($id);
    }

    public function getProperty($id)
    {
        return $this->getPropertyDao()->get($id);
    }

    public function countProperty($conditions)
    {
        return $this->getPropertyDao()->count($conditions);
    }

    public function searchProperty($conditions, $orderBys, $start, $limit)
    {
        return $this->getPropertyDao()->search($conditions, $orderBys, $start, $limit);
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'name',
                'housingEstateId',
                'servicePhone',
                'directorIds',
                'arrivalTime',
                'expiryTime'
            )
        );
    }

    protected function filterFields($fields)
    {
        return  ArrayToolkit::filter(
            $fields,
            array(
                'name' => '',
                'housingEstateId' => '',
                'servicePhone' => '',
                'arrivalTime' => '0',
                'directorIds' => array(),
                'expiryTime' => '0'
            )
        );
    }

    public function conversionFields($fields)
    {
        $fields['arrivalTime'] = strtotime($fields['arrivalTime']);
        $fields['expiryTime'] = strtotime($fields['expiryTime']);

        return $fields;
    }

    protected function getPropertyDao()
    {
        return $this->createDao('CustomBundle:Property:PropertyDao');
    }
}