<?php

namespace CustomBundle\Biz\Property\Service;

interface PropertyService
{
    public function createProperty($fields);

    public function updateProperty($id, $fields);

    public function deleteProperty($id, $fields);

    public function getProperty($id);

    public function countProperty($conditions);

    public function searchProperty($conditions, $orderBys, $start, $limit);
}