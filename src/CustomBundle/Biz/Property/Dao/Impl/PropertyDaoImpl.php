<?php

namespace CustomBundle\Biz\Property\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\Property\Dao\PropertyDao;

class PropertyDaoImpl extends GeneralDaoImpl implements PropertyDao
{
    protected $table = 'property';

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'serializes' => array(
                'directorIds' => 'delimiter',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'id = :id',
                'name LIKE :name',
                'servicePhone LIKE :servicePhone',
                'housingEstateId IN ( :housingEstateIds)',
            )
        );
    }
}