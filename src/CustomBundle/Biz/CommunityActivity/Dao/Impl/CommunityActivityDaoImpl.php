<?php

namespace CustomBundle\Biz\CommunityActivity\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;

class CommunityActivityDaoImpl extends GeneralDaoImpl
{
    protected $table = 'community_activity';

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
                'updatedTime',
                'startTime',
                'endTime'
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'serializes' => array(
                'invited' => 'delimiter',
                'participant' => 'delimiter',
            ),
            'conditions' => array(
                'id = :id',
                'id IN (: ids)',
                'userId = :userId',
                'type = :type',
                'communityId = :communityId',
                'title LIKE :title',
                'startTime > :startTime_GT',
                'endTime < :endTime_LT',
                'endTime >= :endTime_GE',
                'startTime <= :startTime_LE',
            )
        );
    }
}