<?php

namespace CustomBundle\Biz\CommunityActivity\Service\Impl;

use Biz\BaseService;
use Biz\User\Service\UserService;
use Codeages\Biz\Framework\Event\Event;
use CustomBundle\Biz\CommunityActivity\Service\CommunityActivityService;
use AppBundle\Common\ArrayToolkit;

class CommunityActivityServiceImpl extends BaseService implements CommunityActivityService
{
    public function getCommunityActivity($id)
    {
        return $this->getCommunityActivityDao()->get($id);
    }

    public function createCommunityActivity($fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);
        $this->conversionFields($fields);

        $activity = $this->getCommunityActivityDao()->create($fields);

        $this->dispatchEvent('community_activity.notify', new Event(array(
            'communityId' => $activity['communityId'],
            'activity' => $activity
        )));

        return $activity;
    }

    public function updateCommunityActivity($id, $fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);
        $this->conversionFields($fields);

        $activity = $this->getCommunityActivityDao()->update($id, $fields);
        $this->dispatchEvent('community_activity.notify', new Event(array(
            'communityId' => $activity['communityId'],
            'activity' => $activity
        )));

        return $activity;
    }

    public function deleteCommunityActivity($id)
    {
        return $this->getCommunityActivityDao()->delete($id);
    }

    public function countCommunityActivity($conditions)
    {
        return $this->getCommunityActivityDao()->count($conditions);
    }

    public function searchCommunityActivity($conditions, $orderBys, $start, $limit)
    {
        return $this->getCommunityActivityDao()->search($conditions, $orderBys, $start, $limit);
    }

    public function sign($user, $activityId)
    {
        if (empty($user)) {
            $user = $this->getCurrentUser();
        }

        $activity = $this->getCommunityActivity($activityId);

        if (empty($activity)) {
            throw createNotFoundException("没有找到#{$activityId}的活动");
        }

        if (in_array($user['id'], $activity['participant'])) {
            throw $this->createServiceException('你已经签过到了');
        }

        array_push($activity['participant'], $user['id']);

        return $this->getCommunityActivityDao()->update($activityId, array('participant' =>$activity['participant']));
    }

    protected function notification($communityId, $activity)
    {
        $pageSize = 100;
        $community = $this->getCommunityService()->getCommunity($communityId);
        $userCount = $this->getUserService()->searchUserProfileCount(
            array(
                'province' => $community['province'],
                'city' => $community['city'],
                'district' => $community['district'],
                'community' => $community['name']
            )
        );

        $totalPage = ceil($userCount / $pageSize);
        for ($i = 1; $i <= $totalPage; $i++) {
            $users = $this->getUserService()->searchUserProfiles(
                array(
                    'province' => $community['province'],
                    'city' => $community['city'],
                    'district' => $community['district'],
                    'community' => $community['name']
                ),
                array(),
                ($i - 1) * $pageSize,
                $pageSize
            );

            foreach ($users as $user) {
                $this->getNotificationService()->notify(
                    $user['id'],
                    'community-activity-notify',
                    array(
                        'activity' => $activity
                    )
                );
            }
        }
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'communityId',
                'userId',
                'type',
                'startTime',
                'endTime',
                'content',
                'title',
                'remark',
                'invited',
                'participant',
                'site'
            )
        );
    }

    protected function filterFields($fields)
    {
        return ArrayToolkit::filter(
            $fields,
            array(
                'communityId' => '0',
                'userId' => '0',
                'type' => '',
                'startTime' => '0',
                'endTime' => '0',
                'content' => '',
                'title' => '',
                'remark' => '',
                'invited' => '',
                'participant' => '',
                'site' => ''
            )
        );
    }

    public function conversionFields(&$fields)
    {
        $fields['startTime'] = strtotime($fields['startTime']);
        $fields['endTime'] = strtotime($fields['endTime']);

        return $fields;
    }

    protected function getCommunityActivityDao()
    {
        return $this->createDao('CustomBundle:CommunityActivity:CommunityActivityDao');
    }

    /**
     * @return UserService
     */
    protected function getUserService()
    {
        return $this->createService('CustomBundle:User:UserService');
    }

    protected function getNotificationService()
    {
        return $this->createService('User:NotificationService');
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}