<?php

namespace CustomBundle\Biz\CommunityActivity\Service;


interface CommunityActivityService
{
    public function getCommunityActivity($id);

    public function createCommunityActivity($fields);

    public function updateCommunityActivity($id, $fields);

    public function deleteCommunityActivity($id);

    public function countCommunityActivity($conditions);

    public function searchCommunityActivity($conditions, $orderBys, $start, $limit);

    public function sign($user, $activityId);
}