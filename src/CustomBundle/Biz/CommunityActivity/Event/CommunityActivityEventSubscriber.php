<?php
namespace CustomBundle\Biz\CommunityActivity\Event;

use Codeages\PluginBundle\Event\EventSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Codeages\Biz\Framework\Event\Event;

class CommunityActivityEventSubscriber extends EventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            'community_activity.notify' => 'activityNotify'
        );
    }

    public function activityNotify(Event $event)
    {
        $arguments = $event->getSubject();
        $pageSize = 100;

        if (!isset($arguments['communityId'])) {
            return;
        }

        $community = $this->getCommunityService()->getCommunity($arguments['communityId']);
        $userCount = $this->getUserService()->searchUserProfileCount(
            array(
                'province' => $community['province'],
                'city' => $community['city'],
                'district' => $community['district'],
                'community' => $community['name']
            )
        );

        $totalPage = ceil($userCount / $pageSize);
        for ($i = 1; $i <= $totalPage; $i++) {
            $users = $this->getUserService()->searchUserProfiles(
                array(
                    'province' => $community['province'],
                    'city' => $community['city'],
                    'district' => $community['district'],
                    'community' => $community['name']
                ),
                array(),
                ($i - 1) * $pageSize,
                $pageSize
            );

            foreach ($users as $user) {
                $this->getNotificationService()->notify(
                    $user['id'],
                    'community-activity-notify',
                    array(
                        'activity' => $arguments['activity']
                    )
                );
            }
        }
    }

    protected function getCommunityService()
    {
        return $this->getBiz()->service('CustomBundle:Community:CommunityService');
    }

    protected function getUserService()
    {
        return $this->getBiz()->service('User:UserService');
    }

    protected function getNotificationService()
    {
        return $this->getBiz()->service('User:NotificationService');
    }
}