<?php

namespace CustomBundle\Biz\ClinicReservationRecord\Dao\Impl;

use CustomBundle\Biz\ClinicReservationRecord\Dao\ClinicReservationRecordDao;
use Codeages\Biz\Framework\Dao\GeneralDaoImpl;

class ClinicReservationRecordDaoImpl extends GeneralDaoImpl implements ClinicReservationRecordDao
{
    protected $table = 'clinic_reservation_record';

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'serializes' => array(
                'doctorIds' => 'delimiter',
                'nurseIds' => 'delimiter',
                'updateUserIds' => 'delimiter',
            ),
            'conditions' => array(
                'id = :id',
                'userId = :userId',
                'userId IN (:userIds)',
                'status = :status',
                'clinicId IN (:clinicIds)',
                'clinicId = :clinicId',
            )
        );
    }
}