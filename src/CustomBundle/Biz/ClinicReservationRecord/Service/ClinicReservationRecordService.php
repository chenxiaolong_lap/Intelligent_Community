<?php

namespace CustomBundle\Biz\ClinicReservationRecord\Service;


interface ClinicReservationRecordService
{
    public function createReservationRecord($fields);

    public function updateClinicReservationRecord($id, $fields);

    public function getClinicReservationRecord($id);

    public function deleteReservationRecord($id);

    public function countReservationRecord($conditions);

    public function searchReservationRecords($conditions, $orderBys, $start, $limit);
}