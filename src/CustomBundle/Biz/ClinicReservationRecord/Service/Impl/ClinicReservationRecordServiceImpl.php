<?php

namespace CustomBundle\Biz\ClinicReservationRecord\Service\Impl;

use Biz\BaseService;
use CustomBundle\Biz\ClinicReservationRecord\Service\ClinicReservationRecordService;
use AppBundle\Common\ArrayToolkit;

class ClinicReservationRecordServiceImpl extends BaseService implements ClinicReservationRecordService
{
    public function createReservationRecord($fields)
    {
        $user = $this->getCurrentUser();
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        if (empty($fields['clinicId'])) {
            throw  $this->createInvalidArgumentException('对不起,无效的参数,诊所id不能为空');
        } else {
            $clinic = $this->getClinicService()->getClinic($fields['clinicId']);
            if (empty($clinic)) {
                throw $this->createNotFoundException("对不起id为#{$fields['clinicId']}的诊所不存在");
            }
        }

        $fields['reservationTime'] = strtotime($fields['reservationTime']);

        $record = $this->getClinicReservationRecordDao()->create($fields);

        $this->getLogService()->info('clinic', 'clinic-reservation', "用户#{$user['id']}预约<{$clinic['name']}>就诊成功!");
        $this->getNotificationService()->notify(
            $user['id'],
            'clinic-bespeak-result',
            array(
                'notify' => "您所预约《{$clinic['name']}》就诊成功.",
                'message' => "就诊时间为".date('Y-m-d H:i', $record['reservationTime']),
                'clinic' => $clinic
            )
        );

        return $record;
    }

    public function updateClinicReservationRecord($id, $fields)
    {
        $user = $this->getCurrentUser();
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        $record = $this->getClinicReservationRecordDao()->get($id);
        $clinic = $this->getClinicService()->getClinic($record['clinicId']);
        $updateIds = array_unique(array_merge($record['updateUserIds'], array($user['id'])));
        $fields['updateUserIds'] = $updateIds;

        $record = $this->getClinicReservationRecordDao()->update($id, $fields);

        $this->getLogService()->info('clinic', 'clinic-reservation', "用户#{$user['id']}更新<#{$record['id']}>预约就诊记录!");
        $this->getNotificationService()->notify(
            $user['id'],
            'clinic-bespeak-result-update',
            array(
                'notify' => "您更新了《{$record['id']}》就诊记录.",
                'clinic' => $clinic,
                'record' => $record
            )
        );

        return $record;
    }

    public function getClinicReservationRecord($id)
    {
        if (empty($id)) {
            return null;
        }

        return $this->getClinicReservationRecordDao()->get($id);
    }

    public function deleteReservationRecord($id)
    {

    }

    public function countReservationRecord($conditions)
    {
        return $this->getClinicReservationRecordDao()->count($conditions);
    }

    public function searchReservationRecords($conditions, $orderBys, $start, $limit)
    {
        return $this->getClinicReservationRecordDao()->search($conditions, $orderBys, $start, $limit);
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'name',
                'clinicId',
                'userId',
                'doctorIds',
                'nurseIds',
                'status',
                'symptom',
                'updateUserIds',
                'reservationTime',
                'feedback',
                'estimate',
                'scoreId',
            )
        );
    }

    protected function filterFields($fields)
    {
        return  ArrayToolkit::filter(
            $fields,
            array(
                'name' => '',
                'clinicId' => '0',
                'userId' => '0',
                'doctorIds' => array(),
                'nurseIds' => array(),
                'status' => '',
                'symptom' => '',
                'updateUserIds' => array(),
                'reservationTime' => '0',
                'feedback' => '',
                'estimate' => '',
                'scoreId' => '0'
            )
        );
    }

    protected function getNotificationService()
    {
        return $this->createService('User:NotificationService');
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }

    protected function getClinicReservationRecordDao()
    {
        return $this->createDao('CustomBundle:ClinicReservationRecord:ClinicReservationRecordDao');
    }

    protected function getClinicService()
    {
        return $this->createService('CustomBundle:Clinic:ClinicService');
    }
}