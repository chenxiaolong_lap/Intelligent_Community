<?php


namespace CustomBundle\Biz\ClinicReservationRecord\Event;

use Codeages\Biz\Framework\Event\Event;
use Codeages\PluginBundle\Event\EventSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ClinicReservationRecordSubscriber extends EventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            'record.score' => 'updateScore'
        );
    }

    public function updateScore(Event $event)
    {
        $record = $event->getSubject();
        $score = $event->getArguments();

        $this->getClinicReservationRecordService()->updateClinicReservationRecord($record['id'], array('scoreId' => $score['id']));
    }

    protected function getClinicReservationRecordService()
    {
        return $this->getBiz()->service('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }
}