<?php

namespace CustomBundle\Biz\HousingEstate\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\HousingEstate\Dao\HousingEstateDao;

class HousingEstateDaoImpl extends GeneralDaoImpl implements HousingEstateDao
{
    protected $table = 'housing_estate';

    public function getByCommunityIdAndName($communityId, $name)
    {
        return $this->getByFields(array('communityId' => $communityId, 'name' => $name));
    }

    public function findByLikeName($name)
    {
        $sql = "SELECT * FROM {$this->table} WHERE `name` like ? GROUP BY `name` LIMIT 10";

        return $this->db()->fetchAll($sql, array('%'.$name.'%'));
    }


    public function findCountByCommunityIds($communityIds)
    {
        if (empty($communityIds)) {
            return array();
        }

        $marks = str_repeat('?,', count($communityIds) - 1).'?';
        $sql = "SELECT communityId,COUNT(id) AS count FROM {$this->table()} WHERE communityId IN ({$marks}) group by communityId;";

        return $this->db()->fetchAll($sql, $communityIds);
    }

    public function getByName($name)
    {
        return $this->getByFields(array('name' => $name));
    }


    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'id = :id',
                'id IN ( :ids)',
                'communityId IN ( :communityIds)',
                'communityId = :communityId',
                'name LIKE :name',
            )
        );
    }
}