<?php

namespace CustomBundle\Biz\HousingEstate\Dao;

use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface HousingEstateDao extends GeneralDaoInterface
{
    public function getByCommunityIdAndName($communityId, $name);

    public function findByLikeName($name);

    public function findCountByCommunityIds($communityIds);

    public function getByName($name);
}