<?php

namespace CustomBundle\Biz\HousingEstate\Service\Impl;

use AppBundle\Common\ArrayToolkit;
use Biz\BaseService;
use CustomBundle\Biz\HousingEstate\Service\HousingEstateService;

class HousingEstateServiceImpl extends BaseService implements HousingEstateService
{
    public function createHousingEstate($fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        return $this->getHousingEstateDao()->create($fields);
    }

    public function updateHousingEstate($id, $fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        return $this->getHousingEstateDao()->update($id, $fields);
    }

    public function getHousingEstate($id)
    {
        return $this->getHousingEstateDao()->get($id);
    }

    public function deleteHousingEstate($id)
    {
        return $this->getHousingEstateDao()->delete($id);
    }

    public function countHousingEstate($conditions)
    {
        return $this->getHousingEstateDao()->count($conditions);
    }

    public function searchHousingEstate($conditions, $orderBys, $start, $limit)
    {
        return $this->getHousingEstateDao()->search($conditions, $orderBys, $start, $limit);
    }

    public function getHousingEstateByCommunityIdAndName($communityId, $name)
    {
        if (empty($communityId) || empty($name)) {
            return array();
        }

        return $this->getHousingEstateDao()->getByCommunityIdAndName($communityId, $name);
    }

    public function findHousingEstatesByLikeName($name)
    {
        if (empty($name)) {
            return array();
        }

        return $this->getHousingEstateDao()->findByLikeName($name);
    }

    public function findCountHousingEstateByCommunityIds($communityIds)
    {
        if (empty($communityIds)) {
            return array();
        }

        return $this->getHousingEstateDao()->findCountByCommunityIds($communityIds);
    }

    public function getHousingEstateByName($name)
    {
        if (empty($name)) {
            throw $this->createServiceException('参数异常');
        }

        return $this->getHousingEstateDao()->getByName($name);
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'name',
                'address',
                'communityId',
            )
        );
    }

    protected function filterFields($fields)
    {
        return ArrayToolkit::filter(
            $fields,
            array(
                'name' => '',
                'address' => '',
                'communityId' => '0',
            )
        );
    }

    protected function getHousingEstateDao()
    {
        return $this->createDao('CustomBundle:HousingEstate:HousingEstateDao');
    }
}