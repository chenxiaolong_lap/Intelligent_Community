<?php

namespace CustomBundle\Biz\HousingEstate\Service;

interface HousingEstateService
{
    public function createHousingEstate($fields);

    public function updateHousingEstate($id, $fields);

    public function getHousingEstate($id);

    public function deleteHousingEstate($id);

    public function countHousingEstate($conditions);

    public function searchHousingEstate($conditions, $orderBys, $start, $limit);

    public function getHousingEstateByCommunityIdAndName($communityId, $name);

    public function findHousingEstatesByLikeName($name);

    public function findCountHousingEstateByCommunityIds($communityIds);

    public function getHousingEstateByName($name);
}