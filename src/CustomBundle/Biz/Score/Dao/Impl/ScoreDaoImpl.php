<?php

namespace CustomBundle\Biz\Score\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\Score\Dao\ScoreDao;

class ScoreDaoImpl extends GeneralDaoImpl implements ScoreDao
{
    protected $table = 'score';

    public function findByTargetTypeAndTargetId($type, $targetId)
    {
        if (empty($type) || empty($targetId)) {
            return array();
        }

        return $this->findByFields(array('targetType' => $type, 'targetId' => $targetId));
    }

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),

            'conditions' => array(
                'id = :id',
                'id IN (: ids)',
                'targetType = :targetType',
                'targetId = :targetId',
                'userId = :userId',
            )
        );
    }
}