<?php

namespace CustomBundle\Biz\Score\Dao;


use Codeages\Biz\Framework\Dao\GeneralDaoInterface;

interface ScoreDao extends GeneralDaoInterface
{
    public function findByTargetTypeAndTargetId($type, $targetId);
}