<?php

namespace CustomBundle\Biz\Score\Service\Impl;

use Biz\BaseService;
use Codeages\Biz\Framework\Event\Event;
use CustomBundle\Biz\Score\Service\ScoreService;
use AppBundle\Common\ArrayToolkit;

class ScoreServiceImpl extends BaseService implements ScoreService
{
    public function createScore($fields)
    {
        $user = $this->getCurrentUser();
        $sourceId = $fields['sourceId'];

        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);
        $fields['userId'] = $user['id'];

        if (empty($fields['targetType'])) {
            throw $this->createInvalidArgumentException('评分对象类型不能为空');
        }

        if (empty($fields['targetId'])) {
            throw $this->createInvalidArgumentException('评分对象id不能为空');
        }

        $typeService = $this->getConfigService($fields['targetType']);

        $getFunction = "get".ucfirst($fields['targetType']);

        $target = $typeService->$getFunction($fields['targetId']);

        if (empty($target)) {
            throw $this->createNotFoundException('未查询到评分对象');
        }

        $score = $this->getScoreDao()->create($fields);

        $source = $this->getSourceByConfigType($fields['targetType'], $sourceId, $score);

        $this->getLogService()->info('clinic', 'clinic-score', "用户#{$user['id']}对<{$target['name']}>进行了评分");
        $this->getNotificationService()->notify(
            $user['id'],
            'score',
            array(
                'score' => $score,
                'source' => $source,
            )
        );

        return $score;
    }

    public function countScores($conditions)
    {
        return $this->getScoreDao()->count($conditions);
    }

    public function searchScores($conditions, $orderBys, $start, $limit)
    {
        return $this->getScoreDao()->search($conditions, $orderBys, $start,$limit);
    }

    public function findScoresByTargetTypeAndTargetId($type, $targetId)
    {
        if (empty($type) || empty($targetId)) {
            throw $this->createInvalidArgumentException('参数错误');
        }

        return $this->getScoreDao()->findByTargetTypeAndTargetId($type, $targetId);
    }

    protected function getScoreDao()
    {
        return $this->createDao('CustomBundle:Score:ScoreDao');
    }

    protected function getConfigEvent($type)
    {
        $map = array(
            'clinic' => 'record.score'
        );

        return $map[$type];
    }

    protected function getSourceByConfigType($type, $sourceId, $score)
    {
        $map = array(
            'clinic' => 'ClinicReservationRecord'
        );

        if (empty($type) || empty($sourceId)) {
            throw $this->createServiceException('参数错误');
        }


        $getSourceFunction = 'get'.$map[$type];
        $updateSourceFunction = 'update'.$map[$type];

        $service = $this->createService("CustomBundle:{$map[$type]}:{$map[$type]}Service");

        $source = $service->$getSourceFunction($sourceId);

        return $service->$updateSourceFunction($source['id'], array('scoreId' => $score['id']));
    }

    protected function getConfigService($type)
    {
        $type = ucfirst($type);
        return $this->createService("CustomBundle:{$type}:{$type}Service");
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'userId',
                'targetType',
                'targetId',
                'score',
            )
        );
    }

    protected function filterFields($fields)
    {
        return  ArrayToolkit::filter(
            $fields,
            array(
                'userId' => '0',
                'targetType' => '',
                'targetId' => '0',
                'score' => '0',
            )
        );
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }

    protected function getUserService()
    {
        return $this->createService('User:UserService');
    }

    protected function getNotificationService()
    {
        return $this->createService('User:NotificationService');
    }

    protected function getClinicReservationRecordService()
    {
        return $this->createService('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }
}