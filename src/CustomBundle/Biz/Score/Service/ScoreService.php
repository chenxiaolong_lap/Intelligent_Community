<?php

namespace CustomBundle\Biz\Score\Service;


interface ScoreService
{
    public function createScore($fields);

    public function findScoresByTargetTypeAndTargetId($type, $targetId);

    public function searchScores($conditions, $orderBys, $start, $limit);

    public function countScores($conditions);
}