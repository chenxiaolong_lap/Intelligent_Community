<?php

namespace CustomBundle\Biz\Slideshow\Dao\Impl;

use Codeages\Biz\Framework\Dao\GeneralDaoImpl;
use CustomBundle\Biz\Slideshow\Dao\SlideshowDao;

class SlideshowDaoImpl extends GeneralDaoImpl implements SlideshowDao
{
    protected $table = 'community_slideshow';

    public function declares()
    {
        return array(
            'orderbys' => array(
                'createdTime',
                'id',
                'serial_number'
            ),
            'timestamps' => array(
                'createdTime',
                'updatedTime',
            ),
            'conditions' => array(
                'id = :id',
                'communityId = :communityId',
                'serial_number = :serial_number'
            )
        );
    }
}