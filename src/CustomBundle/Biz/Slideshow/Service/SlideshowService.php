<?php

namespace CustomBundle\Biz\Slideshow\Service;


interface SlideshowService
{
    public function createSlideshow($fields);

    public function updateSlideshow($id, $fields);

    public function deleteSlideshow($id, $fields);

    public function getSlideshow($id);

    public function countSlideshow($conditions);

    public function searchSlideshow($conditions, $orderBys, $start, $limit);

    public function changeSlideshowPicture($id, $pictureArray);
}