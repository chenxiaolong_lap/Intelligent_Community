<?php

namespace CustomBundle\Biz\Slideshow\Service\Impl;

use Biz\BaseService;
use CustomBundle\Biz\Slideshow\Service\SlideshowService;
use AppBundle\Common\ArrayToolkit;

class SlideshowServiceImpl extends BaseService implements SlideshowService
{
    public function createSlideshow($fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        return $this->getSlideshowDao()->create($fields);
    }

    public function updateSlideshow($id, $fields)
    {
        $fields = $this->partFields($fields);
        $fields = $this->filterFields($fields);

        return $this->getSlideshowDao()->update($id, $fields);
    }

    public function deleteSlideshow($id, $fields)
    {
        return $this->getSlideshowDao()->delete($id);
    }

    public function getSlideshow($id)
    {
        return $this->getSlideshowDao()->get($id);
    }

    public function countSlideshow($conditions)
    {
        return $this->getSlideshowDao()->count($conditions);
    }

    public function searchSlideshow($conditions, $orderBys, $start, $limit)
    {
        return $this->getSlideshowDao()->search($conditions, $orderBys, $start, $limit);
    }

    public function changeSlideshowPicture($id, $pictureArray)
    {
        if (empty($pictureArray)) {
            throw $this->createInvalidArgumentException('Invalid Param: picture');
        }

        $picture = $this->getFileService()->getFile($pictureArray['id']);
        $slideshow = $this->getSlideshowDao()->get($id);

        if (!empty($slideshow['picturePath'])) {
            $this->getFileService()->deleteFileByUri($slideshow['picturePath']);
        }

        $slideshow = $this->getSlideshowDao()->update($id, array('picturePath' => $picture['uri']));

        return $slideshow;
    }

    protected function partFields($fields)
    {
        return ArrayToolkit::parts(
            $fields,
            array(
                'userId',
                'communityId',
                'pictureUrl',
                'picturePath',
                'backgroundColor',
                'displayType',
                'description',
                'serial_number',
                'status',
            )
        );
    }

    protected function filterFields($fields)
    {
        return  ArrayToolkit::filter(
            $fields,
            array(
                'userId' => '0',
                'communityId' => '0',
                'pictureUrl' => '',
                'picturePath' => '',
                'backgroundColor' => '',
                'displayType' => '',
                'description' => '',
                'serial_number' => '0',
                'status' => '0',
            )
        );
    }

    protected function getFileService()
    {
        return $this->biz->service('Content:FileService');
    }
    
    protected function getSlideshowDao()
    {
        return $this->createDao('CustomBundle:Slideshow:SlideshowDao');
    }
}