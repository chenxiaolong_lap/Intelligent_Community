<?php

namespace CustomBundle\Biz\Import;

use Biz\Importer\Importer;
use CustomBundle\Biz\User\Service\Impl\UserServiceImpl;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use UserImporterPlugin\Biz\UserImporter\Service\Impl\UserImporterServiceImpl;
use AppBundle\Common\FileToolkit;
use Topxia\Service\Common\ServiceKernel;

class CommunityUsersImporter extends Importer
{
    protected $necessaryFields = array(
        'nickname' => '用户名',
        ''
    );
    protected $objWorksheet;
    protected $rowTotal = 0;
    protected $colTotal = 0;
    protected $excelFields = array();
    protected $passValidateUser = array();

    protected $type = 'community-users';

    public function import(Request $request)
    {
        $importData = $request->request->get('importData');

        return $this->excelDataImporting($importData);
    }

    protected function excelDataImporting($importData)
    {
        $existsUserCount = 0;
        $successCount = 0;
        $userByEmail    = array();
        $userByNickname = array();
        $userByMobile   = array();
        $users          = array();

        foreach ($importData as $key => $user) {
            if ($user["gender"] == "男") {
                $user["gender"] = "male";
            }

            if ($user["gender"] == "女") {
                $user["gender"] = "female";
            }

            if ($user["gender"] == "") {
                $user["gender"] = "secret";
            }

            if ($this->getUserImporterService()->isEmailOrMobileRegisterMode()) {
                if ($this->getUserService()->getUserByVerifiedMobile($user["mobile"])) {
                    //email, nickname, verifiedmobile只有一个能修改
                    $userByMobile[] = $user;
                }
            } elseif ($this->getUserService()->getUserByNickname($user["nickname"])) {
                $userByNickname[] = $user;
            } elseif ($this->getUserService()->getUserByEmail($user["email"])) {
                $userByEmail[] = $user;
            } else {
                $users[] = $user;
            }

            ++$successCount;
        }

        $this->getUserImporterService()->importUpdateNickname($userByNickname);
        $this->getUserImporterService()->importUpdateEmail($userByEmail);
        $this->getUserImporterService()->importUpdateMobile($userByMobile);
        $this->getUserImporterService()->importUsers($users);


        return array('existsUserCount' => $existsUserCount, 'successCount' => $successCount);
    }

    public function check(Request $request)
    {
        $file = $request->files->get('excel');
        $courseId = $request->request->get('courseId');
        $danger = $this->validateExcelFile($file);
        if (!empty($danger)) {
            return $danger;
        }

        $repeatInfo = $this->checkRepeatData();
        if (!empty($repeatInfo)) {
            return $this->createErrorResponse($repeatInfo);
        }

        $importData = $this->getUserData($courseId);

        if (empty($importData['errorInfo'])) {
            $passedRepeatInfo = $this->checkPassedRepeatData();
            if ($passedRepeatInfo) {
                return $this->createErrorResponse($passedRepeatInfo);
            }
        } else {
            return $this->createErrorResponse($importData['errorInfo']);
        }

        return $this->createSuccessResponse(
            $importData['allUserData'],
            $importData['checkInfo']
        );
    }

    protected function checkPassedRepeatData()
    {
        $passedUsers = $this->passValidateUser;
        $ids = array();
        $repeatRow = array();
        $repeatIds = array();

        foreach ($passedUsers as $key => $passedUser) {
            if (in_array($passedUser['id'], $ids) && !in_array($passedUser['id'], $repeatIds)) {
                $repeatIds[] = $passedUser['id'];
            } else {
                $ids[] = $passedUser['id'];
            }
        }

        foreach ($passedUsers as $key => $passedUser) {
            if (in_array($passedUser['id'], $repeatIds)) {
                $repeatRow[$passedUser['id']][] = $passedUser['row'];
            }
        }

        $repeatRowInfo = '';
        $repeatArray = array();

        if (!empty($repeatRow)) {
            $repeatRowInfo .= '字段对应用户数据重复'.'</br>';

            foreach ($repeatRow as $row) {
                $repeatRowInfo .= '重复行：'.'</br>';

                foreach ($row as $value) {
                    $repeatRowInfo .= sprintf('第%s行', $value);
                }

                $repeatRowInfo .= '</br>';

                $repeatArray[] = $repeatRowInfo;
                $repeatRowInfo = '';
            }
        }

        return $repeatArray;
    }

    protected function getUserData($courseId)
    {
        $userCount = 0;
        $fieldSort = $this->getFieldSort();
        $validate = array();
        $allUserData = array();

        for ($row = 3; $row <= $this->rowTotal; ++$row) {
            for ($col = 0; $col < $this->colTotal; ++$col) {
                $infoData = $this->objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
                $columnsData[$col] = $infoData.'';
            }

            foreach ($fieldSort as $sort) {
                $userScoreData[$sort['fieldName']] = trim($columnsData[$sort['num']]);
                $fieldCol[$sort['fieldName']] = $sort['num'] + 1;
            }

            $emptyData = array_count_values($userScoreData);

            if (isset($emptyData['']) && count($userScoreData) == $emptyData['']) {
                $checkInfo[] = sprintf('第%s行为空行，已跳过', $row);
                continue;
            }

            $info = $this->validExcelFieldValue($userScoreData, $row, $fieldCol, $courseId);
            empty($info) ? '' : $errorInfo[] = $info;

            $userCount = $userCount + 1;
            $allUserData[] = $userScoreData;

            if (empty($errorInfo)) {
                $user = $this->getUserService()->getUserByNickname($userScoreData['nickname']);

                $validate[] = array_merge($userScoreData, array('row' => $row, 'id' => $user['id']));
            }

            unset($userScoreData);
        }

        $this->passValidateUser = $validate;

        $data['errorInfo'] = empty($errorInfo) ? array() : $errorInfo;
        $data['checkInfo'] = empty($checkInfo) ? array() : $checkInfo;
        $data['userCount'] = $userCount;
        $data['allUserData'] = empty($this->passValidateUser) ? array() : $this->passValidateUser;

        return $data;
    }

    protected function validateExcelFile($file)
    {
        if (!is_object($file)) {
            return $this->createDangerResponse('请选择上传的文件');
        }

        if (FileToolkit::validateFileExtension($file, 'xls xlsx')) {
            return $this->createDangerResponse('Excel格式不正确！');
        }

        $this->excelAnalyse($file);

        if ($this->rowTotal > 1000) {
            return $this->createDangerResponse('Excel超过1000行数据!');
        }

        if (!$this->checkNecessaryFields($this->excelFields)) {
            return $this->createDangerResponse('缺少必要的字段');
        }
    }

    protected function validExcelFieldValue($userData, $row, $fieldCol, $courseId)
    {
        $errorInfo = '';

        if (empty($userData['email']) && empty($userData['mobile'])) {
            $errorInfo = "第{$row}行的信息有误, 注册用户必须含有邮箱或者手机号。";
        }

        if (!$this->getUserService()->isNicknameAvaliable($userData['nickname'])) {
            $errorInfo = "第{$row}行的信息有误, 注册用户的用户名不能使用请再次修改。";
        }

        if (empty($userData['password'])) {
            $errorInfo = "第{$row}行的信息有误, 注册用户必须含有密码。";
        }

        return $errorInfo;
    }

    protected function checkRepeatData()
    {
        $errorInfo = array();
        $checkFields = array_keys($this->getNecessaryFields());
        $fieldSort = $this->getFieldSort();

        foreach ($checkFields as $checkField) {
            $nicknameData = array();

            foreach ($fieldSort as $key => $value) {
                if ($value['fieldName'] == $checkField) {
                    $nickNameCol = $value['num'];
                }
            }

            for ($row = 3; $row <= $this->rowTotal; ++$row) {
                $nickNameColData = $this->objWorksheet->getCellByColumnAndRow($nickNameCol, $row)->getValue();

                $nicknameData[] = $nickNameColData.'';
            }

            $info = $this->arrayRepeat($nicknameData, $nickNameCol);

            empty($info) ? '' : $errorInfo[] = $info;
        }

        return $errorInfo;
    }

    protected function arrayRepeat($array, $nickNameCol)
    {
        $repeatArrayCount = array_count_values($array);

        $repeatRow = '';

        foreach ($repeatArrayCount as $key => $value) {
            if ($value > 1 && !empty($key)) {
                $repeatRow .= sprintf('第%s列重复:', $nickNameCol + 1).'<br>';

                for ($i = 1; $i <= $value; ++$i) {
                    $row = array_search($key, $array) + 3;

                    $repeatRow .= sprintf('第%s行    %s', $row, $key).'<br>';

                    unset($array[$row - 3]);
                }
            }
        }

        return $repeatRow;
    }

    protected function getFieldSort()
    {
        $fieldSort = array();
        $fieldArray = $this->getFieldArray();
        $excelFields = $this->excelFields;

        foreach ($excelFields as $key => $value) {
            $value = $this->trim($value);
            if (in_array($value, $fieldArray)) {
                foreach ($fieldArray as $fieldKey => $fieldValue) {
                    if ($value == $fieldValue) {
                        $fieldSort[$fieldKey] = array('num' => $key, 'fieldName' => $fieldKey);
                        break;
                    }
                }
            }
        }

        return $fieldSort;
    }


    protected function getFieldArray()
    {
        return array(
            'email' => '邮箱',
            'nickname' => '用户名',
            "password"    => '密码',
            "truename"    => '姓名',
            "gender"      => '性别',
            "idcard"      => '身份证号',
            "mobile"      => '手机号',
            "company"     => '公司',
            "job"         => '职业',
            "site"        => '个人主页',
            "weibo"       => '微博',
            "weixin"      => '微信',
            "qq"          => 'QQ',
            "community"   => '所在社区',
            'housingEstate' => '所在小区',
            'province' => '省',
            'city' => '市',
            'district' => '区'
        );
    }

    protected function trim($data)
    {
        $data = trim($data);
        $data = str_replace(' ', '', $data);
        $data = str_replace('\n', '', $data);
        $data = str_replace('\r', '', $data);
        $data = str_replace('\t', '', $data);

        return $data;
    }


    protected function excelAnalyse($file)
    {
        $objPHPExcel = \PHPExcel_IOFactory::load($file);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
        $excelFields = array();

        for ($col = 0; $col < $highestColumnIndex; ++$col) {
            $fieldTitle = $objWorksheet->getCellByColumnAndRow($col, 2)->getValue();
            empty($fieldTitle) ? '' : $excelFields[$col] = $this->trim($fieldTitle);
        }

        $rowAndCol = array('rowLength' => $highestRow, 'colLength' => $highestColumnIndex);

        $this->objWorksheet = $objWorksheet;
        $this->rowTotal = $highestRow;
        $this->colTotal = $highestColumnIndex;
        $this->excelFields = $excelFields;

        return array($objWorksheet, $rowAndCol, $excelFields);
    }

    protected function checkNecessaryFields($excelFields)
    {
        return ArrayToolkit::some($this->necessaryFields, function ($fields) use ($excelFields) {
            return in_array($fields, array_values($excelFields));
        });
    }

    public function getTemplate(Request $request)
    {
        return $this->render('@Custom/my/user/import.html.twig', array(
            'importerType' => $this->type,
        ));
    }

    public function tryImport(Request $request)
    {
        $userId = $request->query->get('userId');
        $user = ServiceKernel::instance()->getCurrentUser();
        $user = $this->getUserService()->getUser($user['id']);

        if (!in_array('COMMUNITY_MANAGE', $user['communityRole'])) {
            throw new \RuntimeException('对不起没有权限导入');
        }
    }

    public function getNecessaryFields()
    {
        $necessaryFields = array('password' => '密码');

        return $necessaryFields;
    }

    /**
     * @return UserServiceImpl
     */
    protected function getUserService()
    {
        return $this->biz->service('User:UserService');
    }

    /**
     * @return UserImporterServiceImpl
     */
    protected function getUserImporterService()
    {
        return $this->biz->service('UserImporterPlugin:UserImporter:UserImporterService');
    }
}