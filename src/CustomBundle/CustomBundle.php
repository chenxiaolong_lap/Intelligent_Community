<?php

namespace CustomBundle;

use Codeages\PluginBundle\System\PluginBase;
use CustomBundle\Biz\CustomServiceProvider;

class CustomBundle extends PluginBase
{
    public function getParent()
    {
        return 'AppBundle';
    }

    public function boot()
    {
        parent::boot();
        $biz = $this->container->get('biz');
        $biz->register(new CustomServiceProvider());
        $serviceAlias = $this->getRewriteServiceAlias();
        $daoAlias = $this->getRewriteDaoAlias();
        $this->rewriteService($serviceAlias);
        $this->rewriteDao($daoAlias);
    }

    public function getRewriteServiceAlias()
    {
        return array(
            'Course:CourseService',
            'User:UserService',
            'Role:RoleService',
            'Article:ArticleService',
        );
    }

    public function getRewriteDaoAlias()
    {
        return array(
            'Course:CourseDao',
            'User:UserProfileDao',
            'User:UserDao',
            'Article:ArticleDao'
        );
    }

    public function rewriteService($serviceAliases)
    {
        $biz = $this->container->get('biz');
        //rewrite service
        foreach ($serviceAliases as $serviceAlias){
            $biz["@{$serviceAlias}"] = $biz->service("CustomBundle:{$serviceAlias}");
        }
    }

    public function rewriteDao($daoAliases)
    {
        $biz = $this->container->get('biz');
        //rewrite service
        foreach ($daoAliases as $daoAlias){
            $biz["@{$daoAlias}"] = $biz->dao("CustomBundle:{$daoAlias}");
        }
    }

    public function getEnabledExtensions()
    {
        return array('DataTag', 'StatusTemplate', 'DataDict', 'NotificationTemplate');
    }
}
