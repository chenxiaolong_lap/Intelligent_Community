<?php
/**
 * Created by PhpStorm.
 * User: cxl
 * Date: 18-6-8
 * Time: 下午2:09
 */

namespace CustomBundle\Extensions\DataTag;


use CustomBundle\Biz\Clinic\Service\ClinicService;

class ClinicDataTag extends CustomDataTag
{
    public function getData(array $arguments)
    {
        $clinicId = $arguments['clinicId'];

        return $this->getClinicService()->getClinic($clinicId);
    }

    /**
     * @return ClinicService
     */
    protected function getClinicService()
    {
        return $this->getServiceKernel()->createService('CustomBundle:Clinic:ClinicService');
    }
}