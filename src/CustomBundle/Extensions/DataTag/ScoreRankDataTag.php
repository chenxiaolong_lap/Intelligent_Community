<?php

namespace CustomBundle\Extensions\DataTag;

use AppBundle\Common\ArrayToolkit;
use Topxia\Service\Common\ServiceKernel;
use AppBundle\Extensions\DataTag\BaseDataTag;
use AppBundle\Extensions\DataTag\DataTag;

class ScoreRankDataTag extends BaseDataTag implements DataTag
{
    public function getData(array $arguments)
    {
        $type = $arguments['type'];
        $sources = $arguments['sources'];

        $data = array();
        foreach ($sources as $source) {
            $scores = $this->getScoreService()->findScoresByTargetTypeAndTargetId($type, $source['id']);
            $data[] = array(
                'score' => !empty($scores) ? array_sum(ArrayToolkit::column($scores, 'score')) / count($scores) : 0,
                'source' => $source
            );
        }

        array_multisort(ArrayToolkit::column($data, 'score'), constant('SORT_DESC'), $data);

        return $data;
    }

    protected function getScoreService()
    {
        return $this->getServiceKernel()->createService('CustomBundle:Score:ScoreService');
    }

    protected function getConfigService($type)
    {
        $type = ucfirst($type);
        return $this->getServiceKernel()->createService("CustomBundle:{$type}:{$type}Service");
    }
}