<?php

namespace CustomBundle\Extensions\DataTag;

class ClinicReservationNumberDataTag extends CustomDataTag
{
    public function getData(array $arguments)
    {
        $clinicId = $arguments['clinicId'];

        $count = $this->getClinicReservationRecordService()->countReservationRecord(array('clinicId' => $clinicId));

        return $count;
    }

    protected function getClinicReservationRecordService()
    {
        return $this->getServiceKernel()->createService('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }
}