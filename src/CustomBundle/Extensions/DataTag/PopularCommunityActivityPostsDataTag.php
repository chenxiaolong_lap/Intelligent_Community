<?php

namespace CustomBundle\Extensions\DataTag;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Extensions\DataTag\BaseDataTag;
use AppBundle\Extensions\DataTag\DataTag;
use CustomBundle\Biz\CommunityActivity\Service\CommunityActivityService;
use Biz\Group\Service\ThreadService;
use Topxia\Service\Common\ServiceKernel;

class PopularCommunityActivityPostsDataTag extends BaseDataTag implements DataTag
{
    /**
     * 获取个人动态
     *
     *   count    必需
     *
     * @param array $arguments 参数
     *
     * @return array 个人动态
     */
    public function getData(array $arguments)
    {
        $community = $this->getCommunity();
        $limitCount = $arguments['count'] * 2;
        $posts = $this->getThreadService()->searchPosts(
            array(
                'targetType' => 'community-activity',
                'parentId' => 0,
                'latest' => 'week',
            ),
            array('ups' => 'DESC', 'createdTime' => 'DESC'),
            0,
            $limitCount
        );
        if ($limitCount > count($posts)) {
            $conditions = array(
                'targetType' => 'community-activity',
            );

            $excludeIds = ArrayToolkit::column($posts, 'id');
            if (!empty($excludeIds)) {
                $conditions['excludeIds'] = $excludeIds;
            }

            $otherPosts = $this->getThreadService()->searchPosts(
                $conditions,
                array('ups' => 'DESC', 'createdTime' => 'DESC'),
                0,
                $limitCount - count($posts)
            );

            $posts = array_merge($posts, $otherPosts);
        }

        $activityIds = ArrayToolkit::column($posts, 'targetId');
        $activities = $this->getCommunityActivityService()->searchCommunityActivity(
            array(
                'ids' => $activityIds,
                'communityId' => $community['id']
            ),
            array(),
            0,
            PHP_INT_MAX
        );
        $activities = ArrayToolkit::index($activities, 'id');

        foreach ($posts as $index => $post) {
            if (empty($activities[$post['targetId']])) {
                unset($posts[$index]);
            }
        }
        $posts = array_slice($posts, 0, $arguments['count']);
        $userIds = ArrayToolkit::column($posts, 'userId');

        $owners = $this->getUserService()->findUsersByIds($userIds);

        foreach ($posts as $key => $post) {
            $posts[$key]['user'] = $owners[$post['userId']];
            $posts[$key]['activity'] = $activities[$post['targetId']];
        }

        return $posts;
    }


    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->getServiceKernel()->createService('CustomBundle:Community:CommunityService');
    }

    protected function getUserService()
    {
        return ServiceKernel::instance()->createService('User:UserService');
    }

    /**
     * @return CommunityActivityService
     */
    protected function getCommunityActivityService()
    {
        return ServiceKernel::instance()->createService('CustomBundle:CommunityActivity:CommunityActivityService');
    }

    /**
     * @return ThreadService
     */
    private function getThreadService()
    {
        return $this->getServiceKernel()->createService('Thread:ThreadService');
    }
}