<?php

namespace CustomBundle\Extensions\DataTag;


use CustomBundle\Biz\Community\Service\CommunityService;
use CustomBundle\Biz\User\Service\Impl\UserServiceImpl;

class CommunityDataTag extends CustomDataTag
{
    public function getData(array $arguments)
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['provice'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        return $community;
    }

    /**
     * @return  CommunityService
     */
    protected function getCommunityService()
    {
        return $this->getServiceKernel()->createService('CustomBundle:Community:CommunityService');
    }

    /**
     * @return UserServiceImpl
     */
    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('CustomBundle:User:Userservice');
    }
}