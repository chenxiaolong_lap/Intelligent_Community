<?php


namespace CustomBundle\Extensions\DataTag;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Extensions\DataTag\BaseDataTag;
use AppBundle\Extensions\DataTag\DataTag;
use CustomBundle\Biz\CommunityAnnouncement\Service\CommunityAnnouncementService;
use Biz\Group\Service\ThreadService;
use Topxia\Service\Common\ServiceKernel;

class PopularCommunityAnnouncementPostsDataTag extends BaseDataTag implements DataTag
{
    /**
     * 获取个人动态
     *
     *   count    必需
     *
     * @param array $arguments 参数
     *
     * @return array 个人动态
     */
    public function getData(array $arguments)
    {
        $community = $this->getCommunity();
        $limitCount = $arguments['count'] * 2;
        $posts = $this->getThreadService()->searchPosts(
            array(
                'targetType' => 'community-announcement',
                'parentId' => 0,
                'latest' => 'week',
            ),
            array('ups' => 'DESC', 'createdTime' => 'DESC'),
            0,
            $limitCount
        );
        if ($limitCount > count($posts)) {
            $conditions = array(
                'targetType' => 'community-announcement',
            );

            $excludeIds = ArrayToolkit::column($posts, 'id');
            if (!empty($excludeIds)) {
                $conditions['excludeIds'] = $excludeIds;
            }

            $otherPosts = $this->getThreadService()->searchPosts(
                $conditions,
                array('ups' => 'DESC', 'createdTime' => 'DESC'),
                0,
                $limitCount - count($posts)
            );

            $posts = array_merge($posts, $otherPosts);
        }

        $announcementIds = ArrayToolkit::column($posts, 'targetId');
        $announcements = $this->getCommunityAnnouncementService()->searchCommunityAnnouncement(
            array(
                'ids' => $announcementIds,
                'communityId' => $community['id']
            ),
            array(),
            0,
            PHP_INT_MAX
        );
        $announcements = ArrayToolkit::index($announcements, 'id');

        foreach ($posts as $index => $post) {
            if (empty($announcements[$post['targetId']])) {
                unset($posts[$index]);
            }
        }
        $posts = array_slice($posts, 0, $arguments['count']);
        $userIds = ArrayToolkit::column($posts, 'userId');

        $owners = $this->getUserService()->findUsersByIds($userIds);

        foreach ($posts as $key => $post) {
            $posts[$key]['user'] = $owners[$post['userId']];
            $posts[$key]['announcement'] = $announcements[$post['targetId']];
        }

        return $posts;
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->getServiceKernel()->createService('CustomBundle:Community:CommunityService');
    }

    protected function getUserService()
    {
        return ServiceKernel::instance()->createService('User:UserService');
    }

    /**
     * @return CommunityAnnouncementService
     */
    protected function getCommunityAnnouncementService()
    {
        return ServiceKernel::instance()->createService('CustomBundle:CommunityAnnouncement:CommunityAnnouncementService');
    }

    /**
     * @return ThreadService
     */
    private function getThreadService()
    {
        return $this->getServiceKernel()->createService('Thread:ThreadService');
    }
}