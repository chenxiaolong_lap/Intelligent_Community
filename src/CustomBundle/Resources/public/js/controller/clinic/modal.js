define(function(require, exports, module) {
  
  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  var Notify = require('common/bootstrap-notify');
  
  exports.run = function () {
    var $modal = $('#clinic-modal-form').parents('.modal');
  
    var validator = new Validator({
      element: '#clinic-modal-form',
      autoSubmit: false,
      failSilently: true,
      onFormValidated: function(error, results, $form) {
        if (error) {
          return false;
        }
        $('#edit-housing-estate-btn').button('loading');
      
        $.post($form.attr('action'), $form.serialize(), function(resp) {
          if(resp.status){
            $modal.modal('hide');
          
            Notify.success(Translator.trans('诊所信息保存成功'));
          
            window.location.reload();
          }else{
            Notify.danger(resp.message);
          
            $('#edit-housing-estate-btn').button('reset');
          }
        }).error(function(){
          Notify.danger(Translator.trans('操作失败'));
          $('#edit-housing-estate-btn').button('reset');
        });
      }
    });
  }
});