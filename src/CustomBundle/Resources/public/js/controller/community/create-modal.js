define(function(require, exports, module) {
  
  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  var Notify = require('common/bootstrap-notify');
  require('es-ckeditor');
  require('../libs/distpicker.js');
  var AutoComplete = require('autocomplete');
  
  exports.run = function () {
    $(".distpicker").distpicker({
      province: "省份",
      city: "城市",
      district: "市区"
    });
    
    var $modal = $('#community-create-form').parents('.modal');
    
    var validator = new Validator({
      element: '#community-create-form',
      autoSubmit: false,
      failSilently: true,
      onFormValidated: function(error, results, $form) {
        if (error) {
          return false;
        }
        $('#create-community-btn').button('loading');
        
        $.post($form.attr('action'), $form.serialize(), function(resp) {
          if(resp.status){
            $modal.modal('hide');
            
            Notify.success(Translator.trans('社区信息保存成功'));
            
            window.location.reload();
          }else{
            Notify.danger(resp.message);
            
            $('#create-community-btn').button('reset');
          }
        }).error(function(){
          Notify.danger(Translator.trans('操作失败'));
          $('#create-community-btn').button('reset');
        });
      }
    });
    
    // validator.on('formValidate', function(elemetn, event) {
    //   editor.updateElement();
    // });
    
    validator.addItem({
      element: '[name="name"]',
      rule: 'chinese_alphanumeric byte_minlength{min:2} byte_maxlength{max:36}'
    });
    
    validator.addItem({
      element: '[name="province"]',
      required: true,
      errormessage: '请输入社区名称',
    });
    
    validator.addItem({
      element: '[name=address]',
      required: true,
      errormessage: '请输入社区服务中心地址',
    });
    
    validator.addItem({
      element: '[name="officePhone"]',
      required: true,
      rule: 'officePhoneRule',
    });
    
    Validator.addRule("officePhoneRule", function(options) {
      var value = $(options.element).val();
      var reg = /^[0-9]{3,4}-?[0-9]{7,8}$/;
      
      return reg.test(value);
    }, '电话号码不规范')
  }
});