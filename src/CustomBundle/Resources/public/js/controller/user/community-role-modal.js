define(function(require, exports, module) {
  
  var Notify = require('common/bootstrap-notify');
  
  exports.run = function() {
    var $form = $("#community-role-form");
    
    var $modal = $form.parents('.modal');
    
    $form.on('submit', function() {
      if($form.find(':checkbox:checked').length <= 0){
        Notify.danger('请为用户选择社区角色！');
        return false;
      }
      
      $('#community-role-btn').button('loading');
      
      $.post($form.attr('action'), $form.serialize(), function(html) {
        $modal.modal('hide');
        Notify.success(Translator.trans('用户社区角色设置成功！'));
      }).error(function(){
        Notify.danger(Translator.trans('用户社区角色设置失败！'));
        $('#community-role-btn').button('reset');
      });
      
      return false;
    });
  };
});