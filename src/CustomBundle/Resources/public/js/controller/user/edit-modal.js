define(function(require, exports, module) {

  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  var Notify = require('common/bootstrap-notify');
  require('es-ckeditor');
  require('../libs/distpicker.js');
  var AutoComplete = require('autocomplete');

  exports.run = function() {

    var $form = $('#user-edit-form');

    // group: 'course'
    var editor = CKEDITOR.replace('about', {
      toolbar: 'Simple',
      filebrowserImageUploadUrl: $('#about').data('imageUploadUrl')
    });

    var $modal = $('#user-edit-form').parents('.modal');

    var validator = new Validator({
      element: '#user-edit-form',
      autoSubmit: false,
      failSilently: true,
      onFormValidated: function(error, results, $form) {
        if (error) {
          return false;
        }
        $('#edit-user-btn').button('loading');

        $.post($form.attr('action'), $form.serialize(), function(resp) {
          if(resp.success){
            $modal.modal('hide');

            Notify.success(Translator.trans('用户信息保存成功'));

            window.location.reload();
          }else{
            Notify.danger(resp.message);

            $('#edit-user-btn').button('reset');
          }
        }).error(function(){
          Notify.danger(Translator.trans('操作失败'));
          $('#edit-user-btn').button('reset');
        });
      }
    });

    validator.on('formValidate', function(elemetn, event) {
      editor.updateElement();
    });

    validator.addItem({
      element: '[name="truename"]',
      rule: 'chinese_alphanumeric byte_minlength{min:4} byte_maxlength{max:36}'
    });

    validator.addItem({
      element: '[name="qq"]',
      rule: 'qq'
    });

    validator.addItem({
      element: '[name="weibo"]',
      rule: 'url',
      errormessageUrl: Translator.trans('网站地址不正确，须以http://weibo.com开头。')
    });

    validator.addItem({
      element: '[name="site"]',
      rule: 'url',
      errormessageUrl: Translator.trans('网站地址不正确，须以http://或https://开头。')
    });

    validator.addItem({
      element: '[name="mobile"]',
      rule: 'phone'
    });

    validator.addItem({
      element: '[name="idcard"]',
      rule: 'idcard'
    });

    for(var i=1;i<=5;i++){
      validator.addItem({
        element: '[name="intField'+i+'"]',
        rule: 'int'
      });

      validator.addItem({
        element: '[name="floatField'+i+'"]',
        rule: 'float'
      });

      validator.addItem({
        element: '[name="dateField'+i+'"]',
        rule: 'date'
      });
    }

    validator.addItem({
      element: '[name="company"]',
      rule: 'chinese_limit{max:200}'
    });

    validator.addItem({
      element: '[name="province"]',
      required: true,
      display: '省份'
    });

    validator.addItem({
      element: '[name="job"]',
      errormessage: '请填写职位'
    });

    var communityAutoComplete = new AutoComplete({
      trigger: '#community',
      dataSource: $("#community").data('url'),
      filter: {
        name: 'stringMatch',
        options: {
          key: 'name'
        }
      },
      selectFirst: true
    }).render();

    var housingEstateAutoComplete = new AutoComplete({
      trigger: '#housingEstate',
      dataSource: $("#housingEstate").data('url'),
      filter: {
        name: 'stringMatch',
        options: {
          key: 'name'
        }
      },
      selectFirst: true
    }).render();
  
    var addressAutoComplete = new AutoComplete({
      trigger: '#address',
      dataSource: $("#address").data('url'),
      filter: {
        name: 'stringMatch',
        options: {
          key: 'address'
        }
      },
      selectFirst: true
    }).render();

    $(".distpicker").distpicker({
      province: "省份",
      city: "城市",
      district: "市区"
    });
  };
});