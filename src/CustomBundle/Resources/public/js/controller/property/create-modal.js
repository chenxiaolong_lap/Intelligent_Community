define(function(require, exports, module) {
  
  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  var Notify = require('common/bootstrap-notify');
  require('es-ckeditor');
  require('../libs/distpicker.js');
  require("jquery.bootstrap-datetimepicker");
  var AutoComplete = require('autocomplete');
  
  exports.run = function () {
    $(".distpicker").distpicker({
      province: "省份",
      city: "城市",
      district: "市区"
    });
    
    var directorId = 1;
    var $modal = $('#property-create-form').parents('.modal');
    var $form = $('#property-create-form');
    
    var validator = new Validator({
      element: '#property-create-form',
      autoSubmit: false,
      failSilently: true,
      onFormValidated: function(error, results, $form) {
        if (error) {
          return false;
        }
        $('#create-property-btn').button('loading');
        
        $.post($form.attr('action'), $form.serialize(), function(resp) {
          if(resp.status){
            $modal.modal('hide');
            
            Notify.success(Translator.trans('小区物业公司创建成功'));
            
            window.location.reload();
          }else{
            Notify.danger(resp.message);
            
            $('#create-property-btn').button('reset');
          }
        }).error(function(){
          Notify.danger(Translator.trans('操作失败'));
          $('#create-property-btn').button('reset');
        });
      }
    });
    
    validator.addItem({
      element: '[name="name"]',
      required: true,
      rule: 'chinese_alphanumeric byte_minlength{min:2} byte_maxlength{max:36}'
    });
    
    validator.addItem({
      element: '[name="servicePhone"]',
      required: true,
      rule: 'officePhoneRule',
    });
    
    validator.addItem({
      element: '[name="housingEstateName"]',
      required: true,
      rule: 'chinese_alphanumeric byte_minlength{min:2} byte_maxlength{max:36}'
    });
    
    validator.addItem({
      element: '[name="arrivalTime"]',
      required: true,
    });
    
    validator.addItem({
      element: '[name="expiryTime"]',
      required: true
    });
    
    // validator.addItem({
    //   element: `[name^="director"]`,
    //   required: true,
    //   rule: 'chinese_limit{max:200} director_equals remote',
    // });
    
    Validator.addRule("officePhoneRule", function(options) {
      var value = $(options.element).val();
      var reg = /^[0-9]{3,4}-?[0-9]{7,8}$/;
      
      return reg.test(value);
    }, '电话号码不规范');
  
    Validator.addRule("director_equals", function(options) {
      let bool = true;
      let element = options.element;
      $form.find('.director').each(function(i, e){
        if (element.attr('name') == $(e).attr('name')) {
          return true;
        }
      
        if ($(element).val() == $(e).val()) {
          return bool = false;
        }
      });
      return bool;
    }, '用户名不能相同');
  
    function initDatePicker($id) {
      let $picker = $($id);
      $picker.datetimepicker({
        format: 'yyyy-mm-dd',
        language: document.documentElement.lang,
        minView: 3, //month
        autoclose: true,
        endDate: new Date(Date.now() + 86400 * 365 * 10 * 1000)
      });
      $picker.datetimepicker('setStartDate', new Date());
    }
    
    initDatePicker('#arrivalTime');
    initDatePicker('#expiryTime');
  
    var housingEstateAutoComplete = new AutoComplete({
      trigger: '#housingEstateName',
      dataSource: $("#housingEstateName").data('url'),
      filter: {
        name: 'stringMatch',
        options: {
          key: 'name'
        }
      },
      selectFirst: true
    }).render();
  
    var nicknameAutoComplete = new AutoComplete({
      trigger: '[id="director[1]"]',
      dataSource: $("[id='director[1]']").data('url'),
      filter: {
        name: 'stringMatch',
        options: {
          key: 'nickname'
        }
      },
      selectFirst: true
    }).render();
    
    function nicknameMatch(directorId) {
      new AutoComplete({
        trigger: `[id="director[${directorId}]"]`,
        dataSource: $(`[id='director[${directorId}]']`).data('url'),
        filter: {
          name: 'stringMatch',
          options: {
            key: 'nickname'
          }
        },
        selectFirst: true
      }).render();
    }
    
    $form.on('click', '.add-property-director', function () {
      let length = $form.find('.director').length;
      if (length >= 10) {
        Notify.danger('物业管理人员最多添加10个人！');
        return false;
      }
      
      directorId++;
      $form.find('.director:last')
        .closest('.form-group')
        .after(appendHtml(directorId));
  
      //addItemValidator(`name="director[${directorId}]"`);
      nicknameMatch(directorId);
    });
  
    $form.on('click', '.delete-property-director', function () {
      if (!confirm('确认要删除吗！')) {
        return false;
      }
      
      let name = $(this).attr('name');
      let $element = $(this).closest('.form-group');
      validator.removeItem(`[name='${name}]'`);
      $element.remove();
    });
  
    function addItemValidator(element) {
      validator.addItem({
        element: element,
        required: true,
        rule: 'chinese_limit{max:200} director_equals remote',
        display: '物业管理人员'
      });
    }
    
    function appendHtml(directorId) {
      let html = `<div class="form-group">
            <div class="col-md-3 control-label"><label class="hidden">物业管理人员</label></div>
            <div class="col-sm-6 controls">
              <input type="text" placeholder="请输入用户名" id="director[${directorId}]" name="director[${directorId}]" class="form-control control-room director" value="" data-url="/fill/userinfo/nickname/match?q={{query}}" data-role="item-input">
            </div>
            <a type="button" class="delete-property-director" href="javascript:;" data-url="">删除</a>
          </div>`;
  
      return html;
      
    }
  }
});