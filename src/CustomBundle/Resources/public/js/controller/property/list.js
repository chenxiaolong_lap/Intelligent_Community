define(function(require, exports, module) {
  
  var Notify = require('common/bootstrap-notify');
  require("jquery.bootstrap-datetimepicker");
  var validator = require('bootstrap.validator');
  require('../libs/distpicker.js');
  exports.run = function () {
    $(".distpicker").distpicker({
      provinceQuery: "省份",
      cityQuery: "城市",
      districtQuery: "市区"
    });
  }
  
});