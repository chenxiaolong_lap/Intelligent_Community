define(function(require, exports, module) {
  
  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  var Notify = require('common/bootstrap-notify');
  require('es-ckeditor');
  require('../libs/distpicker.js');
  var AutoComplete = require('autocomplete');
  
  exports.run = function () {
    
    var $modal = $('#housing-estate-edit-form').parents('.modal');
    
    var validator = new Validator({
      element: '#housing-estate-edit-form',
      autoSubmit: false,
      failSilently: true,
      onFormValidated: function(error, results, $form) {
        if (error) {
          return false;
        }
        $('#edit-housing-estate-btn').button('loading');
        
        $.post($form.attr('action'), $form.serialize(), function(resp) {
          if(resp.status){
            $modal.modal('hide');
            
            Notify.success(Translator.trans('小区信息保存成功'));
            
            window.location.reload();
          }else{
            Notify.danger(resp.message);
            
            $('#edit-housing-estate-btn').button('reset');
          }
        }).error(function(){
          Notify.danger(Translator.trans('操作失败'));
          $('#edit-housing-estate-btn').button('reset');
        });
      }
    });
    
    $('#delete-housing-estate-btn').click(function () {
      
      if (!confirm('删除小区，请确保小区下没有用户信息，物业管理公司信息，确认要删除吗？')) {
        return false
      }
      $(this).button('loading');
      $.post($(this).data('url'), {}, function (data) {
        if (data.status) {
          $modal.modal('hide');
          
          Notify.success('删除小区成功！');
          
          window.location.reload();
        } else {
          Notify.danger('删除失败！');
          $(this).button('reset');
        }
      }).error(function () {
        Notify.danger(Translator.trans('操作失败'));
        $('#edit-community-btn').button('reset');
      })
    });
    
    // validator.on('formValidate', function(elemetn, event) {
    //   editor.updateElement();
    // });
    
    validator.addItem({
      element: '[name="name"]',
      rule: 'chinese_alphanumeric byte_minlength{min:2} byte_maxlength{max:36}'
    });
    
    
    validator.addItem({
      element: '[name=address]',
      required: true,
      errormessage: '请输入小区详细地址',
    });
    
    var communityAutoComplete = new AutoComplete({
      trigger: '#communityName',
      dataSource: $("#communityName").data('url'),
      filter: {
        name: 'stringMatch',
        options: {
          key: 'name'
        }
      },
      selectFirst: true
    }).render();
  }
});