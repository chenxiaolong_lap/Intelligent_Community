import notify from 'common/notify';

export default class Activity {
  constructor(options) {
    this.validator = null;
    this.$element = $('#activity-form');
    this.$defaultFormData = this.$element.serialize();
    
    this.initEditor();
    this.initPopover();
    this.initValidator();
    this.alertFrom();
    this.initDate();
    this.addValidatorMethod();
    this.initSelect2();
    this.initInvited();
  }
  
  alertFrom() {
    let self = this;
    $(window).bind('beforeunload', function () {
      if (self.$defaultFormData != self.$element.serialize()) {
        return Translator.trans('还有没有保存的数据,是否要离开此页面?');
      }
    });
  }
  
  initDate() {
    this.datatimepick($('#startTime'));
    this.datatimepick($('#endTime'));
  }
  
  initValidator() {
    let self = this;
    this.validator = this.$element.validate({
      rules: {
        'remark': {
          required: true
        },
        'content': {
          required: true
        },
        'title': {
          required: true,
          chinese_limit: 255,
        },
        'site': {
          required: true,
          chinese_limit: 255,
        },
        'startTime': {
          required: true
        },
        'endTime': {
          required: true,
          dateLimit: true,
        }
      },
      messages: {
        'site': {
          chinese_limit: "只能输入255个字",
        },
        'title': {
          chinese_limit: "只能输入255个字",
        },
      },
      submitHandler: form => {
        $(window).unbind('beforeunload');
        if ($(form).valid()) {
          $('#activity-operate-save').button('loading');
          form.submit();
        }
      }
    });
  }
  
  initEditor() {
    let self = this;
    let editor = CKEDITOR.replace('content', {
      toolbar: 'SimpleMini',
      allowedContent: true,
    });

    var editor2 = CKEDITOR.replace('remark', {
      toolbar: 'Detail',
      allowedContent: true,
      resize_enabled: true,
      disableObjectResizing: true,
      filebrowserImageUploadUrl: $('#remark').data('imageUploadUrl'),
      filebrowserFlashUploadUrl: $('#remark').data('flashUploadUrl'),
    });
  
    editor.on('change', () => {
      $('#content').val(editor.getData());
    });
  
    editor2.on('change', () => {
      $('#remark').val(editor2.getData());
    });
  }
  
  initPopover() {
    $("#article-property-tips").popover({
      html: true,
      trigger: 'hover',
      placement: 'left',
      content: $("#article-property-tips-html").html()
    });
  };
  
  initInvited() {
    let self = this;
    
    $('#invited-all').click(function () {
      console.log($(this).is(':checked'));
      if ($(this).is(':checked') == true) {
        $('#user-invited-list input').each(function () {
          $(this).attr('checked', true);
          console.log(1);
        })
      } else {
        $('#user-invited-list input').each(function () {
          $(this).attr('checked', false);
          console.log(0);
        })
      }
    })
  }
  
  addValidatorMethod() {
    let self = this;
    
    $.validator.addMethod('dateLimit', function (value, element, params) {
      let bool = true;
      let startTime = self.conversionDateString($('#startTime').val());
      let endTime = self.conversionDateString($(element).val());
      
      if (endTime != '' && startTime != '' && endTime <= startTime) {
        bool = false
      }
      return bool;
    }, '结束时间不能比开始时间晚！');
    
    
  }
  
  datatimepick($e) {
    $e.datetimepicker({
      startDate: new Date(Date.now()),
      autoclose: true,
      format: 'yyyy-mm-dd h:i',
      minuteStep: 15,//步进
      minView: 'hour',
      language: document.documentElement.lang,
      endDate: new Date(Date.now() + 86400 * 365 * 10 * 1000)
    });
  }
  
  conversionDateString(dateString) {
    let time = Date.parse(dateString);
    time = time / 1000;
    return time;
  }
  
  initSelect2() {
    $('#search-user').select2({
      ajax: {
        url: $('#search-user').data('url'),
        dataType: 'json',
        quietMillis: 100,
        data(term, page) {
          return {
            q: term,
            page_limit: 10
          };
        },
        results(data) {
          console.log(data);
          return {
            results: data.map((item) => {
              return {id: item.name, name: item.name};
            })
          };
        }
      },
      initSelection(element, callback) {
        const data = [];
        $(element.val().split(',')).each(function () {
          data.push({
            id: this,
            name: this
          });
        });
        callback(data);
      },
      formatSelection(item) {
        return item.name;
      },
      formatResult(item) {
        return item.name;
      },
      formatSearching: function () {
        return Translator.trans('site.searching_hint');
      },
      multiple: true,
      maximumSelectionSize: 10,
      width: 'off',
      createSearchChoice() {
        return null;
      }
    })
  }
}

new Activity();