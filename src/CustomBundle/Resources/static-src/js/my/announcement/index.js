import notify from 'common/notify';
import 'app/common/local-image/upload';

export default class CreateAnnouncement {
  constructor(options) {
    this.validator = null;
    this.$element = $('#announcement-form');
    
    this.initDate();
    this.initEditor();
    this.addValidatorMethod();
    this.initValidator();
  }
  
  initDate() {
    $("#startTime").datetimepicker({
      autoclose: true,
      minuteStep: 30,
      format: 'yyyy-mm-dd hh:ii',
      minView: 'hour',
      language: document.documentElement.lang,
      endDate: new Date(Date.now() + 86400 * 365 * 10 * 1000)
    });
    $('#startTime').datetimepicker('setStartDate', new Date);
    
    $("#endTime").datetimepicker({
      autoclose: true,
      minuteStep: 30,
      format: 'yyyy-mm-dd hh:ii',
      minView: 'hour',
      language: document.documentElement.lang,
      endDate: new Date(Date.now() + 86400 * 365 * 10 * 1000)
    });
    $('#endTime').datetimepicker('setStartDate', new Date);
  }
  
  initEditor() {
    var editor = CKEDITOR.replace('body', {
      toolbar: 'Detail',//SimpleMini
      height: 300,
      resize_enabled: true,
      disableObjectResizing: true,
      filebrowserImageUploadUrl: $('#body').data('imageUploadUrl')
    });
  
    editor.on('change', () => {
      $('#body').val(editor.getData());
    });
  }
  
  addValidatorMethod() {
    let self = this;
    
    $.validator.addMethod('dateLimit', function (value, element, params) {
      let bool = true;
      let startTime = self.conversionDateString($('#startTime').val());
      let endTime = self.conversionDateString($(element).val());
      
      if (endTime != '' && startTime != '' && endTime <= startTime) {
        bool = false
      }
      return bool;
    }, '结束时间不能比开始时间晚！');
  }
  
  initValidator() {
    this.validator = this.$element.validate({
      rules: {
        'body': {
          required: true,
        },
        'title': {
          required: true,
          chinese_limit: 50,
        },
        'startTime': {
          required: true
        },
        'endTime': {
          required: true,
          dateLimit: true
        }
      },
      messages: {
        'body': {
          required: '请输入公告内容'
        },
        'title': {
          required: '请输入公告标题'
        }
      },
      submitHandler: form => {
        if ($(form).valid()) {
          $.post($(form).attr('action'), $(form).serialize(), resp => {
            if (resp.status) {
              notify('success', '操作成功');
              window.location.reload();
            } else {
              notify('danger', resp.message, 3000);
            }
          }).error(function (data) {
            notify('danger', JSON.parse(data.responseText).error.message);
          });
        }
      }
    })
  }
  
  conversionDateString(dateString) {
    let time = Date.parse(dateString);
    time = time / 1000;
    return time;
  }
}

new CreateAnnouncement();

