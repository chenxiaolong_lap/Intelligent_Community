import distpicker from 'custombundle/libs/js/distpicker';
import notify from 'common/notify';

export default class Setting {
  constructor(options) {
    this.validator = null;
    this.$element = $('#setting-community-form');
  
    this.addValidatorMethod();
    this.initDistpicker();
    this.initValidator();
  }
  
  initValidator() {
    this.validator = this.$element.validate({
      rules: {
        'name': {
          required: true,
          chinese_limit: 64
        },
        'province': {
          required: true,
        },
        'officePhone': {
          officePhoneRule: true,
          required: true
        },
        'address': {
          required: true,
          chinese_limit: 255,
        }
      },
      messages: {
        'name': {
          required: '请输入社区名称',
        },
        'province': {
          required: '请选择省',
        },
        'officePhone': {
          required: '请输入服务电话',
        },
        'address': {
          required: '请输入服务地址',
        }
      },
      submitHandler: form => {
        if ($(form).valid()) {
          $('#setting-submit-btn').button('loading');
          form.submit();
        }
      }
    })
  }
  
  initDistpicker() {
    $(".distpicker").distpicker({
      province: "省份",
      city: "城市",
      district: "市区"
    });
  }
  
  addValidatorMethod() {
    $.validator.addMethod("officePhoneRule", function(value, element, params) {
      var reg = /^[0-9]{3,4}-?[0-9]{7,8}$/;
      
      return reg.test(value);
    }, '电话号码不规范')
  }
}

new Setting();