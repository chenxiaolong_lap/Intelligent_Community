import EsImageCrop from 'common/es-image-crop.js';
import notify from 'common/notify';

class CoverCrop {
  constructor() {
    this.init();
  }
  
  init() {
    let imageCrop = new EsImageCrop({
      element: "#picture-crop",
      group: 'community',
      cropedWidth: 1440,
      cropedHeight: 400
    });
    imageCrop.afterCrop = function(response) {
      let url = $("#upload-slideshow-picture-btn").data("url");
      $.post(url, { images: response }, function(data) {
        if (data.status) {
          notify('success', '图片保存成功', 2000);
          $('#modal').modal('hide');
          $('#upload-picture-btn').prev().html(data.slideshow.filePath);
          $('.slideshow-picture').find('a').attr('href', data.slideshow.fileUrl);
          $('.slideshow-picture').find('img').attr('src', data.slideshow.filePath);
        } else {
          notify('danger', '图片保存失败');
          $("#upload-slideshow-picture-btn").button('reset');
        }
      }, 'json').error(function () {
        notify('danger', '操作失败');
        $("#upload-slideshow-picture-btn").button('reset');
      });
    };
    
    $("#upload-slideshow-picture-btn").click(function(event) {
      event.stopPropagation();
      $(event.currentTarget).button('loading');
      imageCrop.crop({
        imgs: {
          picture: [1140, 400],
        }
      });
      
    })
    
    $('.go-back').click(function() {
      history.go(-1);
    });
  }
}


new CoverCrop();

