import EsImageCrop from 'common/es-image-crop.js';
import notify from 'common/notify';

class CoverCrop {
  constructor() {
    this.init();
  }
  
  init() {
    let imageCrop = new EsImageCrop({
      element: "#article-pic-crop",
      group: 'article',
      cropedWidth: 754,
      cropedHeight: 424
    });
    imageCrop.afterCrop = function(response) {
      let url = $("#upload-picture-crop-btn").data("gotoUrl");
      $.post(url, { images: response }, function(data) {
        console.log(data);
          notify('success', '图片保存成功', 2000);
          $('#modal').modal('hide');
          $("#article-thumb-container").show();
          $("#article_thumb_remove").removeClass('hidden');
          $("#article-thumb").val(data.large.file.uri);
          $("#article-originalThumb").val(data.large.file.uri);
          $('#article-thumb-preview').attr('src',data.large.file.url);
          $("#article-thumb-container").html("<img class='img-responsive' src='"+data.large.file.url+"'>")
      }, 'json').error(function () {
        notify('danger', '操作失败');
        $("#upload-picture-crop-btn").button('reset');
      });
    };
    
    $("#upload-picture-crop-btn").click(function(event) {
      event.stopPropagation();
      $(event.currentTarget).button('loading');
      imageCrop.crop({
        imgs: {
          large: [754, 424]
        }
      });
      
    })
    
    $('.go-back').click(function() {
      history.go(-1);
    });
  }
}


new CoverCrop();

