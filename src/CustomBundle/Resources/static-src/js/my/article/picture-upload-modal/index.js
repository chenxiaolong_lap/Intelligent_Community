import EsWebUploader from 'common/es-webuploader.js';
import notify from 'common/notify';

new EsWebUploader({
  element: '#article-upload-btn',
  onUploadSuccess: function(file, response) {
    let url = $("#article-upload-btn").data("gotoUrl");
    
    $('#modal').load(url);
  }
});