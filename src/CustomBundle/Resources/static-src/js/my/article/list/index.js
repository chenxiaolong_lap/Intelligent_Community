import notify from 'common/notify';

export default class Article {
  constructor(options) {
    
    this.initPopover();
    this.initGroupBtn();
    this.initLabel();
    this.initBatchDelete();
  }
  
  initPopover() {
    $("#article-property-tips").popover({
      html: true,
      trigger: 'hover',
      placement: 'left',
      content: $("#article-property-tips-html").html()
    });
  }
  
  initGroupBtn() {
    $("#article-table").on('click', '[data-role=trash-item]', function(){
      $.post($(this).data('url'), function(){
        window.location.reload();
      });
    });
  
    $("#article-table").on('click', '[data-role=publish-item]', function(){
      $.post($(this).data('url'), function(){
        window.location.reload();
      });
    });
  
    $("#article-table").on('click', '[data-role=unpublish-item]', function(){
      $.post($(this).data('url'), function(){
        window.location.reload();
      });
    });
  
    $("#article-table").on('click', '[data-role=delete-item]', function(){
      if (!confirm(Translator.trans('真的要永久删除该内容吗？'))) {
        return ;
      }
      $.post($(this).data('url'),{id: $(this).data('id')}, function(data){
        if (data.status) {
          window.location.reload();
        } else {
          notify('danger', '删除失败')
        }
      }).error(function () {
        notify('danger', '操作失败')
      });
    });
  }
  
  initLabel() {
    $(".featured-label").on('click', function() {
      var $self = $(this);
      var span = $self.find('span');
      var spanClass = span.attr('class');
      var postUrl;
    
      if(spanClass == "label label-default"){
        postUrl = $self.data('setUrl');
        $.post(postUrl, function(response) {
          var labelStatus = "label label-success";
          span.attr('class',labelStatus)
        });
      }else{
        postUrl = $self.data('cancelUrl');
        $.post(postUrl, function(response) {
          var labelStatus = "label label-default";
          span.attr('class',labelStatus)
        });
      }
    });
  
    $(".promoted-label").on('click', function(){
    
      var $self = $(this);
      var span = $self.find('span');
      var spanClass = span.attr('class');
      var postUrl = "";
    
      if(spanClass == "label label-default"){
        postUrl = $self.data('setUrl');
        $.post(postUrl, function(response) {
          var labelStatus = "label label-success";
          span.attr('class',labelStatus)
        });
      }else{
        postUrl = $self.data('cancelUrl');
        $.post(postUrl, function(response) {
          var labelStatus = "label label-default";
          span.attr('class',labelStatus)
        });
      }
    
    });
  
    $(".sticky-label").on('click', function(){
    
      var $self = $(this);
      var span = $self.find('span');
      var spanClass = span.attr('class');
      var postUrl = "";
    
      if(spanClass == "label label-default"){
        postUrl = $self.data('setUrl');
        $.post(postUrl, function(response) {
          var labelStatus = "label label-success";
          span.attr('class',labelStatus)
        });
      }else{
        postUrl = $self.data('cancelUrl');
        $.post(postUrl, function(response) {
          var labelStatus = "label label-default";
          span.attr('class',labelStatus)
        });
      }
    });
  }
  
  initBatchDelete() {
    let $container = $('#aticle-table-container');
    this.batchSelect($container);
    this.batchDelete($container);
    this.itemDelete($container);
  }
  
  batchSelect($element) {
    $element.on('click', '[data-role=batch-select]', function(){
      if( $(this).is(":checked") == true){
        $element.find('[data-role=batch-select], [data-role=batch-item]').prop('checked', true);
      } else {
        $element.find('[data-role=batch-select], [data-role=batch-item]').prop('checked', false);
      }
    });
  
    $element.on('click', '[data-role=batch-item]', function(){
    
      var length = $element.find('[data-role=batch-item]').length;
      var checked_count = 0;
      $element.find('[data-role=batch-item]').each(function(){
        if ($(this).is(":checked")) {
          checked_count++;
        };
      })
    
      if (checked_count == length){
        $element.find('[data-role=batch-select]').prop('checked',true);
      } else {
        $element.find('[data-role=batch-select]').prop('checked',false);
      }
    
    })
  }
  
  batchDelete($element) {
    $element.on('click', '[data-role=batch-delete]', function() {
    
      var $btn = $(this);
      name = $btn.data('name');
    
      var ids = [];
      $element.find('[data-role=batch-item]:checked').each(function(){
        ids.push(this.value);
      });
    
      if (ids.length == 0) {
        notify('danger', `未选中任何${name}`, 2000);
        return ;
      }
    
      if (!confirm(Translator.trans('确定要删除选中的%ids%条%name%吗？',{ids:ids.length,name:name}))) {
        return ;
      }
    
      $element.find('.btn').addClass('disabled');
    
      notify('info', `正在删除${name}，请稍等`, 2000);
    
      $.post($btn.data('url'), {ids:ids}, function(){
        window.location.reload();
      });
    
    });
  }
  
  itemDelete($element) {
    $element.on('click', '[data-role=item-delete]', function() {
      var $btn = $(this),
        name = $btn.data('name'),
        message = $btn.data('message');
    
      if (!message) {
        message = Translator.trans('真的要删除该%name%吗？',{name:name});
      }
    
      if (!confirm(message)) {
        return ;
      }
    
      $.post($btn.data('url'), function() {
        if ($.isFunction(onSuccess)) {
          onSuccess.call($element, $item);
        } else {
          $btn.parents('[data-role=item]').remove();
          notify('success', `删除${name}%成功`, 2000);
        }
      });
    });
  }
}

new Article();