import 'libs/jquery-validation.js';
import notify from 'common/notify';
import 'libs/select2.js';

export default class Article {
  constructor(options) {
    this.validator = null;
    this.$element = $('#article-form');
    this.$defaultFormData = this.$element.serialize();
    
    this.initEditor();
    this.initPopover();
    this.initTagSelect();
    this.initValidator();
    this.alertFrom();
    this._addMethodRules()
  }
  
  alertFrom() {
    let self = this;
    $(window).bind('beforeunload',function(){
      self.$element.data('serialize', self.$element.serialize());
      if (self.$defaultFormData !== self.$element.data('serialize')) {
        return Translator.trans('还有没有保存的数据,是否要离开此页面?');
      }
    });
  }
  
  initValidator() {
    let self = this;
    this.validator = this.$element.validate({
      rules: {
        'body': {
          updateEditor: true,
          required: true
        },
        'categoryId': {
          selectNotNull: true
        },
        'title': {
          required: true,
          chinese_limit: 255,
        },
        'sourceUrl': {
          url: true
        },
        'source': {
          chinese_limit: 255,
        },
        'tags': {
          required: true
        }
      },
      messages: {
        'sourceUrl': {
          url: '网址以http://开头'
        },
        'tags': {
          required: '请输入标签'
        }
      },
      submitHandler: form => {
        $(window).unbind('beforeunload');
        if ($(form).valid()) {
          $('#article-operate-save').button('loading');
          form.submit();
        }
      }
    });
  }
  
  _addMethodRules () {
    $.validator.addMethod('selectNotNull', function (value, element, params) {
      console.log(value);
      if (value == 0) {
        return false;
      }
      
      return true
    }, '请选择栏目')
  }
  
  initEditor() {
    let self = this;
    var ckeditor = CKEDITOR.replace('body', {
      toolbar: 'Full',
      allowedContent: true,
      resize_enabled: true,
      disableObjectResizing: true,
      filebrowserImageUploadUrl: $('#body').data('imageUploadUrl'),
      filebrowserFlashUploadUrl: $('#body').data('flashUploadUrl'),
      height: 400
    });
    
    ckeditor.on('change', () => {
      $('#body').val(ckeditor.getData());
    });
  
    $.validator.addMethod('updateEditor', function (value, element, params) {
      ckeditor.updateElement();
      return true
    });
  
    $("#article_thumb_remove").on('click', function() {
      if (!confirm(Translator.trans('确认要删除吗？'))) return false;
      var $btn = $(this);
      $.post($btn.data('url'), function() {
        $("#article-thumb-container").html('');
        self.$element('[name=thumb]').val('');
        self.$element.find('[name=originalThumb]').val('');
        $btn.hide();
        notify('success', '删除成功！');
      }).error(function() {
        notify('danger', '删除失败！');
      });
    });
  
    return ckeditor;
  }
  
  initPopover() {
    $("#article-property-tips").popover({
      html: true,
      trigger: 'hover',
      placement: 'left',
      content: $("#article-property-tips-html").html()
    });
  }
  
  initTagSelect() {
    $('#categoryId').select2({
      dropdownAutoWidth: true,
      width: '100%',
      limit: 10,
    });
    
    $('#article-tags').select2({
      dropdownAutoWidth: true,
      ajax: {
        url: $('#article-tags').data('matchUrl'),
        dataType: 'json',
        quietMillis: 100,
        data: function(term, page) {
          return {
            q: term,
            page_limit: 10
          };
        },
        results: function(data) {
          
          var results = [];
          
          $.each(data, function(index, item) {
            
            results.push({
              id: item.name,
              name: item.name
            });
          });
          
          return {
            results: results
          };
          
        }
      },
      initSelection: function(element, callback) {
        var data = [];
        $(element.val().split(",")).each(function() {
          data.push({
            id: this,
            name: this
          });
        });
        callback(data);
      },
      formatSelection: function(item) {
        return item.name;
      },
      formatResult: function(item) {
        return item.name;
      },
      multiple: true,
      maximumSelectionSize: 20,
      placeholder: Translator.trans('请输入标签'),
      width: 'off',
      createSearchChoice: function() {
        return null;
      }
    });
  }
}

new Article();