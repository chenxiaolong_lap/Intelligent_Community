import notify from 'common/notify';
import BatchSelect from 'app/common/widget/batch-select';

var $panel = $('#file-manage-panel');
new BatchSelect($panel);

$('[data-role=batch-delete]').click(function () {
  var flag = false;
  var ids = [];
  $('[data-role=batch-item]').each(function () {
    if ($(this).is(":checked")) {
      flag = true;
      ids.push(this.value);
    }
  });

  if (flag) {
    if (!confirm('确认是否要删除选中的文件？')) {
      return;
    }

    $.post($(this).data('url'), {"fileIds": ids}, function (data) {
      if (data.status) {
        notify('success', data.message);
        window.location.reload();
      }
    })
    
  } else {
    notify('danger',Translator.trans('notify.file_not_select.message'));
    return;
  }
});

$('[data-role=batch-download]').click(function () {
  let url = $(this).data('url');
  var flag = false;
  var ids = [];
  $('[data-role=batch-item]').each(function () {
    if ($(this).is(":checked")) {
      flag = true;
      ids.push(this.value);
    }
  });

  if (flag) {
    $.get(url, {"fileIds": ids}, function(data){
      if (data.status) {
        window.location.href = data.goto;
      }
    }).error(function (data) {
      notify('danger', JSON.parse(data.responseText).error.message);
    });
  } else {
    notify('danger',Translator.trans('notify.file_not_select.message'));
    return;
  }
});

$('.js-delete').click(function () {
  if (!confirm('确认是否要删除文件？')) {
    return;
  }

  $.get($(this).data('url'), '', function (data) {
    if (data.status) {
      notify('success', data.message);
      window.location.reload();
    }
  })
})