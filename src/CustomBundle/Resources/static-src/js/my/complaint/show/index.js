import 'libs/jquery-validation.js';
import notify from 'common/notify';

export default class Complaint {
  constructor(options) {
    this.validator = null;
    this.$element = $('#complaint-handle-form');
    
    this.initValidator();
    this.handleComplaint();
  }
  
  initValidator() {
    let self = this;
    this.validator = this.$element.validate({
      rules: {
        'result': {
          required: true,
          course_title: true
        },
      },
      messages: {
        'result': {
          required: '请输入处理结果'
        }
      },
      ajax: true,
      submitHandler: form => {
        if ($(form).valid()) {
          $('#submit-handle').button('loading');
          $.post($(form).attr('action'), $(form).serialize(), resp => {
            if (resp.status) {
              notify('success', Translator.trans('投诉处理成功.'), 2000);
              setTimeout(function () {
                window.location.reload()
              }, 3000);
            } else {
              notify('danger', resp.message, 2000);
            }
          }).error(function (data) {
            notify('danger', JSON.parse(data.responseText).error.message, 3000);
            $('#submit-handle').button('reset');
          });
        }
      }
    });
  }
  
  handleComplaint() {
    $('#handle-complaint').click(function () {
      $('.handle-form').removeClass('hidden');
    })
  }
}

new Complaint();