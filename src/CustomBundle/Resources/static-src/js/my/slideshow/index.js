import EsWebUploader from 'common/es-webuploader';
import notify from 'common/notify';
import InputEdit from 'app/common/input-edit';
import 'libs/jquery-validation.js';

export default class Slideshow {
  constructor(options) {
    this.validator = null;
    this.$element  = $('#edit-slideshow-form');
    this.$defaultFormData = this.$element.serialize();
    
    this.addValidatorMethod();
    this.createSlideshow();
    this.initUploader();
    this.initValidator();
    this.initEditor();
    this.alertFrom();
    this.selectColor();
  }
  
  selectColor() {
    $('#backgroundColor').click(function () {
      $('#color').trigger('click');
      $('#color').trigger('change');
    });
    
    $('#color').on('change', function () {
      $('#backgroundColor').val($(this).val());
    })
  }
  
  initUploader() {
    new EsWebUploader({
      element: '#upload-picture-btn',
      onUploadSuccess: function(file, response) {
        let url = $("#upload-picture-btn").data("gotoUrl");
        $.get(url, function(html) {
          $("#modal").modal('show').html(html);
        })
      }
    });
  }
  
  initEditor() {
    var editor = CKEDITOR.replace('description', {
      height: 300,
      toolbar: [
        { items: ['FontSize', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
        { items: [ 'Bold', 'Italic', 'Underline', 'TextColor', '-', 'RemoveFormat', 'PasteText', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', 'Maximize'] }
        ],
      resize_enabled: true,
      allowedContent: true,
      resize_maxHeight: 700,
    });
  
    editor.on('change', () => {
      $('#description').val(editor.getData());
    });
    
    $.validator.addMethod('updateEditor', function (value, element, params) {
      editor.updateElement();
      return true
    });
  }
  
  alertFrom() {
    let self = this;
    $(window).bind('beforeunload',function(){
      self.$element.data('serialize', self.$element.serialize());
      if (self.$defaultFormData !== self.$element.data('serialize')) {
        return Translator.trans('还有没有保存的数据,是否要离开此页面?');
      }
    });
  }
  
  initValidator() {
    let self = this;
    
    this.validator = this.$element.validate({
      rules: {
        'backgroundColor': {
          rgbRule: true
        },
        'pictureUrl': {
          url: true,
        },
        'description': {
          updateEditor: true,
        }
      },
      messages: {
        'pictureUrl': {
          url: '网址以http://开头'
        },
      },
      submitHandler: form => {
        $(window).unbind('beforeunload');
        if ($(form).valid()) {
          $('#form-submit-btn').button('loading');
          form.submit();
        }
      }
    });
  }
  
  addValidatorMethod() {
    $.validator.addMethod('rgbRule', function(value, element, params){
      if (value !== '') {
        let $value = value;
        let reg = /^#[0-9a-fA-F]{3,6}$/;
  
        return reg.test($value);
      } else {
        return true
      }

    }, '背景颜色rgb编码不规范')
  }
  
  createSlideshow() {
    let self = this;
    $('#create-slideshow').click(function () {
      $.post($(this).data('url'), {}, function (data) {
        if (data.status) {
          window.location.href = $('#create-slideshow').data('goto');
          notify('success', '创建成功', 3000);
        } else {
          notify('danger', data.message)
        }
      },'json');
    })
  }
}

new Slideshow();