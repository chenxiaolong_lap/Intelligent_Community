import 'libs/jquery-validation.js';
import notify from 'common/notify';

export default class Bespeak {
  constructor(options) {
    this.validator = null;
    this.$element = $('#bespeak-form');
    this.$defaultFormData = this.$element.serialize();
    
    this.initValidator();
    this.alertFrom();
    this.initDate();
  }
  
  alertFrom() {
    let self = this;
    $(window).bind('beforeunload',function(){
      self.$element.data('serialize', self.$element.serialize());
      if (self.$defaultFormData !== self.$element.data('serialize')) {
        return Translator.trans('还有没有保存的数据,是否要离开此页面?');
      }
    });
  }
  
  initDate() {
    $("#reservationTime").datetimepicker({
      autoclose: true,
      minuteStep: 30,
      format: 'yyyy-mm-dd hh:ii',
      minView: 'hour',
      language: document.documentElement.lang,
      endDate: new Date(Date.now() + 86400 * 365 * 10 * 1000)
    });
    $('#reservationTime').datetimepicker('setStartDate', new Date);
  }
  
  initValidator() {
    let self = this;
    this.validator = this.$element.validate({
      rules: {
        'symptom': {
          required: true,
          chinese_limit: 500,
          course_title: true
        },
      },
      messages: {
        'sourceUrl': {
          url: '网址以http://开头'
        },
      },
      ajax: true,
      submitHandler: form => {
        $(window).unbind('beforeunload');
        if ($(form).valid()) {
          $('#bespeak-submit').button('loading');
          $.post($(form).attr('action'), $(form).serialize(), resp => {
            if (resp.status) {
              notify('success', Translator.trans('预约成功,请等待接诊.'), 2000);
              window.location.reload();
              $('#bespeak-submit').button('reset')
            } else {
              notify('danger', resp.message, 3000);
              $('#bespeak-submit').button('reset')
            }
          });
        }
      }
    });
  }
}

new Bespeak();