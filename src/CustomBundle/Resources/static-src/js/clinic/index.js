import 'libs/jquery-validation.js';
import notify from 'common/notify';
import 'libs/select2.js';
import EsWebUploader from 'common/es-webuploader';

export default class Clinic {
  constructor(options) {
    this.validator = null;
    this.$element = $('#clinic-apply-form');
    this.$defaultFormData = this.$element.serialize();
    
    this.initCommunitySelect();
    this.initValidator();
    this.alertFrom();
    this.initUploader()
  }
  
  initUploader() {
    new EsWebUploader({
      element: '#upload-picture-btn',
      onUploadSuccess: function(file, response) {
        let url = $("#upload-picture-btn").data("gotoUrl");
        $.get(url, function(html) {
          $("#modal").modal('show').html(html);
        })
      }
    });
  }
  
  alertFrom() {
    let self = this;
    $(window).bind('beforeunload',function(){
      self.$element.data('serialize', self.$element.serialize());
      if (self.$defaultFormData !== self.$element.data('serialize')) {
        return Translator.trans('还有没有保存的数据,是否要离开此页面?');
      }
    });
  }
  
  initValidator() {
    let self = this;
    this.validator = this.$element.validate({
      rules: {
        'name': {
          required: true,
          chinese_limit: 255,
        },
        'community': {
          required: true,
          chinese_limit: 255,
        },
        'address': {
          required: true
        },
        'businessLicense': {
          required: true,
          alphanumeric: 255,
        },
        'legalPersonName': {
          required: true,
          chinese_limit: 64,
        },
        'legalPersonIdcard': {
          required: true,
          idcardNumber: true
        },
        'phone': {
          required: true,
          phone: true,
        },
        'email': {
          required: true,
          email: true,
        },
        'slogan': {
          course_title: true,
          chinese_limit: 255,
        },
        'introduction': {
          course_title: true,
        }
      },
      messages: {
        'sourceUrl': {
          url: '网址以http://开头'
        },
      },
      ajax: true,
      submitHandler: form => {
        $(window).unbind('beforeunload');
        if ($(form).valid()) {
          $.post($(form).attr('action'), $(form).serialize(), resp => {
            if (resp.status) {
              notify('success', Translator.trans('申请成功,请等待审核通过.'), 2000);
              windows.setTimeout(function () {
                window.location.href = '/';
              }, 2000);
            } else {
              notify('danger', resp.message, 3000);
            }
          }).error(function (data) {
            notify('danger', JSON.parse(data.responseText).error.message, 3000);
          });
        }
      }
    });
  }
  
  initCommunitySelect() {
    $('#community').select2({
      dropdownAutoWidth: true,
      ajax: {
        url: $('#community').data('matchUrl'),
        dataType: 'json',
        quietMillis: 100,
        data: function(term, page) {
          return {
            q: term,
            page_limit: 10
          };
        },
        results: function(data) {
          
          var results = [];
          
          $.each(data, function(index, item) {
            
            results.push({
              id: item.name,
              name: item.name
            });
          });
          
          return {
            results: results
          };
          
        }
      },
      initSelection: function(element, callback) {
        var data = [];
        $(element.val().split(",")).each(function() {
          data.push({
            id: this,
            name: this
          });
        });
        callback(data);
      },
      formatSelection: function(item) {
        return item.name;
      },
      formatResult: function(item) {
        return item.name;
      },
      multiple: false,
      placeholder: Translator.trans('请输入社区名称'),
      width: 'off',
      createSearchChoice: function() {
        return null;
      }
    });
  }
}

new Clinic();