import ReactDOM from 'react-dom';
import React from 'react';
import PersonaMultiInput from '../../../common/component/persona-multi-input';
import sortList from 'common/sortable';
import notify from 'common/notify';

ReactDOM.render(
  <PersonaMultiInput
    addable={true}
    dataSource= {$('#clinic-doctors').data('init-value')}
    outputDataElement='doctors'
    inputName="doctorIds[]"
    searchable={{enable:true,url:$('#clinic-doctors').data('query-url') + "?q="}}
  />,
  document.getElementById('clinic-doctors')
);

ReactDOM.render(
  <PersonaMultiInput
    addable={true}
    dataSource= {$('#clinic-nurses').data('init-value')}
    outputDataElement='nurses'
    inputName="nurseIds[]"
    searchable={{enable:true,url:$('#clinic-nurses').data('query-url') + "?q="}}
  />,
  document.getElementById('clinic-nurses')
);

// $('.js-btn-save').on('click', function(event){
//   if($("input[name=teachers]").val() !== '[]'){
//     $('#teachers-form').submit();
//   }else{
//     notify('warning', Translator.trans('course.manage.min_teacher_num_error_hint'));
//   }
// });