import EsImageCrop from 'common/es-image-crop.js';
import notify from 'common/notify';

class CoverCrop {
  constructor() {
    this.init();
  }
  
  init() {
    let imageCrop = new EsImageCrop({
      element: "#picture-crop",
      group: 'community',
      cropedWidth: 900,
      cropedHeight: 600
    });
    imageCrop.afterCrop = function(response) {
      let url = $("#save-picture-btn").data("url");
      $.post(url, { images: response }, function(data) {
        console.log(data);
        if (data.status) {
          notify('success', '图片上传成功', 2000);
          $('#clinic-mark').attr('src', data.marks.largeMark);
          $('#largeMark').attr('value', data.marks.largeMark);
          $('#midMark').attr('value', data.marks.midMark);
          $('#smallMark').attr('value', data.marks.smallMark);
          $('#modal').modal('hide');
        } else {
          notify('danger', '图片保存失败');
          $("#save-picture-btn").button('reset');
        }
      }, 'json').error(function () {
        notify('danger', '操作失败');
        $("#save-picture-btn").button('reset');
      });
    };
    
    $("#save-picture-btn").click(function(event) {
      event.stopPropagation();
      $(event.currentTarget).button('loading');
      imageCrop.crop({
        imgs: {
          large: [600, 400],
          mid: [480, 270],
          small: [150, 150]
        }
      });
      
    })
    
    $('.go-back').click(function() {
      history.go(-1);
    });
  }
}


new CoverCrop();

