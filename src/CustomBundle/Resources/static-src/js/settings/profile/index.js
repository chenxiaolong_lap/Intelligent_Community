import Profile from './profile';
import autocomplete from 'common/autocomplete';

new Profile();

autocomplete({
  element: '#community',
  valueKey: 'name',
});

autocomplete({
  element: '#housingEstate',
  valueKey: 'name',
});

autocomplete({
  element: '#address',
  valueKey: 'address',
});
