import notify from 'common/notify';
import 'app/common/local-image/upload';
import InputEdit from 'app/common/input-edit';
import autocomplete from 'common/autocomplete';
import distpicker from 'custombundle/libs/js/distpicker';

export default class Profile {
  constructor(options) {
    this.validator = null;
    this.$element  = $('#user-profile-form');

    this.initDate();
    this.initEditor();
    this.initValidator();
    this.initDistpicker();
    this.editNickName();
    this.syncCertificate();
  }

  initDate() {
    $(".js-date").datetimepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      minView: 'month',
      language: document.documentElement.lang
    });
  }

  initEditor() {
    CKEDITOR.replace('profile_about', {
      toolbar: 'Simple',
      filebrowserImageUploadUrl: $('#profile_about').data('imageUploadUrl')
    });
  }

  initValidator() {
    let self = this;

    self.validator = self.$element.validate({
      rules: {
        'nickname': {
          required: true,
          chinese_alphanumeric: true,
          byte_minlength: 4,
          byte_maxlength: 18,
          nickname: true,
          chinese_alphanumeric: true,
          es_remote: {
            type: 'get',
          }
        },
        'profile[company]': {
          required: true,
          chinese_limit: 200,
        },
        'profile[province]': {
          required: true,
        },
        'profile[truename]': {
          minlength: 2,
          maxlength: 18
        },
        'profile[title]': {
          maxlength: 24
        },
        'profile[qq]': 'qq',
        'profile[weibo]': 'url',
        'profile[blog]': 'url',
        'profile[site]': 'url',
        'profile[mobile]': 'mobile',
        'profile[idcard]': 'idcardNumber',
        'profile[intField1]': {digits: true, maxlength: 9},
        'profile[intField2]': {digits: true, maxlength: 9},
        'profile[intField3]': {digits: true, maxlength: 9},
        'profile[intField4]': {digits: true, maxlength: 9},
        'profile[intField5]': {digits: true, maxlength: 9},
        'profile[floatField1]': 'float',
        'profile[floatField2]': 'float',
        'profile[floatField3]': 'float',
        'profile[floatField4]': 'float',
        'profile[floatField5]': 'float',
        'profile[dateField5]': 'date',
        'profile[dateField5]': 'date',
        'profile[dateField5]': 'date',
        'profile[dateField5]': 'date',
        'profile[dateField5]': 'date',
      },
      messages: {
        'profile[province]': {
          required: '请选择所在省',
        },
      },
    });
  }

  elementAddRules($element) {
    $element.rules("add", {
      required: true,
      chinese_limit: 20,
      control_room_equals: true,
      es_remote: {
        type: 'get',
      },
      messages: {
        required: '请输入中控室名称'
      }
    });
  }

  elementRemoveRules($element) {
    $element.rules('remove');
  }

  editNickName() {
    new InputEdit({
      el: '#nickname-form-group',
      success(data) {
        notify('success', Translator.trans(data.message));
      },
      fail(data) {
        if (data.responseJSON.message) {
          notify('danger', Translator.trans(data.responseJSON.message));
        } else {
          notify('danger', Translator.trans('user.settings.basic_info.nickname_change_fail'));
        }
      }
    });
  }

  initDistpicker() {
    $(".distpicker").distpicker({
      province: "省份",
      city: "城市",
      district: "市区"
    });
  }

  syncCertificate() {
    this.$element.on('click', '#update-certificate', function () {
      $('.icon-gengxin').removeClass('icon-gengxin');
      $('.iconfont').addClass('icon-duihao');
      $.ajax({
        url: $(this).data('url'),
        type: 'GET',
        dataType: 'json',
        data: {},
        success: function (data) {
          $('.icon-gengxin').removeClass('icon-gengxin');
          $('.icon').addClass('icon-duihao');
          if (data.status == 'success') {
            $('#certificate').html('');
            var length = data.certificates.length;
            var i = 0;
            $.each(data.certificates, function (key, certificate) {
              if (++i !== length) {
                $('#certificate').append(certificate.job + '[' + certificate.level + ']' + '、');
              } else {
                $('#certificate').append(certificate.job + '[' + certificate.level + ']');
              }

            });
            notify('success', data.message);
          } else if (data.status == 'warn') {
            notify('warning', data.message);
          } else {
            notify('danger', data.message);
          }
        },
        error: function (data) {
          notify('danger', '证书更新失败');
        }
      });
    });
  }
}