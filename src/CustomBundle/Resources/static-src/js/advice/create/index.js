import 'libs/jquery-validation.js';
import notify from 'common/notify';

export default class Advice {
  constructor(options) {
    this.validator = null;
    this.$element = $('#advice-form');
    
    this.initValidator();
  }
  
  initValidator() {
    let self = this;
    this.validator = this.$element.validate({
      rules: {
        'content': {
          required: true,
          course_title: true
        },
        
      },
      ajax: true,
      submitHandler: form => {
        if ($(form).valid()) {
          $.post($(form).attr('action'), $(form).serialize(), resp => {
            if (resp.status) {
              notify('success', Translator.trans('提交建议成功.'), 2000);
              window.location.reload();
            } else {
              notify('danger', resp.message, 3000);
            }
          }).error(function (data) {
            notify('danger', JSON.parse(data.responseText).error.message, 3000);
          });
        }
      }
    });
  }
}

new Advice();