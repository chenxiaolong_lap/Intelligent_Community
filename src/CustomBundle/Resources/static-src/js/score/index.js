import notify from 'common/notify';

$('#score-save-btn').click(function () {
  if ($('input:checked').length < 1) {
    notify('danger', '至少选择一个选项');
    return false;
  }
  
  $.post($('#score-form').attr('action'), $('#score-form').serialize(), function (data) {
    if (data.status) {
      notify('success', '保存成功', 5000);
      window.location.reload();
    }
  })
});