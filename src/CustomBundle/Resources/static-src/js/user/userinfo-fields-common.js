import SmsSender from 'app/common/widget/sms-sender';
import distpicker from 'custombundle/libs/js/distpicker';
import InputEdit from 'app/common/input-edit';
import 'app/common/local-image/upload';
import notify from 'common/notify';

export default class UserInfoFieldsItemValidate {
  constructor(options) {
    this.validator = null;
    this.$element = $(options.element);
    this.setup();
  }
  
  setup() {
    this.createValidator();
    this.initComponents();
    this.smsCodeValidate();
    this.initEvent();
    this.initDistpicker();
    this.editNickName();
  }
  
  initEvent()
  {
    this.$element.on('click', '#getcode_num', (event) => this.changeCaptcha(event));
    this.$element.on('click', '.js-sms-send', (event) => this.sendSms(event));
  }
  
  initComponents() {
    $('.date').each(function () {
      $(this).datetimepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        minView: 2,
        language: document.documentElement.lang
      });
    });
  }
  
  createValidator() {
    this.validator = this.$element.validate({
      currentDom: '#form-submit-btn',
      rules: {
        email: {
          required: true,
          email: true,
          remote: {
            url: $('#email').data('url'),
            type: 'get',
            data: {
              'value': function () {
                return $('#email').val();
              }
            }
          }
        },
        mobile: {
          required: true,
          phone: true,
          remote: {
            url: $('#mobile').data('url'),
            type: 'get',
            data: {
              'value': function () {
                return $('#mobile').val();
              }
            }
          }
        },
        truename: {
          required: true,
          chinese_alphanumeric: true,
          minlength: 2,
          maxlength: 36,
        },
        qq: {
          required: true,
          qq: true,
        },
        idcard: {
          required: true,
          idcardNumber: true,
        },
        gender: {
          required: true,
        },
        company: {
          required: true,
        },
        job: {
          required: true,
        },
        weibo: {
          required: true,
          url: true,
        },
        weixin: {
          required: true,
        },
        province: {
          required: true,
        },
        community: {
          required: true,
        },
        housingEstate: {
          required: true
        },
        address: {
          required: true,
        }
      },
      messages: {
        gender: {
          required: Translator.trans('site.choose_gender_hint'),
        },
        mobile: {
          phone: Translator.trans('validate.phone.message'),
        },
        province: {
          required: '请选择省份',
        },
        community: {
          required: '请填写所在社区',
        },
        job: {
          required: '请填写职位',
        },
        housingEstate: {
          required: '请填写所在小区',
        },
        address: {
          required: '请填写详细地址',
        }
      },
      submitHandler: form => {
        if ($(form).valid()) {
          $.post($(form).attr('action'), $(form).serialize(), resp => {
            if (resp.success) {
              notify('success', Translator.trans('site.save_success_hint'));
              setTimeout(function () {
                window.location.href = resp.goto;
              }, 1000);
            } else {
              notify('danger', resp.message, 3000);
            }
          });
        }
      }
    });
    this.getCustomFields();
  }
  
  smsCodeValidate() {
    if ($('.js-captch-num').length > 0) {
      
      //$('.js-captch-num').find('#getcode_num').attr("src", $("#getcode_num").data("url") + "?" + Math.random());
      
      $('input[name="captcha_num"]').rules('add', {
        required: true,
        alphanumeric: true,
        es_remote: {
          type: 'get',
          callback: function (bool) {
            if (bool) {
              $('.js-sms-send').removeClass('disabled');
            } else {
              $('.js-sms-send').addClass('disabled');
              $('.js-captch-num').find('#getcode_num').attr("src",$("#getcode_num").data("url")+ "?" + Math.random());
            }
          }
        },
        messages: {
          required: Translator.trans('site.captcha_code.required'),
          alphanumeric: Translator.trans('json_response.verification_code_error.message'),
        }
      });
      
      $('input[name="sms_code"]').rules('add', {
        required: true,
        unsigned_integer: true,
        es_remote: {
          type: 'get',
        },
        messages: {
          required: Translator.trans('validate.sms_code_input.message'),
        }
      })
    }
  }
  
  sendSms(e) {
    new SmsSender({
      element: '.js-sms-send',
      url: $('.js-sms-send').data('smsUrl'),
      smsType: 'sms_bind',
      dataTo: 'mobile',
      captchaNum: 'captcha_num',
      captcha: true,
      captchaValidated: $('input[name="captcha_num"]').valid(),
      preSmsSend: function () {
        let couldSender = true;
        return couldSender;
      }
    });
  }
  
  getCustomFields() {
    for (var i = 1; i <= 5; i++) {
      $(`[name="intField${i}"]`).rules('add', {
        required: true,
        positive_integer: true,
      });
      $(`[name="floatField${i}"]`).rules('add', {
        required: true,
        float: true,
      });
      $(`[name="dateField${i}"]`).rules('add', {
        required: true,
        date: true,
      });
    }
    for (var i = 1; i <= 10; i++) {
      $(`[name="varcharField${i}"]`).rules('add', {
        required: true,
      });
      $(`[name="textField${i}"]`).rules('add', {
        required: true,
      });
    }
  }
  
  changeCaptcha(e) {
    var $code = $(e.currentTarget);
    $code.attr("src", $code.data("url") + "?" + Math.random());
  }
  
  initDistpicker(){
    $(".distpicker").distpicker({
      province: "省份",
      city: "城市",
      district: "市区"
    });
  }
  
  editNickName(){
    new InputEdit({
      el: '#nickname-form-group',
      success(data) {
        notify('success', Translator.trans(data.message));
      },
      fail(data) {
        if (data.responseJSON.message) {
          notify('danger', Translator.trans(data.responseJSON.message));
        } else {
          notify('danger', Translator.trans('user.settings.basic_info.nickname_change_fail'));
        }
      }
    });
  }
}
