import UserInfoFieldsItemValidate from '../userinfo-fields-common';
import autocomplete from 'common/autocomplete';

new UserInfoFieldsItemValidate({
  element: '#fill-userinfo-form'
});

autocomplete({
  element: '#community',
  valueKey: 'name',
});

autocomplete({
  element: '#housingEstate',
  valueKey: 'name',
});

autocomplete({
  element: '#address',
  valueKey: 'address',
});
