import 'app/js/article/detail/index.js';
import notify from 'common/notify';

$('#community-activity-sign').click(function () {
  $.post($(this).data('url'), '', function (data) {
    if (data.status) {
      notify('success', '签到成功');
      $('#community-activity-sign').removeClass('btn btn-primary').addClass('color-primary').html(`<a id="sign-success" class="color-primary pull-right"><i class="glyphicon glyphicon-ok"></i>已签到</a>`);
      $('#thread-body').removeClass('hidden');
    }
  })
})