import notify from 'common/notify';
import 'libs/jquery-validation.js';

export default class BaiduMap {
  constructor(options) {
    this.$map = null;
    this.$point = null;
    this.$defaultMap = null;
  
    this._initMap();
    this._initSearch();
    this._initSelect();
    this._initHoverItemMap();
  }
  
  _initMap() {
    let map = this._initCommunityDistrict();                // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
  
    var opts = {type: BMAP_NAVIGATION_CONTROL_SMALL};
    map.addControl(new BMap.NavigationControl(opts));
    map.addControl(new BMap.ScaleControl());
    //map.addControl(new BMap.OverviewMapControl());
    map.addControl(new BMap.MapTypeControl());
  
    this.$map = map;
    this.$defaultMap = map;
  }
  
  SearchOnMap(val) {//远程搜索
    let self = this;
    var map = this.$map;
    var circle = new BMap.Circle(self.$point,1000,{fillColor:"green", strokeWeight: 1 ,fillOpacity: 0.3, strokeOpacity: 0.3});//圆形搜索区域
    map.addOverlay(circle);
    
    var local = new BMap.LocalSearch(map, {
      renderOptions:{map: map, panel: "result-lists", autoViewport: false}
    });
    local.search(val, self.$point, 1000);//参数可带坐标 距离
  }
  
  _initSelect() {//热门标签选择
    let self = this;
    $('#periphery-nav li').click(function () {
      $('#periphery-nav li').each(function (index, item) {
        $(this).removeClass('selected');
      });
      $(this).addClass('selected');
      self.SearchOnMap($(this).children().eq(0).text());
      $('#keyword').val('');
    });
  }
  
  _initHoverItemMap() {
    let self = this;
    let map = this.$defaultMap;
   
    if ($('.course-list').find('.course-item__pc').length > 0) {
      $('.course-item__pc').hover(function () {
        var Geo = new BMap.Geocoder();
        var region = $(this).data('region') + $(this).data('address');
        var name = $(this).data('name');
        
        Geo.getPoint(region, function (point) {
          if (point) {
            self.$point = point;
            map.centerAndZoom(point, 16);
            let marker = new BMap.Marker(point);
            map.addOverlay(marker);
  
            let opts = {
              width : 200,     // 信息窗口宽度
              height: 100,     // 信息窗口高度
              title : name, // 信息窗口标题
              enableMessage:true,//设置允许信息窗发送短息
              message:""
            };
  
            let infoWindow = new BMap.InfoWindow(`地址：${region}.`, opts);  // 创建信息窗口对象
            marker.addEventListener("click", function(){
              map.openInfoWindow(infoWindow,point); //开启信息窗口
            });
            
          }else{
            notify('danger',"该地址没有解析到结果!");
          }
        })
      }, function () {
        // self._initCommunityDistrict();
      })
    }
  }
  
  clickItem(itemId, html, marker) {
    let infoWindow = new BMap.InfoWindow(html);  // 创建信息窗口对象
    marker.addEventListener("click", function(){
      this.openInfoWindow(infoWindow);
      图片加载完毕重绘infowindow
      // document.getElementById(itemId).onload = function (){
      //   infoWindow.redraw();   //防止在网速较慢，图片未加载时，生成的信息框高度比图片的总高度小，导致图片部分被隐藏
      // }
    });
  }
  
  _initCommunityDistrict() {
    let self = this;
    var map = new BMap.Map("baiduMap");
   
    // 创建地址解析器实例
    var myGeo = new BMap.Geocoder();
    // 将地址解析结果显示在地图上,并调整地图视野
    $.get('/get/community/map/info', {}, function (data) {
      if (data == null) {
        notify('danger', '对不起获取社区地址出错，请检查个人信息是否正确', 5000);
      }
      
      var communityMapInfo = `${data.city}${data.district}${data.name}`;
      myGeo.getPoint(communityMapInfo, function(point){
        if (point) {
          self.$point = point;
          map.centerAndZoom(point, 16);
          let marker = new BMap.Marker(point);
          map.addOverlay(marker);
  
          let opts = {
            width : 200,     // 信息窗口宽度
            height: 100,     // 信息窗口高度
            title : data.name , // 信息窗口标题
            enableMessage:true,//设置允许信息窗发送短息
            message:""
          };
          
          let infoWindow = new BMap.InfoWindow(`地址：${data.city}${data.district}${data.address}.`, opts);  // 创建信息窗口对象
          marker.addEventListener("click", function(){
            map.openInfoWindow(infoWindow,point); //开启信息窗口
          });
          
        }else{
          notify('danger',"您选择地址没有解析到结果!");
        }
      }, data.city);
    });
    
    return map;
  }
  
  _initSearch() {//搜索机制
    let self = this;
    let action = $('#search-form').attr('action');
    $('#type-choose').change(function () {
      if($(this).val() == 'nolocal') {
        $('#search-form').removeAttr('action');
        $('#search-on-map').attr('type', 'button');
        $('#search-on-map').bind('click', function () {
          $('#periphery-nav li').each(function (index, item) {
            $(this).removeClass('selected');
          });
          $('#search-on-map').button('loading');
          self.SearchOnMap($('#keyword').val());
          $('#search-on-map').button('reset');
        })
      } else if ($(this).val() == 'local') {
        $('#search-on-map').attr('type', 'submit');
        $('#search-form').attr('action', action);
      }
    });
    
    // $('#search-on-map').click(function () {
    //   if ($('#keyword').val() !== '' && $('#type-choose').val() != 'local') {
    //     $('#periphery-nav li').each(function (index, item) {
    //       $(this).removeClass('selected');
    //     });
    //     $('#search-on-map').button('loading');
    //     self.SearchOnMap($('#keyword').val());
    //     $('#search-on-map').button('reset');
    //   }
    // })
  }
}

new BaiduMap();