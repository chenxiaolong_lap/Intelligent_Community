import BaseOptions from 'app/common/component/multi-input/options';
import React, { Component } from 'react';

export default class Options extends BaseOptions{
  render() {
    let options = [];
    this.props.searchResult.map((item,i) => {
      let temp = (<li key={ i }><a data-item = {JSON.stringify(item)} onClick={event=>this.handleChange(event)} data-name={item.nickname}>{item.nickname} {item.truename}</a></li>);
      options.push(temp);
    });
    if(options.length <= 0) {
      options.push((<li className="not-find"><a>{ Translator.trans('site.data.not_found') }</a></li>));
    }
    return (
      <ul className={`dropdown-menu options ${ this.state.resultful && 'show' } `}>
        {options}
      </ul>
    )
  }
}