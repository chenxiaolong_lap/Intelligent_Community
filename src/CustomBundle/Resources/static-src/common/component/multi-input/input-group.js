import React, { Component } from 'react';
import { trim } from 'app/common/unit';
import { send } from 'app/common/server';
import Options from './options';
import postal from 'postal';
import BaseInputGroup from 'app/common/component/multi-input/input-group'

export default class InputGroup extends BaseInputGroup {
  
  render() {
    let createTrans = Translator.trans('site.data.create');
    return (
      <div className="input-group form-group">
        <input className="form-control" placeholder="请输入用户名/真实姓名" value={this.state.itemName} onChange={event => this.handleNameChange(event)} onFocus={event => this.onFocus(event)} />
        {this.context.searchable.enable && this.state.resultful && <Options searchResult={this.state.searchResult} selectChange={(event, name) => this.selectChange(event, name)} resultful={this.state.resultful} />}
        {this.context.addable && <span className="input-group-btn"><a className="btn btn-default" onClick={() => this.handleAdd()}>{createTrans}</a></span>}
      </div>
    );
  }
}

InputGroup.contextTypes = {
  addItem: React.PropTypes.func,
  addable: React.PropTypes.bool,
  searchable:  React.PropTypes.shape({
    enable: React.PropTypes.bool,
    url: React.PropTypes.string,
  }),
};
