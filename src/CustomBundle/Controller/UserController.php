<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\UserController as BaseUserController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\SmsToolkit;
use Biz\User\CurrentUser;
use AppBundle\Common\Paginator;

class UserController extends BaseUserController
{
    public function fillUserInfoAction(Request $request)
    {
        $auth = $this->getSettingService()->get('auth');
        $user = $this->getCurrentUser();
        $userInfo = $this->getUserService()->getUserProfile($user['id']);

        if ($auth['fill_userinfo_after_login'] && !isset($auth['registerSort'])) {
            return $this->redirect($this->generateUrl('homepage'));
        }

        if (!$user->isLogin()) {
            return $this->createMessageResponse('error', '请先登录！');
        }

        $goto = $this->getTargetPath($request);

        if ($request->getMethod() == 'POST') {
            $formData = $request->request->all();

            $authSetting = $this->setting('auth', array());

            if (!empty($formData['mobile']) &&
                !empty($authSetting['mobileSmsValidate']))
            {
                list($result, $sessionField, $requestField) = SmsToolkit::smsCheck($request, 'sms_bind');

                if (!$result) {
                    $response = array(
                        'success' => false,
                        'message' => 'register.userinfo_fill_tips'
                    );

                    return $this->createJsonResponse($response);
                }
            }

            $userInfo = $this->saveUserInfo($request, $user);

            return $this->createJsonResponse(array('success' => true, 'goto' => $goto));
        }

        $userFields = $this->getUserFieldService()->getEnabledFieldsOrderBySeq();
        $userFields = ArrayToolkit::index($userFields, 'fieldName');

        return $this->render('@Custom/user/fill-userinfo-fields.html.twig', array(
            'userFields'   => $userFields,
            'user'         => $userInfo,
            'goto'         => $goto,
        ));
    }

    protected function saveUserInfo($request, $user)
    {
        $formData = $request->request->all();

        $userInfo = $this->partsFields($formData);

        if (isset($formData['email']) && !empty($formData['email'])) {
            $this->getAuthService()->changeEmail($user['id'], null, $formData['email']);

            if (!$user['setup']) {
                $this->getUserService()->setupAccount($user['id']);
            }
        }

        $authSetting = $this->setting('auth', array());
        if (!empty($formData['mobile']) &&
            !empty($authSetting['fill_userinfo_after_login']) &&
            !empty($authSetting['mobileSmsValidate']))
        {
            $verifiedMobile = $formData['mobile'];
            $this->getUserService()->changeMobile($user['id'], $verifiedMobile);
        }

        $currentUser = new CurrentUser();
        $currentUser->fromArray($this->getUserService()->getUser($user['id']));
        $this->switchUser($request, $currentUser);

        $userInfo = $this->getUserService()->updateUserProfile($user['id'], $userInfo);

        return $userInfo;
    }

    protected function partsFields($fields)
    {
        return ArrayToolkit::parts($fields, array(
            'truename',
            'mobile',
            'qq',
            'company',
            'weixin',
            'weibo',
            'idcard',
            'gender',
            'job',
            'intField1',
            'intField2',
            'intField3',
            'intField4',
            'intField5',
            'floatField1',
            'floatField2',
            'floatField3',
            'floatField4',
            'floatField5',
            'dateField1',
            'dateField2',
            'dateField3',
            'dateField4',
            'dateField5',
            'varcharField1',
            'varcharField2',
            'varcharField3',
            'varcharField4',
            'varcharField5',
            'varcharField10',
            'varcharField6',
            'varcharField7',
            'varcharField8',
            'varcharField9',
            'textField1',
            'textField2',
            'textField3',
            'textField4',
            'textField5',
            'textField6',
            'textField7',
            'textField8',
            'textField9',
            'textField10',
            'province',
            'city',
            'district',
            'community',
            'housingEstate',
            'address',
        ));
    }

    public function matchAddressAction(Request $request)
    {
        $queryFields = $request->query->get('q');

        $addresses = $this->getUserService()->findAddressesByLikeAddress($queryFields);

        $data = array();
        foreach ($addresses as $address) {
            $data[] = array(
                'id' => $address['id'],
                'address' => $address['address']
            );
        }

        return $this->createJsonResponse($data);
    }

    public function matchNicknameAction(Request $request)
    {
        $queryFields = $request->query->get('q');

        $nicknames = $this->getUserService()->findUsersByLikeNickname($queryFields);

        $data = array();
        foreach ($nicknames as $nickname) {
            $data[] = array(
                'id' => $nickname['id'],
                'name' => $nickname['nickname'],
                'nickname' => $nickname['nickname']
            );
        }

        return $this->createJsonResponse($data);
    }

    public function matchByNicknameOrTruenameAction(Request $request)
    {
        $queryField = $request->query->get('q');
        $users = $this->getUserService()->matchUsersByNicknameOrTruename($queryField);

        $data = array();

        foreach ($users as $user) {
            $data[] = array(
                'id' => $user['id'],
                'nickname' => $user['nickname'],
                'truename' => $user['truename'],
                'avatar' => $this->getWebExtension()->getFilePath($user['smallAvatar'], 'avatar.png'),
                'isVisible' => 1,
            );
        }

        return $this->createJsonResponse($data);
    }

    public function obtainAction()
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        $users = $this->getUserService()->searchUsers(
            array(
                'province' => $userProfile['province'],
                'city' => $userProfile['province'],
                'district' => $userProfile['district'],
                'community' => $userProfile['community'],
            ),
            array(),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/my/activity/user-invited-list.html.twig', array(
            'users' => $users
        ));
    }

    public function showAction(Request $request, $id)
    {
        $user = $this->tryGetUser($id);
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $userProfile['about'] = strip_tags($userProfile['about'], '');
        $userProfile['about'] = preg_replace('/ /', '', $userProfile['about']);
        $user = array_merge($user, $userProfile);

//        if (strpos('COMMUNITY_MANAGE', $user['communityRole'])) {
//            return $this->reservationRecordAction($request, $user['id']);
//        }

        return $this->reservationRecordAction($request, $user['id']);
    }


    public function reservationRecordAction(Request $request, $userId)
    {
        $user = $this->getUserService()->getUser($userId);

        $fields = $request->query->all();

        $conditions = array(
            'keyword' => '',
            'keywordType' => '',
            'userId' => $userId
        );

        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'clinicName') {
                $clinics = $this->getClinicService()->searchClinics(
                    array('name' => $conditions['keyword']),
                    array(),
                    0,
                    PHP_INT_MAX
                );

                $conditions['clinicIds'] = ArrayToolkit::column($clinics, 'id');
            }
        }

        $count = $this->getClinicReservationRecordService()->countReservationRecord($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            10
        );

        $records = $this->getClinicReservationRecordService()->searchReservationRecords(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $clinicIds = ArrayToolkit::column($records, 'clinicId');
        $clinics = $this->getClinicService()->searchClinics(
            array('clinicIds' => $clinicIds),
            array(),
            0,
            PHP_INT_MAX
        );
        $clinics = ArrayToolkit::index($clinics, 'id');

        $scores = $this->getScoreService()->searchScores(
            array('ids' => ArrayToolkit::column($records, 'scoreId')),
            array(),
            0,
            PHP_INT_MAX
        );
        $scores = ArrayToolkit::index($scores, 'id');

        return $this->render('@Custom/user/reservation-record.html.twig', array(
            'user' => $user,
            'count' => $count,
            'paginator' => $paginator,
            'clinics' => $clinics,
            'scores' => $scores,
            'records' => $records,
            'type' => 'reservation-record',
        ));
    }

    public function communityActivityRecordAction(Request $request, $userId)
    {
        $user = $this->getUserService()->getUser($userId);
        $fields = $request->query->all();

        $conditions = array(
            'keywordType' => '',
            'keyword' => '',
            'keywordUserType' => '',
        );
        $conditions = array_merge($conditions, $fields);

        if ($conditions['keywordType'] == 'title' && !empty($conditions['keyword'])) {
            $conditions['title'] = $conditions['keyword'];
        }

        if (!empty($conditions['status'])) {
            if ($conditions['status'] == '未开始') {
                $conditions['startTime_GT'] = time();
            } elseif ($conditions['status'] == '进行中') {
                $conditions['endTime_GE'] = time();
                $conditions['startTime_LE'] = time();
            } elseif ($conditions['status'] == '已结束') {
                $conditions['endTime_LT'] = time();
            }
        }

        if (!empty($conditions['targetType'])) {
            $conditions['type'] = $conditions['targetType'];
        }

        $activityCount = $this->getCommunityActivityService()->countCommunityActivity($conditions);

        $paginator = new Paginator(
            $this->get('request'),
            $activityCount,
            20
        );

        $activities = $this->getCommunityActivityService()->searchCommunityActivity(
            $conditions,
            array('updatedTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('@Custom/user/activity-record.html.twig', array(
            'paginator' => $paginator,
            'activityCount' => $activityCount,
            'activities' => $activities,
            'user' => $user,
            'type' => 'community-activity-record'
        ));
    }

    public function setCommunityRoleAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);

        if ($request->getMethod() == 'POST') {
            $communityRoles = $request->request->get('roles');

            $updateProfile = $this->getUserService()->setCommunityRole($id, $communityRoles);

            return $this->createJsonResponse(array('status' => true, 'message' => '设置用户社区角色成功'));
        }

        return $this->render('@Custom/my/user/community-role-modal.html.twig', array(
            'user' => $user
        ));
    }

    protected function getClinicService()
    {
        return $this->createService('CustomBundle:Clinic:ClinicService');
    }

    protected function getClinicReservationRecordService()
    {
        return $this->createService('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }

    protected function getCommunityActivityService()
    {
        return $this->createService('CustomBundle:CommunityActivity:CommunityActivityService');
    }

    protected function getScoreService()
    {
        return $this->createService('CustomBundle:Score:ScoreService');
    }
}