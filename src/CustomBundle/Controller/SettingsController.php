<?php

namespace CustomBundle\Controller;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Controller\SettingsController as BaseSettingsController;
use CustomBundle\Biz\Certificate\Service\CertificateService;
use Symfony\Component\HttpFoundation\Request;
use CustomBundle\Common\CertificateToolkit;
use AppBundle\Common\FileToolkit;

class SettingsController extends BaseSettingsController
{
    public function profileAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $profile = $this->getUserService()->getUserProfile($user['id']);
        $certificates = $this->getCertificateService()->findCertificatesByUserId($user['id']);

        $name = 250;

        $profile['title'] = $user['title'];

        if ($request->getMethod() === 'POST') {
            $profile = $request->request->get('profile');

            if (!((strlen($user['verifiedMobile']) > 0) && (isset($profile['mobile'])))) {
                $this->getUserService()->updateUserProfile($user['id'], $profile);

                $this->setFlashMessage('success', 'site.save.success');
            } else {
                $this->setFlashMessage('danger', 'user.settings.profile.unable_change_bind_mobile');
            }

            return $this->redirect($this->generateUrl('settings'));
        }

        $fields = $this->getUserFieldService()->getEnabledFieldsOrderBySeq();

        if (array_key_exists('idcard', $profile) && $profile['idcard'] == '0') {
            $profile['idcard'] = '';
        }

        $fromCourse = $request->query->get('fromCourse');

        return $this->render('@Custom/settings/profile.html.twig', array(
            'profile' => $profile,
            'fields' => $fields,
            'fromCourse' => $fromCourse,
            'user' => $user,
            'certificates' => $certificates
        ));
    }

    public function approvalSubmitAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $profile = $this->getUserService()->getUserProfile($user['id']);
        $profileInfo = $profile;
        $profile['idcard'] = substr_replace($profile['idcard'], '************', 4, 12);
        $approval = $this->getUserService()->getLastestApprovalByUserIdAndStatus($user['id'], $user['approvalStatus']);

        if ($request->getMethod() === 'POST') {
            $faceImg = $request->files->get('faceImg');
            $backImg = $request->files->get('backImg');

            if (abs(filesize($faceImg)) > 2 * 1024 * 1024 || abs(filesize($backImg)) > 2 * 1024 * 1024
                || !FileToolkit::isImageFile($backImg) || !FileToolkit::isImageFile($faceImg)) {
                $this->setFlashMessage('danger', 'user.settings.verification.photo_require_tips');

                return $this->render('settings/approval.html.twig', array(
                    'profile' => $profile,
                ));
            }

            $directory = $this->container->getParameter('topxia.upload.private_directory').'/approval';
            $this->getUserService()->applyUserApproval($user['id'], $request->request->all(), $faceImg, $backImg, $directory);
            return $this->redirect($this->generateUrl('setting_approval_submit'));
        }

        return $this->render('@Custom/settings/approval.html.twig', array(
            'profile' => $profile,
            'approval' => $approval,
            'profileInfo' => $profileInfo,
        ));
    }

    public function updateCertificateAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $profile = $this->getUserService()->getUserProfile($user['id']);

        if (!empty($profile['idcard']) && !empty($profile['truename'])) {
            $conditions = array(
                'cid'    => $profile['idcard'],
                'name'   => $profile['truename'],
                'userId' => $profile['id'],
            );

            $response = $this->getCertificateService()->obtainCertificate($conditions);

            if ($response['status'] == 'success') {
                $certificates = $this->getCertificateService()->findCertificatesByUserId($profile['id']);

                $response = array(
                    'status'  => 'success',
                    'message' => '证书更新完成',
                    'certificates' => $certificates,
                );
            }
        } else {
            $response = array(
                'status'  => 'fail',
                'message' => '对不起，你不具备查询条件，请完善个人信息！',
            );
        }

        return $this->createJsonResponse($response);
    }

    /**
     * @return CertificateService
     */
    protected function getCertificateService()
    {
        return $this->createService('CustomBundle:Certificate:CertificateService');
    }
}
