<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class HousingEstateController extends BaseController
{
    public function matchHousingEstateAction(Request $request)
    {
        $queryFields = $request->query->get('q');

        $housingEstates = $this->getHousingEstateService()->findHousingEstatesByLikeName($queryFields);

        $data = array();

        foreach ($housingEstates as $housingEstate) {
            $data[] = array(
                'id' => $housingEstate['id'],
                'name' => $housingEstate['name']
            );
        }

        return $this->createJsonResponse($data);
    }

    protected function getHousingEstateService()
    {
        return $this->createService('CustomBundle:HousingEstate:HousingEstateService');
    }
}