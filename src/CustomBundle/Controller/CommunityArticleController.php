<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\ArticleController as BaseArticleController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;

class CommunityArticleController extends BaseArticleController
{
    public function indexAction(Request $request, $filter = 'latest')
    {
        $community = $this->getCommunity();
        $categoryTree = $this->getCategoryService()->getCategoryTree();
        $conditions = $this->fillOrgCode(
            array(
                'status' => 'published',
            )
        );
        $conditions = array_merge($conditions, array('communityId' => $community['id']));

        $sort = 'published';
        if ($filter == 'latest') {
            $sort = 'published';
        } elseif ($filter == 'promoted') {
            $conditions['promoted'] = 1;
            $sort = 'normal';
        } elseif ($filter == 'hot') {
            $sort = 'popular';
        }

        $paginator = new Paginator(
            $this->get('request'),
            $this->getArticleService()->countArticles($conditions),
            $this->setting('article.pageNums', 10)
        );

        $latestArticles = $this->getArticleService()->searchArticles(
            $conditions,
            $sort,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $categoryIds = ArrayToolkit::column($latestArticles, 'categoryId');

        $categories = $this->getCategoryService()->findCategoriesByIds($categoryIds);

        $featuredConditions = $this->fillOrgCode(
            array(
                'status' => 'published',
                'featured' => 1,
            )
        );
        $featuredConditions = array_merge($featuredConditions, array('communityId' => $community['id']));

        $featuredArticles = $this->getArticleService()->searchArticles(
            $featuredConditions,
            'normal',
            0,
            5
        );

        $featuredCategories = array();

        foreach ($featuredArticles as $key => $value) {
            $featuredCategories[$value['id']] = $this->getCategoryService()->getCategory($value['categoryId']);
        }

        $promotedConditions = $this->fillOrgCode(
            array(
                'status' => 'published',
                'promoted' => 1,
            )
        );
        $promotedConditions = array_merge($promotedConditions, array('communityId' => $community['id']));

        $promotedArticles = $this->getArticleService()->searchArticles(
            $promotedConditions,
            'normal',
            0,
            2
        );

        $promotedCategories = array();

        foreach ($promotedArticles as $key => $value) {
            $promotedCategories[$value['id']] = $this->getCategoryService()->getCategory($value['categoryId']);
        }

        return $this->render(
            '@Custom/community-article/index.html.twig',
            array(
                'categoryTree' => $categoryTree,
                'latestArticles' => $latestArticles,
                'featuredArticles' => $featuredArticles,
                'featuredCategories' => $featuredCategories,
                'promotedArticles' => $promotedArticles,
                'promotedCategories' => $promotedCategories,
                'paginator' => $paginator,
                'categories' => $categories,
                'filter' => $filter
            )
        );
    }

    public function defaultIndexBlockAction()
    {
        $community = $this->getCommunity();
        $categoryTree = $this->getCategoryService()->getCategoryTree();
        $conditions = $this->fillOrgCode(
            array(
                'status' => 'published',
            )
        );
        $conditions = array_merge($conditions, array('communityId' => $community['id']));

        $latestArticles = $this->getArticleService()->searchArticles(
            $conditions,
            'published',
            0,
            8
        );

        $categoryIds = ArrayToolkit::column($latestArticles, 'categoryId');
        $categories = $this->getCategoryService()->findCategoriesByIds($categoryIds);
        $categories = ArrayToolkit::index($categories, 'id');

        $hotArticles = $this->getArticleService()->searchArticles(
            $conditions,
            'popular',
            0,
            8
        );

        $hotCategories = $this->getCategoryService()->findCategoriesByIds(ArrayToolkit::column($hotArticles, 'categoryId'));
        $hotCategories = ArrayToolkit::index($hotCategories, 'id');

        $conditions['promoted'] = 1;
        $promotedArticles = $this->getArticleService()->searchArticles(
            $conditions,
            'normal',
            0,
            8
        );

        $promotedCategories = $this->getCategoryService()->findCategoriesByIds(ArrayToolkit::column($promotedArticles, 'categoryId'));
        $promotedCategories = ArrayToolkit::index($promotedCategories, 'id');

        return $this->render(
            '@Custom/my/default/article/block.html.twig',
            array(
                'categoryTree' => $categoryTree,
                'latestArticles' => $latestArticles,
                'latestCategories' => $categories,
                'hotArticles' => $hotArticles,
                'hotCategories' => $hotCategories,
                'promotedArticles' => $promotedArticles,
                'promotedCategories' => $promotedCategories,
            )
        );
    }

    public function detailAction(Request $request, $id)
    {
        $article = $this->getArticleService()->getArticle($id);

        if (empty($article)) {
            throw $this->createNotFoundException('文章已删除或者未发布！');
        }

        if ($article['status'] != 'published') {
            return $this->createMessageResponse('error', '文章不是发布状态，请查看！');
        }

        $this->getArticleService()->viewArticle($id);

        $category = $this->getCategoryService()->getCategory($article['categoryId']);

        $tags = $this->getTagService()->findTagsByOwner(array('ownerType' => 'article', 'ownerId' => $id));

        $tagNames = ArrayToolkit::column($tags, 'name');

        $seoKeyword = '';

        if ($tags) {
            $seoKeyword = ArrayToolkit::column($tags, 'name');
            $seoKeyword = implode(',', $seoKeyword);
        }

        $breadcrumbs = $this->getCategoryService()->findCategoryBreadcrumbs($category['id']);

        $conditions = array(
            'targetId' => $id,
            'targetType' => 'article',
            'parentId' => 0,
        );

        $paginator = new Paginator(
            $request,
            $this->getThreadService()->searchPostsCount($conditions),
            10
        );

        $posts = $this->getThreadService()->searchPosts(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($posts, 'userId'));

        $user = $this->getCurrentUser();

        $userLike = $this->getArticleService()->getArticleLike($id, $user['id']);

        $articleBody = $article['body'];

        $articleBody = strip_tags($articleBody, '');

        $articleBody = preg_replace('/ /', '', $articleBody);

        return $this->render(
            '@Custom/my/article/detail.html.twig',
            array(
                'article' => $article,
                'tags' => $tags,
                'seoKeyword' => $seoKeyword,
                'seoDesc' => $articleBody,
                'breadcrumbs' => $breadcrumbs,
                'categoryName' => $category['name'],
                'categoryCode' => $category['code'],
                'posts' => $posts,
                'users' => $users,
                'paginator' => $paginator,
                'tagNames' => $tagNames,
                'userLike' => $userLike,
                'category' => $category,
                'service' => $this->getThreadService(),
            )
        );
    }

    public function categoryAction(Request $request, $categoryCode, $filter = 'latest')
    {
        $community = $this->getCommunity();
        $category = $this->getCategoryService()->getCategoryByCode($categoryCode);

        if (empty($category)) {
            throw $this->createNotFoundException('资讯栏目页面不存在');
        }

        $conditions = array(
            'communityId' => $community['id'],
            'categoryId' => $category['id'],
            'includeChildren' => true,
            'status' => 'published',
        );

        $sort = array();
        if ($filter == 'latest') {
            $sort = 'published';
        } elseif ($filter == 'promoted') {
            $conditions['promoted'] = 1;
            $sort = 'normal';
        } elseif ($filter == 'hot') {
            $sort = 'popular';
        }

        $paginator = new Paginator(
            $this->get('request'),
            $this->getArticleService()->countArticles($conditions),
            $this->setting('article.pageNums', 10)
        );

        $articles = $this->getArticleService()->searchArticles(
            $conditions,
            $sort,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $categoryIds = ArrayToolkit::column($articles, 'categoryId');

        $categories = $this->getCategoryService()->findCategoriesByIds($categoryIds);

        return $this->render(
            '@Custom/community-article/list.html.twig',
            array(
                'categoryCode' => $categoryCode,
                'category' => $category,
                'articles' => $articles,
                'paginator' => $paginator,
                'categories' => $categories,
                'filter' => $filter
            )
        );
    }

    public function categoryNavAction(Request $request, $categoryCode)
    {
        list($rootCategories, $categories, $activeIds) = $this->getCategoryService()->makeNavCategories($categoryCode);

        return $this->render(
            '@Custom/community-article/part/category.html.twig',
            array(
                'rootCategories' => $rootCategories,
                'categories' => $categories,
                'categoryCode' => $categoryCode,
                'activeIds' => $activeIds,
            )
        );
    }

    public function tagAction(Request $request, $name, $filter = 'latest')
    {
        $community = $this->getCommunity();
        $tag = $this->getTagService()->getTagByName($name);

        if (empty($tag)) {
            throw $this->createAccessDeniedException('标签不存在!');
        }

        $tagOwnerRelations = $this->getTagService()->findTagOwnerRelationsByTagIdsAndOwnerType(
            array($tag['id']),
            'article'
        );

        $conditions = array(
            'status' => 'published',
            'articleIds' => ArrayToolkit::column($tagOwnerRelations, 'ownerId'),
            'communityId' => $community['id']
        );

        $sort = array();
        if ($filter == 'latest') {
            $sort = 'published';
        } elseif ($filter == 'promoted') {
            $conditions['promoted'] = 1;
            $sort = 'normal';
        } elseif ($filter == 'hot') {
            $sort = 'popular';
        }

        $paginator = new Paginator(
            $this->get('request'),
            $this->getArticleService()->countArticles($conditions),
            $this->setting('article.pageNums', 10)
        );

        $articles = $this->getArticleService()->searchArticles(
            $conditions,
            $sort,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $categoryIds = ArrayToolkit::column($articles, 'categoryId');

        $categories = $this->getCategoryService()->findCategoriesByIds($categoryIds);

        return $this->render(
            '@Custom/community-article/list-articles-by-tag.html.twig',
            array(
                'articles' => $articles,
                'tag' => $tag,
                'categories' => $categories,
                'paginator' => $paginator,
                'filter' => $filter
            )
        );
    }

    public function popularArticlesBlockAction()
    {
        $community = $this->getCommunity();
        $conditions = $this->fillOrgCode(
            array(
                'type' => 'article',
                'status' => 'published',
            )
        );
        $conditions = array_merge($conditions, array('communityId' => $community['id']));

        $articles = $this->getArticleService()->searchArticles($conditions, 'popular', 0, 6);

        return $this->render(
            'article/popular-articles-block.html.twig',
            array(
                'articles' => $articles,
            )
        );
    }

    public function recommendArticlesBlockAction()
    {
        $community = $this->getCommunity();
        $conditions = array(
            'type' => 'article',
            'status' => 'published',
            'promoted' => 1,
            'communityId' => $community['id']
        );

        $articles = $this->getArticleService()->searchArticles($conditions, 'normal', 0, 6);

        return $this->render(
            'article/recommend-articles-block.html.twig',
            array(
                'articles' => $articles,
            )
        );
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}