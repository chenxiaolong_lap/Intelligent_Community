<?php

namespace CustomBundle\Controller\Admin;

use AppBundle\Controller\Admin\UserApprovalController as BaseUserApprovalController;
use Symfony\Component\HttpFoundation\Request;
use CustomBundle\Common\IdCardToolkit;

class UserApprovalController extends BaseUserApprovalController
{
    public function approveAction(Request $request, $id)
    {
        list($user, $userApprovalInfo) = $this->getApprovalInfo($request, $id, 'approving');

        $params = array('idcard' => $userApprovalInfo['idcard']);
        $idcardInfo = IdCardToolkit::request($params);

        if (!empty($idcardInfo['result'])) {
            $idcardInfo = $idcardInfo['result'];
        } else {
            $idcardInfo = '';
        }

        if ('POST' == $request->getMethod()) {
            $data = $request->request->all();

            if ('success' == $data['form_status']) {
                $this->getUserService()->passApproval($id, $data['note']);
            } elseif ('fail' == $data['form_status']) {
                if ($this->isPluginInstalled('TeacherAudit')) {
                    $approval = $this->getTeacherAuditService()->getApprovalByUserId($user['id']);

                    if (!empty($approval)) {
                        $this->getTeacherAuditService()->rejectApproval($user['id'], '教师资格申请因实名认证未通过而失败');
                    }
                }

                $this->getUserService()->rejectApproval($id, $data['note']);
            }

            return $this->createJsonResponse(array('status' => 'ok'));
        }

        return $this->render('@Custom/admin/user/user-approve-modal.html.twig',
            array(
                'user' => $user,
                'userApprovalInfo' => $userApprovalInfo,
                'idcardInfo' => $idcardInfo,
            )
        );
    }
}