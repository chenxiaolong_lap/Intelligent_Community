<?php

namespace CustomBundle\Controller\Admin;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Controller\Admin\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;

class ClinicController extends BaseController
{
    public function indexAction(Request $request)
    {
        $fields = $request->query->all();

        $conditions = array(
            'keyword' => '',
            'keywordType' => ''
        );

        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'name') {
                $conditions['name'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'phone') {
                $conditions['phone'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'email') {
                $conditions['email'] = $conditions['keyword'];
            }
        }

        if (!empty($conditions['provinceQuery'])) {
            $conditions['province'] = $conditions['provinceQuery'];
            if (!empty($conditions['cityQuery'])) {
                $conditions['city'] = $conditions['cityQuery'];
            }

            if (!empty($conditions['districtQuery'])) {
                $conditions['district'] = $conditions['districtQuery'];
            }

            $communities = $this->getCommunityService()->searchCommunity(
                array(
                    'province' => !empty($conditions['province'])? $conditions['province']:'',
                    'city' => !empty($conditions['city']) ? $conditions['city'] : '',
                    'district' => !empty($conditions['district']) ? $conditions['district'] : '',
                ),
                array(),
                0,
                PHP_INT_MAX
            );

            $conditions['communityIds'] = !empty(ArrayToolkit::column($communities, 'id'))? ArrayToolkit::column($communities, 'id') : array(-1);
        }

        $count = $this->getClinicService()->countClinic($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $clinics = $this->getClinicService()->searchClinics(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $communityIds = ArrayToolkit::column($clinics, 'communityId');

        $communities = $this->getCommunityService()->searchCommunity(
            array('ids' => $communityIds),
            array(),
            0,
            PHP_INT_MAX
        );
        $communities = ArrayToolkit::index($communities, 'id');

        return $this->render('@Custom/admin/clinic/index.html.twig', array(
            'communities' => $communities,
            'paginator' => $paginator,
            'clinicCount' => $count,
            'clinics' => $clinics
        ));
    }

    public function showAction($id)
    {
        $clinic = $this->getClinicService()->getClinic($id);

        if (empty($clinic)) {
            throw $this->createNotFoundException('对不起，没有查询到该诊所');
        }

        $clinic['legalPersonIdcard'] = substr_replace($clinic['legalPersonIdcard'], '************', 4, 12);

        $doctors = $this->getUserService()->findUsersByIds($clinic['doctorIds']);
        $doctorProfiles = $this->getUserService()->findUserProfilesByIds($clinic['doctorIds']);
        $nurses = $this->getUserService()->findUsersByIds($clinic['nurseIds']);
        $nurseProfiles = $this->getUserService()->findUserProfilesByIds($clinic['nurseIds']);
        $managers = $this->getUserService()->findUsersByIds($clinic['manageIds']);
        $managerProfiles = $this->getUserService()->findUserProfilesByIds($clinic['manageIds']);
        $community = $this->getCommunityService()->getCommunity($clinic['communityId']);
        $doctorProfiles = ArrayToolkit::index($doctorProfiles, 'id');
        $nurseProfiles = ArrayToolkit::index($nurseProfiles, 'id');
        $managerProfiles = ArrayToolkit::index($managerProfiles, 'id');

        return $this->render('@Custom/admin/clinic/show-modal.html.twig', array(
            'clinic' => $clinic,
            'doctors' => $doctors,
            'nurses' => $nurses,
            'managers' => $managers,
            'community' => $community,
            'doctorProfiles' => $doctorProfiles,
            'nurseProfiles' => $nurseProfiles,
            'managerProfiles' => $managerProfiles
        ));
    }

    public function editAction(Request $request, $id)
    {
        $clinic = $this->getClinicService()->getClinic($id);

        $doctors = $this->getUserService()->findUsersByIds($clinic['doctorIds']);
        $doctorProfiles = $this->getUserService()->findUserProfilesByIds($clinic['doctorIds']);
        $nurses = $this->getUserService()->findUsersByIds($clinic['nurseIds']);
        $nurseProfiles = $this->getUserService()->findUserProfilesByIds($clinic['nurseIds']);
        $managers = $this->getUserService()->findUsersByIds($clinic['manageIds']);
        $managerProfiles = $this->getUserService()->findUserProfilesByIds($clinic['manageIds']);
        $community = $this->getCommunityService()->getCommunity($clinic['communityId']);
        $doctorProfiles = ArrayToolkit::index($doctorProfiles, 'id');
        $nurseProfiles = ArrayToolkit::index($nurseProfiles, 'id');
        $managerProfiles = ArrayToolkit::index($managerProfiles, 'id');

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $this->getClinicService()->updateClinic($id, $fields);

            return $this->createJsonResponse(array(
                'status' => true
            ));
        }

        return $this->render('@Custom/admin/clinic/modal.html.twig', array(
            'clinic' => $clinic,
            'doctors' => $doctors,
            'nurses' => $nurses,
            'managers' => $managers,
            'community' => $community,
            'doctorProfiles' => $doctorProfiles,
            'nurseProfiles' => $nurseProfiles,
            'managerProfiles' => $managerProfiles
        ));
    }

    protected function getClinicService()
    {
        return $this->createService('CustomBundle:Clinic:ClinicService');
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}