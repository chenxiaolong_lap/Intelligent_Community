<?php

namespace CustomBundle\Controller\Admin;

use AppBundle\Common\Paginator;
use AppBundle\Common\ArrayToolkit;
use CustomBundle\Biz\Certificate\Service\CertificateService;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\Admin\UserController as BaseUserController;

class UserController extends BaseUserController
{
    public function indexAction(Request $request)
    {
        $fields = $request->query->all();

        $conditions = array(
            'roles' => '',
            'keywordType' => '',
            'keyword' => '',
            'keywordUserType' => '',
        );

        $conditions = array_merge($conditions, $fields);
        $conditions = $this->fillOrgCode($conditions);

        $userCount = $this->getUserService()->countUsers($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $userCount,
            20
        );

        $users = $this->getUserService()->searchUsers(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        //根据mobile查询user_profile获得userIds

        if (isset($conditions['keywordType']) && $conditions['keywordType'] == 'verifiedMobile' && !empty($conditions['keyword'])) {
            $profilesCount = $this->getUserService()->searchUserProfileCount(array('mobile' => $conditions['keyword']));
            $userProfiles = $this->getUserService()->searchUserProfiles(
                array('mobile' => $conditions['keyword']),
                array('id' => 'DESC'),
                0,
                $profilesCount
            );
            $userIds = ArrayToolkit::column($userProfiles, 'id');

            if (!empty($userIds)) {
                unset($conditions['keywordType']);
                unset($conditions['keyword']);
                $conditions['userIds'] = array_merge(ArrayToolkit::column($users, 'userId'), $userIds);
            }

            $userCount = $this->getUserService()->countUsers($conditions);
            $paginator = new Paginator(
                $this->get('request'),
                $userCount,
                20
            );

            $users = $this->getUserService()->searchUsers(
                $conditions,
                array('createdTime' => 'DESC'),
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
        }

        $app = $this->getAppService()->findInstallApp('UserImporter');

        $showUserExport = false;

        if (!empty($app) && array_key_exists('version', $app)) {
            $showUserExport = version_compare($app['version'], '1.0.2', '>=');
        }

        $userIds = ArrayToolkit::column($users, 'id');
        $profiles = $this->getUserService()->findUserProfilesByIds($userIds);

        $allRoles = $this->getAllRoles();

        return $this->render('@Custom/admin/user/index.html.twig', array(
                'users' => $users,
                'allRoles' => $allRoles,
                'userCount' => $userCount,
                'paginator' => $paginator,
                'profiles' => $profiles,
                'showUserExport' => $showUserExport,
            )
        );
    }

    public function showAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);
        $profile = $this->getUserService()->getUserProfile($id);
        $profile['title'] = $user['title'];
        $fields = $this->getFields();
        $certificates = $this->getCertificateService()->findCertificatesByUserId($id);

        return $this->render('@Custom/admin/user/show-modal.html.twig', array(
            'user'         => $user,
            'profile'      => $profile,
            'fields'       => $fields,
            'certificates' => $certificates,
        ));
    }

    public function editAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);

        $profile = $this->getUserService()->getUserProfile($user['id']);
        $profile['title'] = $user['title'];

        if ($request->getMethod() === 'POST') {
            $profile = $request->request->all();

            if (!((strlen($user['verifiedMobile']) > 0) && isset($profile['mobile']))) {
                $profile = $this->getUserService()->updateUserProfile($user['id'], $profile);
                $this->getLogService()->info('user', 'edit', "管理员编辑用户资料 {$user['nickname']} (#{$user['id']})", $profile);
            } else {
                return $this->createJsonResponse(array(
                    'success' => false,
                    'message' => 'user.settings.profile.unable_change_bind_mobile',
                ));
            }

            return $this->createJsonResponse(array(
                'success' => true,
            ));
        }
        $fields      = $this->getFields();

        return $this->render('admin/user/edit-modal.html.twig', array(
            'user'        => $user,
            'profile'     => $profile,
            'fields'      => $fields,
        ));
    }

    public function setCommunityRoleAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);

        if ($request->getMethod() == 'POST') {
            $communityRoles = $request->request->get('roles');

            $updateProfile = $this->getUserService()->setCommunityRole($id, $communityRoles);

            return $this->createJsonResponse(array('status' => true, 'message' => '设置用户社区角色成功'));
        }

        return $this->render('@Custom/admin/user/community-role-modal.html.twig', array(
            'user' => $user
        ));
    }

    protected function getUserService()
    {
        return $this->createService('CustomBundle:User:UserService');
    }

    /**
     * @return CertificateService
     */
    protected function getCertificateService()
    {
        return $this->createService('CustomBundle:Certificate:CertificateService');
    }
}
