<?php

namespace CustomBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use Symfony\Component\HttpFoundation\Request;

class PropertyController extends BaseController
{
    public function indexAction(Request $request)
    {
        $fields = $request->query->all();

        $conditions = array(
            'keyword' => '',
            'keywordType' => ''
        );

        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'name') {
                $conditions['name'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'servicePhone') {
                $conditions['servicePhone'] = $conditions['keyword'];
            }
        }

        $count = $this->getPropertyService()->countProperty($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );
        $properties = $this->getPropertyService()->searchProperty(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $conditions = $this->queryByCommunity($conditions);
        $conditions = $this->queryByHousingEstate($conditions);
        $conditions = $this->queryByUser($conditions);

        if (!empty($conditions['propertyIds'])) {
            $count = $this->getPropertyService()->countProperty($conditions);
            $paginator = new Paginator(
                $this->get('request'),
                $count,
                20
            );
            $properties = $this->getPropertyService()->searchProperty(
                $conditions,
                array('createdTime' => 'DESC'),
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
        } elseif (isset($conditions['propertyIds']) && empty($conditions['propertyIds'])) {
            $count = 0;
            $paginator = new Paginator(
                $this->get('request'),
                $count,
                1
            );
            $properties = array();
        }

        $housingEstateIds = ArrayToolkit::column($properties, 'housingEstateId');
        $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
            array('housingEstateIds' => $housingEstateIds),
            array(),
            0,
            PHP_INT_MAX
        );

        $communityIds = ArrayToolkit::column($housingEstates, 'communityId');
        $communities = $this->getCommunityService()->searchCommunity(
            array('communityIds' => $communityIds),
            array(),
            0,
            PHP_INT_MAX
        );

        $housingEstates = ArrayToolkit::index($housingEstates, 'id');
        $communities = ArrayToolkit::index($communities, 'id');

        return $this->render('@Custom/admin/property/index.html.twig', array(
            'paginator' => $paginator,
            'propertyCount' => $count,
            'properties' => $properties,
            'housingEstates' => $housingEstates,
            'communities' => $communities,
        ));
    }

    public function showAction($id)
    {
        $property = $this->getPropertyService()->getProperty($id);

        $directors = $this->getUserService()->findUsersByIds($property['directorIds']);
        $housingEstate = $this->getHousingEstateService()->getHousingEstate($property['housingEstateId']);
        $community = $this->getCommunityService()->getCommunity($housingEstate['communityId']);

        $userCount = $this->getUserService()->searchUserProfiles(
            array(
                'province' => $community['province'],
                'city' => $community['city'],
                'district' => $community['district'],
                'community' => $community['name'],
                'housingEstate' => $housingEstate['name']
            ),
            array(),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/admin/property/show-modal.html.twig', array(
            'directors' => $directors,
            'property' => $property,
            'userCount' => $userCount,
            'housingEstate' => $housingEstate,
            'community' => $community,
        ));
    }

    public function createAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $users = $fields['director'];

            $directorIds = array();
            foreach ($users as $user) {
                $user = $this->getUserService()->getUserByNickname($user);
                if (empty($user)) {
                    return $this->createMessageResponse('error', '未查询到<'.$user.'>用户,请确保用户名填写正确！');
                }
                $directorIds[] = $user['id'];
            }

            $housingEstateName = $fields['housingEstateName'];
            $housingEstate = $this->getHousingEstateService()->getHousingEstateByName($housingEstateName);
            unset($fields['director']);
            unset($fields['housingEstateName']);
            $fields = array_merge($fields, array('directorIds' => $directorIds, 'housingEstateId' => $housingEstate['id']));

            $this->getPropertyService()->createProperty($fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/admin/property/create-modal.html.twig');
    }

    public function deleteAction($id)
    {
        $property = $this->getPropertyService()->getProperty($id);

        if (!empty($property)) {
            throw $this->createAccessDeniedException('对不起，不存在该小区物业公司！');
        }

        $this->getPropertyService()->deleteProperty($id);

        return $this->createJsonResponse(array('status' => true));
    }

    public function editAction(Request $request, $id)
    {
        $property = $this->getPropertyService()->getProperty($id);
        $userIds = $property['director'];
        $userIds = json_decode($userIds, true);
        $directors = $this->getUserService()->findUsersByIds($userIds);

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $this->getPropertyService()->updateProperty($id, $fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/admin/property/edit-modal.html.twig', array(
            'property' => $property,
            'directors' => $directors
        ));
    }

    protected function queryByCommunity($conditions)
    {
        //根据地区，社区名称查询communityIds
        if ((isset($conditions['keywordType']) && $conditions['keywordType'] == 'communityName' && !empty($conditions['keyword'])) || !empty($conditions['provinceQuery'])) {
            $communityQuery = array();
            if ($conditions['keywordType'] == 'communityName') {
                $communityQuery['community'] = $conditions['keyword'];
            }

            if (!empty($conditions['provinceQuery'])) {
                $communityQuery['province'] = $conditions['provinceQuery'];
                if (!empty($conditions['cityQuery'])) {
                    $communityQuery['city'] = $conditions['cityQuery'];
                }
                if (!empty($conditions['districtQuery'])) {
                    $communityQuery['district'] = $conditions['districtQuery'];
                }
            }

            $communityCount = $this->getCommunityService()->countCommunity($communityQuery);
            $communities = $this->getCommunityService()->searchCommunity(
                $communityQuery,
                array(),
                0,
                $communityCount
            );

            $communityIds = ArrayToolkit::column($communities, 'id');
            if (empty($communityIds)) {
                $conditions['propertyIds'] = array();
            } else {
                $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
                    array('communityIds' => $communityIds),
                    array(),
                    0,
                    PHP_INT_MAX
                );
                $housingEstateIds = ArrayToolkit::column($housingEstates, 'id');

                if (empty($housingEstateIds)) {
                    $conditions['propertyIds'] = array();
                } else {
                    $properties = $this->getPropertyService()->searchProperty(
                        array('housingEstateIds' => $housingEstateIds),
                        array(),
                        0,
                        PHP_INT_MAX
                    );
                    $propertyIds = ArrayToolkit::column($properties, 'id');
                    $conditions['propertyIds'] = $propertyIds;
                }
            }
        }

        return $conditions;
    }

    public function queryByHousingEstate($conditions)
    {
        if ((isset($conditions['keywordType']) && $conditions['keywordType'] == 'housingEstateName' && !empty($conditions['keyword']))) {
            $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
                array('name' => $conditions['keyword']),
                array(),
                0,
                PHP_INT_MAX
            );
            $housingEstateIds = ArrayToolkit::column($housingEstates, 'id');

            if (empty($housingEstateIds)) {
                $conditions['propertyIds'] = array();
            } else {
                $properties = $this->getPropertyService()->searchProperty(
                    array('housingEstateIds' => $housingEstateIds),
                    array(),
                    0,
                    PHP_INT_MAX
                );
                $propertyIds = ArrayToolkit::column($properties, 'id');

                if (isset($conditions['propertyIds'])) {
                    $conditions['propertyIds'] = array_intersect($propertyIds, $conditions['propertyIds']);
                } else {
                    $conditions['propertyIds'] = $propertyIds;
                }
            }
        }

        return $conditions;
    }

    public function queryByUser($conditions)
    {
        if ((isset($conditions['keywordType']) && $conditions['keywordType'] == 'director' && !empty($conditions['keyword']))) {
            $users = $this->getUserService()->searchUsers(
                array('nickname' => $conditions['keyword']),
                array(),
                0,
                PHP_INT_MAX
            );

            $userIds = ArrayToolkit::column($users, 'id');

            $conditions['directorIds'] = $userIds;
        }

        return $conditions;
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getHousingEstateService()
    {
        return $this->createService('CustomBundle:HousingEstate:HousingEstateService');
    }

    protected function getPropertyService()
    {
        return $this->createService('CustomBundle:Property:PropertyService');
    }
}