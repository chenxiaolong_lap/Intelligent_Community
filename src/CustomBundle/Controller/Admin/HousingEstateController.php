<?php

namespace CustomBundle\Controller\Admin;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;
use AppBundle\Common\ArrayToolkit;

class HousingEstateController extends BaseController
{
    public function indexAction(Request $request)
    {
        $fields = $request->query->all();

        $conditions = array(
            'keyword' => '',
            'keywordType' => ''
        );

        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'name') {
                $conditions['name'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'servicePhone') {
                $conditions['servicePhone'] = $conditions['keyword'];
            }
        }

        $count = $this->getHousingEstateService()->countHousingEstate($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $conditions = $this->queryByCommunity($conditions);

        if (!empty($conditions['communityIds'])) {
            $count = $this->getHousingEstateService()->countHousingEstate($conditions);
            $paginator = new Paginator(
                $this->get('request'),
                $count,
                20
            );

            $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
                $conditions,
                array('createdTime' => 'DESC'),
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
        } elseif (isset($conditions['communityIds']) && empty($conditions['communityIds'])) {
            $count = 0;
            $paginator = new Paginator(
                $this->get('request'),
                $count,
                1
            );

            $housingEstates = array();
        }

        $communityIds = ArrayToolkit::column($housingEstates, 'communityId');
        $communities = $this->getCommunityService()->searchCommunity(
            array('communityIds' => $communityIds),
            array(),
            0,
            PHP_INT_MAX
        );
        $communities = ArrayToolkit::index($communities, 'id');

        return $this->render('@Custom/admin/housing-estate/index.html.twig', array(
            'housingEstates' => $housingEstates,
            'paginator' => $paginator,
            'housingEstateCount' => $count,
            'communities' => $communities
        ));
    }

    public function createAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $this->getHousingEstateService()->createHousingEstate($fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/admin/housing-estate/create-modal.html.twig');
    }

    public function editAction(Request $request, $id)
    {
        $housingEstate = $this->getHousingEstateService()->getHousingEstate($id);

        $community = $this->getCommunityService()->getCommunity($housingEstate['communityId']);

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $communities = $this->getCommunityService()->findCommunitiesByLikeName($fields['communityName']);

            if (empty($communities)) {
                return $this->createJsonResponse(array(
                    'status' => false,
                    'message' => '对不起，不存在该社区，请先到社区管理中创建社区！'
                ));
            }

            $this->getHousingEstateService()->createHousingEstate($fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/admin/housing-estate/edit-modal.html.twig', array(
            'housingEstate' => $housingEstate,
            'community' => $community
        ));
    }

    public function deleteAction($id)
    {
        $housingEstate = $this->getHousingEstateService()->getHousingEstate($id);
        $community = $this->getCommunityService()->getCommunity($housingEstate['communityId']);

        $userCount = $this->getUserService()->searchUserProfileCount(
            array(
                'province' => $community['province'],
                'city' => $community['city'],
                'district' => $community['district'],
                'community' => $community['name'],
                'housingEstate' => $housingEstate['name']
            )
        );

        if (!empty($userCount)) {
            return $this->createJsonResponse(array(
                'status' => false,
                'message' => '对不起该小区下存在着用户信息，不能删除该小区！'
            ));
        }

        $this->getHousingEstateService()->deleteHousingEstate($id);

        return $this->createJsonResponse(array('status' => true));
    }

    public function showAction($id)
    {
        $housingEstate = $this->getHousingEstateService()->getHousingEstate($id);

        $community = $this->getCommunityService()->getCommunity($housingEstate['communityId']);

        return $this->render('@Custom/admin/housing-estate/show-modal.html.twig', array(
            'housingEstate' => $housingEstate,
            'community' => $community
        ));
    }

    public function queryCountUserAction($province, $city, $district, $community, $housingEstate)
    {
        $fields = array(
            'province' => $province,
            'city' => $city,
            'district' => $district,
            'community' => $community,
            'housingEstate' => $housingEstate
        );

        $count = $this->getUserService()->searchUserProfileCount(
            $fields,
            array(),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/admin/housing-estate/query-user-count.html.twig', array('count' => $count));
    }

    protected function queryByCommunity($conditions)
    {
        //根据地区，社区名称查询communityIds
        if ((isset($conditions['keywordType']) && $conditions['keywordType'] == 'communityName' && !empty($conditions['keyword'])) || !empty($conditions['provinceQuery'])) {
            $communityQuery = array();
            if ($conditions['keywordType'] == 'communityName') {
                $communityQuery['community'] = $conditions['keyword'];
            }

            if (!empty($conditions['provinceQuery'])) {
                $communityQuery['province'] = $conditions['provinceQuery'];
                if (!empty($conditions['cityQuery'])) {
                    $communityQuery['city'] = $conditions['cityQuery'];
                }
                if (!empty($conditions['districtQuery'])) {
                    $communityQuery['district'] = $conditions['districtQuery'];
                }
            }

            $communityCount = $this->getCommunityService()->countCommunity($communityQuery);
            $communities = $this->getCommunityService()->searchCommunity(
                $communityQuery,
                array(),
                0,
                $communityCount
            );

            $communityIds = ArrayToolkit::column($communities, 'id');
            $conditions['communityIds'] = $communityIds;
        }

        return $conditions;
    }
    
    protected function getHousingEstateService()
    {
        return $this->createService('CustomBundle:HousingEstate:HousingEstateService');
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}