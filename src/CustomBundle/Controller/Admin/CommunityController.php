<?php

namespace CustomBundle\Controller\Admin;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;

class CommunityController extends BaseController
{
    public function indexAction(Request $request)
    {
        $fields = $request->query->all();

        $conditions = array(
            'keyword' => '',
            'keywordType' => ''
        );

        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'name') {
                $conditions['name'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'officePhone') {
                $conditions['officePhone'] = $conditions['keyword'];
            }
        }

        if (!empty($conditions['provinceQuery'])) {
            $conditions['province'] = $conditions['provinceQuery'];
            if (!empty($conditions['cityQuery'])) {
                $conditions['city'] = $conditions['cityQuery'];
            }

            if (!empty($conditions['districtQuery'])) {
                $conditions['district'] = $conditions['districtQuery'];
            }
        }

        $count = $this->getCommunityService()->countCommunity($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $communities = $this->getCommunityService()->searchCommunity(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $communityIds = ArrayToolkit::column($communities, 'id');

        $countHousingEstates = $this->getHousingEstateService()->findCountHousingEstateByCommunityIds($communityIds);
        $countHousingEstates = ArrayToolkit::index($countHousingEstates, 'communityId');

        return $this->render('@Custom/admin/community/index.html.twig', array(
            'communities' => $communities,
            'paginator' => $paginator,
            'communityCount' => $count,
            'countHousingEstates' => $countHousingEstates
        ));
    }

    public function createAction(Request $request)
    {
        $user = $this->getCurrentUser();

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $community = $this->getCommunityService()->createCommunity($fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/admin/community/create-modal.html.twig');
    }

    public function editCommunityAction(Request $request, $id)
    {
        $community = $this->getCommunityService()->getCommunity($id);

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $community = $this->getCommunityService()->updateCommunity($id, $fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/admin/community/edit-modal.html.twig', array('community' => $community));
    }

    public function deleteCommunityAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();

        if (!$user->isAdmin() || !$user->isSuperAdmin()) {
            throw $this->createAccessDeniedException('对不起，你没有权限删除');
        }

        $community = $this->getCommunityService()->getCommunity($id);

        $housingEstateCount = $this->getHousingEstateService()->countHousingEstate(array('communityId' => $community['id']));

        $userCount = $this->getUserService()->searchUserProfileCount(array(
            'province' => $community['province'],
            'city' => $community['city'],
            'district' => $community['district'],
            'community' => $community['name']
        ));

        if (!empty($housingEstateCount) || !empty($userCount)) {
            return $this->createJsonResponse(array(
                'status' => false,
                'message' => '对不起该社区下存在小区信息或用户信息，不能删除！'
            ));
        }

        $this->getCommunityService()->deleteCommunity($id);

        return $this->createJsonResponse(array('status' => true));
    }

    public function showAction($id)
    {
        $community = $this->getCommunityService()->getCommunity($id);

        $countHousingEstate = $this->getHousingEstateService()->countHousingEstate(array('communityId' => $id));
        $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
            array('communityId' => $id),
            array(),
            0,
            PHP_INT_MAX
        );

        $countUser = $this->getUserService()->searchUserProfileCount(array(
            'community' => $community['name'],
            'province' => $community['province'],
            'city' => $community['city'],
            'district' => $community['district']
        ));

        return $this->render('@Custom/admin/community/show-modal.html.twig', array(
            'community' => $community,
            'countHousingEstate' => $countHousingEstate,
            'housingEstates' => $housingEstates,
            'countUser' => $countUser,
        ));
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getHousingEstateService()
    {
        return $this->createService('CustomBundle:HousingEstate:HousingEstateService');
    }
}