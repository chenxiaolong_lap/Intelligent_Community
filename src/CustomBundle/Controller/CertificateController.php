<?php

namespace CustomBundle\Controller;

use AppBundle\Common\CurlToolkit;
use AppBundle\Controller\BaseController;
use Biz\User\Service\TokenService;
use CustomBundle\Biz\Certificate\Service\CertificateService;
use Symfony\Component\HttpFoundation\Request;

class CertificateController extends BaseController
{
    public function callbackAction(Request $request)
    {
        $token = $request->query->get('token');
        $token = $this->getTokenService()->verifyToken('certificate.callback', $token);

        if (empty($token)) {
            return $this->createJsonResponse(array('result' => -1, 'msg' => '请求非法'));
        }

        $result = $request->request->get('ResultJson');
        $result = $result ? json_decode($result, true) : array();

        if (!empty($result['certificateList'])) {
            $this->getCertificateService()->syncCertificates(
                $token['userId'],
                $result['certificateList']
            );
        }

        return $this->createJsonResponse(array('result' => 0, 'msg' => 'ok'));
    }

    /**
     * @return CertificateService
     */
    protected function getCertificateService()
    {
        return $this->createService('CustomBundle:Certificate:CertificateService');
    }

    protected function getLogService()
    {
        return $this->createService('System:LogService');
    }

    /**
     * @return TokenService
     */
    protected function getTokenService()
    {
        return $this->createService('User:TokenService');
    }

    protected function getUserService()
    {
        return $this->createService('User.UserService');
    }
}