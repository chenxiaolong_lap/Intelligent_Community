<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\BaseController;
use AppBundle\Common\Paginator;
use AppBundle\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;

class AdviceController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        $conditions = array(
            'communityId' => $community['id']
        );

        $count = $this->getAdviceService()->countAdvices($conditions);

        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $advices = $this->getAdviceService()->searchAdvices(
            $conditions,
            array(
                'createdTime' => 'DESC'
            ),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($advices, 'userId'));
        $users = ArrayToolkit::index($users, 'id');

        return $this->render('@Custom/advice-box/index.html.twig', array(
            'advices' => $advices,
            'count' => $count,
            'paginator' => $paginator,
            'users' => $users
        ));
    }

    public function createAction(Request $request)
    {
        $user = $this->getCurrentUser();
        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $community = $this->getCommunity();
            $fields['communityId'] = $community['id'];
            $fields['userId'] = $user['id'];

            $this->getAdviceService()->createAdvice($fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/advice-box/create-modal.html.twig');
    }

    public function showAction($id)
    {
        $advice = $this->getAdviceService()->getAdvice($id);

        return $this->render('@Custom/advice-box/show-modal.html.twig', array(
            'advice' => $advice
        ));
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        return $community;
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getAdviceService()
    {
        return $this->createService('CustomBundle:Advice:AdviceService');
    }
}