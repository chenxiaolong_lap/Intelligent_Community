<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;
use AppBundle\Common\ArrayToolkit;

class ClinicReservationRecordController extends BaseController
{
    public function listOfClinicAction(Request $request, $clinicId)
    {
        $conditions = $request->query->all();
        $conditions['clinicId'] = $clinicId;

        if (!empty($conditions['nickname'])) {
            $users = $this->getUserService()->findUsersByLikeNickname($conditions['nickname']);
            $conditions['userIds'] = !empty($users)? ArrayToolkit::column($users, 'id') : array(-1);
        }

        $count = $this->getClinicReservationRecordService()->countReservationRecord($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $records = $this->getClinicReservationRecordService()->searchReservationRecords(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $userIds = ArrayToolkit::column($records, 'userId');
        $users = $this->getUserService()->searchUsers(
            array('ids' => $userIds),
            array(),
            0,
            PHP_INT_MAX
        );
        $users = ArrayToolkit::index($users, 'id');

        return $this->render('@Custom/clinic/show-tabs/record.html.twig', array(
            'records' => $records,
            'users' => $users,
            'paginator' => $paginator,
            'count' => $count,
            'clinicId' => $clinicId,
            'conditions' => $conditions
        ));
    }

    public function showEstimateAction($userId, $recordId)
    {
        $record = $this->getClinicReservationRecordService()->getClinicReservationRecord($recordId);
        $user = $this->getUserService()->getUser($userId);

        return $this->render('@Custom/clinic/show/show-estimate-modal.html.twig', array(
            'record' => $record,
            'user' => $user
        ));
    }

    public function userOfClinicListAction($userId, $clinicId)
    {
        $records = $this->getClinicReservationRecordService()->searchReservationRecords(
            array(
                'userId' => $userId,
                'clinicId' => $clinicId,
            ),
            array('createdTime' => 'DESC'),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/clinic/bespeak/record-list.html.twig', array(
            'records' => $records
        ));
    }

    public function showAction($id)
    {
        $record = $this->getClinicReservationRecordService()->getClinicReservationRecord($id);

        if (empty($record)) {
            throw $this->createNotFoundException('对不起，不存在该记录');
        }

        $user = $this->getUserService()->getUser($record['userId']);
        $userProfile = $this->getUserService()->getUserProfile($record['userId']);

        $doctors = $this->getUserService()->findUsersByIds($record['doctorIds']);
        $doctorProfiles = $this->getUserService()->findUserProfilesByIds($record['doctorIds']);

        $nurses = $this->getUserService()->findUsersByIds($record['nurseIds']);
        $nurseProfiles = $this->getUserService()->findUserProfilesByIds($record['nurseIds']);

        $clinic = $this->getClinicService()->getClinic($record['clinicId']);
        $doctorProfiles = ArrayToolkit::index($doctorProfiles, 'id');
        $nurseProfiles = ArrayToolkit::index($nurseProfiles, 'id');

        $updateUsers = $this->getUserService()->findUsersByIds($record['updateUserIds']);

        return $this->render('@Custom/my/clinic/record/show-modal.html.twig', array(
            'record' => $record,
            'user' => $user,
            'userProfile' => $userProfile,
            'clinic' => $clinic,
            'doctors' => $doctors,
            'doctorProfiles' => $doctorProfiles,
            'nurses' => $nurses,
            'nurseProfiles' => $nurseProfiles,
            'updateUsers' => $updateUsers
        ));
    }

    protected function getClinicService()
    {
        return $this->createService('CustomBundle:Clinic:ClinicService');
    }

    protected function getClinicReservationRecordService()
    {
        return $this->createService('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }
}