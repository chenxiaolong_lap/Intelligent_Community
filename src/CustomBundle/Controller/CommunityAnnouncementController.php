<?php

namespace CustomBundle\Controller;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;

class CommunityAnnouncementController extends BaseController
{
    public function indexAction(Request $request, $type = '')
    {
        $community = $this->getCommunity();
        $conditions = $request->query->all();
        $conditions['communityId'] = $community['id'];

        if ($type == '警告') {
            $conditions['targetType'] = '警告';
        } elseif ($type == '温馨提示') {
            $conditions['targetType'] = '温馨提示';
        } elseif ($type == '通知') {
            $conditions['targetType'] = '通知';
        }

        if (!empty($conditions['filter'])) {
            if ($conditions['filter'] == 'noStart') {
                $conditions['startTime_GT'] = time();
            } elseif ($conditions['filter'] == 'underway') {
                $conditions['endTime_GE'] = time();
                $conditions['startTime_LE'] = time();
            } elseif ($conditions['filter'] == 'finish') {
                $conditions['endTime_LT'] = time();
            }
        }

        $paginator = new Paginator(
            $this->get('request'),
            $this->getCommunityAnnouncementService()->countCommunityAnnouncement($conditions),
            10
        );

        $announcements = $this->getCommunityAnnouncementService()->searchCommunityAnnouncement(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('@Custom/community-announcement/index.html.twig', array(
            'paginator' => $paginator,
            'announcements' => $announcements,
            'targetType' => $type,
            'filter' => !empty($conditions['filter']) ? $conditions['filter'] : ''
        ));
    }

    public function defaultIndexBlockAction()
    {
        $community = $this->getCommunity();
        $conditions = array();
        $conditions['communityId'] = $community['id'];

        $conditions['startTime_GT'] = time();
        $noStartActivities = $this->getCommunityAnnouncementService()->searchCommunityAnnouncement(
            $conditions,
            array('startTime' => 'DESC'),
            0,
            7
        );
        unset($conditions['startTime_GT']);

        $conditions['endTime_GE'] = time();
        $conditions['startTime_LE'] = time();
        $underwayActivities = $this->getCommunityAnnouncementService()->searchCommunityAnnouncement(
            $conditions,
            array('startTime' => 'ASC'),
            0,
            7
        );
        unset($conditions['endTime_GE']);
        unset($conditions['startTime_LE']);

        $conditions['endTime_LT'] = time();
        $finishActivities = $this->getCommunityAnnouncementService()->searchCommunityAnnouncement(
            $conditions,
            array('endTime' => 'DESC'),
            0,
            7
        );

        return $this->render('@Custom/my/default/announcement/block.html.twig', array(
            'noStartActivities' => $noStartActivities,
            'underActivities' => $underwayActivities,
            'finishActivities' => $finishActivities,
        ));
    }

    public function showAction(Request $request, $id)
    {
        $announcement = $this->getCommunityAnnouncementService()->getCommunityAnnouncement($id);

        $conditions = array(
            'targetId' => $id,
            'targetType' => 'community-announcement',
            'parentId' => 0,
        );

        $postNum = $this->getThreadService()->searchPostsCount($conditions);
        $paginator = new Paginator(
            $request,
            $postNum,
            10
        );

        $posts = $this->getThreadService()->searchPosts(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($posts, 'userId'));

        return $this->render('@Custom/my/announcement/preview.html.twig', array(
            'announcement' => $announcement,
            'posts' => $posts,
            'users' => $users,
            'postNum' => $postNum,
            'paginator' => $paginator,
            'service' => $this->getThreadService(),
        ));
    }

    public function postAction(Request $request, $id)
    {
        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $post['content'] = $fields['content'];
            $post['targetType'] = 'community-announcement';
            $post['targetId'] = $id;

            $user = $this->getCurrentUser();

            if (!$user->isLogin()) {
                throw $this->createAccessDeniedException('用户没有登录,不能评论!');
            }

            $post = $this->getThreadService()->createPost($post);

            return $this->render(
                'thread/part/post-item.html.twig',
                array(
                    'post' => $post,
                    'author' => $user,
                    'service' => $this->getThreadService(),
//                    'postReplyUrl' => $this->generateUrl(
//                        'article_post_reply',
//                        array('articleId' => $id, 'postId' => $post['id'])
//                    ),
                )
            );
        }
    }

    public function postReplyAction(Request $request, $id, $postId)
    {
        $fields = $request->request->all();
        $fields['content'] = $this->autoParagraph($fields['content']);
        $fields['targetId'] = $id;
        $fields['targetType'] = 'community-announcement';
        $fields['parentId'] = $postId;

        $post = $this->getThreadService()->createPost($fields);

        return $this->render(
            'thread/subpost-item.html.twig',
            array(
                'post' => $post,
                'author' => $this->getCurrentUser(),
                'service' => $this->getThreadService(),
            )
        );
    }

    protected function autoParagraph($text)
    {
        if (trim($text) !== '') {
            $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');
            $text = preg_replace("/\n\n+/", "\n\n", str_replace(array("\r\n", "\r"), "\n", $text));
            $texts = preg_split('/\n\s*\n/', $text, -1, PREG_SPLIT_NO_EMPTY);
            $text = '';

            foreach ($texts as $txt) {
                $text .= '<p>'.nl2br(trim($txt, "\n"))."</p>\n";
            }

            $text = preg_replace('|<p>\s*</p>|', '', $text);
        }

        return $text;
    }

    protected function getCommentService()
    {
        return $this->getBiz()->service('Content:CommentService');
    }

    protected function getCommunityAnnouncementService()
    {
        return $this->createService('CustomBundle:CommunityAnnouncement:CommunityAnnouncementService');
    }

    protected function getThreadService()
    {
        return $this->getBiz()->service('Thread:ThreadService');
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}