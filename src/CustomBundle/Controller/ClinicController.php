<?php

namespace CustomBundle\Controller;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class ClinicController extends BaseController
{
    public function applyAction(Request $request)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            return $this->createMessageResponse('info', '请先登录', '', 3, $this->generateUrl('login'));
        }

        $user = $this->getUserService()->getUser($user['id']);

        if ($user['approvalStatus'] == 'unapprove' || $user['approvalStatus'] == 'approve_fail') {
            return $this->createMessageResponse('info', '对不起,诊所申请,用户必须实名认证.');
        }

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $communityRegion = explode('/', strstr($fields['community'], '--', true));
            $community = $this->getCommunityService()->getCommunityByRegionAndName(
                $communityRegion[0],
                $communityRegion[1],
                $communityRegion[2],
                substr(strstr($fields['community'], '--'), 2)
            );
            $fields['communityId'] = $community['id'];
            unset($fields['community']);

            $this->getClinicService()->createClinic($fields);

            return $this->createJsonResponse(
                array('status' => 'success')
            );
        }

        return $this->render('@Custom/clinic/index.html.twig');
    }

    public function markCropModalAction(Request $request)
    {
        $currentUser = $this->getCurrentUser();

        if ($request->getMethod() === 'POST') {
            $options = $request->request->all();
            $clinicMarks = $this->getClinicService()->changeMark($options['images']);
            $marks = array();
            foreach ($clinicMarks as $key => $clinicMark) {
                $marks[$key] = $this->getWebExtension()->getFpath($clinicMark);
            }

            return $this->createJsonResponse(array(
                'status' => 'success',
                'marks' => $marks, )
            );
        }

        $fileId = $request->getSession()->get('fileId');
        list($pictureUrl, $naturalSize, $scaledSize) = $this->getFileService()->getImgFileMetaInfo($fileId, 270, 270);

        return $this->render('@Custom/clinic/picture-cover-modal.html.twig', array(
            'pictureUrl' => $pictureUrl,
            'naturalSize' => $naturalSize,
            'scaledSize' => $scaledSize,
        ));
    }

    public function listAction(Request $request, $filter = 'clinic')//社区周边
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
        $fields = $request->query->all();
        $conditions = array(
            'keywordType' => '',
            'keyword' => '',
            'communityId' => $community['id'],
        );

        //显示所有诊所，不局限于社区内部
        if (isset($fields['communityLimit']) && $fields['communityLimit'] == '0') {
            unset($conditions['communityId']);
        }

        if (!empty($fields['keyword'])) {
            $fields['name'] = $fields['keyword'];
        }

        $conditions = array_merge($conditions, $fields);

        $clinicCount = $this->getClinicService()->countClinic($conditions);

        $paginator = new Paginator(
            $this->get('request'),
            $clinicCount,
            10
        );

        $clinics = $this->getClinicService()->searchClinics(
            $conditions,
            array(),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $communityIds = ArrayToolkit::column($clinics, 'communityId');

        $communities = $this->getCommunityService()->searchCommunity(
            array(
                'ids' => $communityIds
            ),
            array(),
            0,
            PHP_INT_MAX
        );
        $communities = ArrayToolkit::index($communities, 'id');


        return $this->render('@Custom/community-periphery/index.html.twig', array(
            'clinics' => $clinics,
            'count' => $clinicCount,
            'communities' => $communities,
            'filter' => $filter,
            'paginator' => $paginator,
            'keyword' => !empty($fields['keyword']) ? $fields['keyword']: '',
        ));
    }

    public function showAction(Request $request, $id, $filter = 'base')
    {
        if (empty($id)) {
            throw $this->createNotFoundException();
        }

        $clinic = $this->getClinicService()->getClinic($id);

        $community = $this->getCommunityService()->getCommunity($clinic['communityId']);

        return $this->render('@Custom/clinic/show/show.html.twig', array(
            'clinic' => $clinic,
            'community' => $community,
            'filter' => $filter
        ));
    }

    public function clinicTeamAction($id)
    {
        $clinic = $this->getClinicService()->getClinic($id);

        $doctors = $this->getUserService()->findUsersByIds($clinic['doctorIds']);
        $doctorProfiles = $this->getUserService()->findUserProfilesByIds($clinic['doctorIds']);
        $doctorProfiles = ArrayToolkit::index($doctorProfiles, 'id');

        $nurses = $this->getUserService()->findUsersByIds($clinic['nurseIds']);
        $nurseProfiles = $this->getUserService()->findUserProfilesByIds($clinic['nurseIds']);
        $nurseProfiles = ArrayToolkit::index($nurseProfiles, 'id');

        return $this->render('@Custom/clinic/show-tabs/team.html.twig', array(
            'clinic' => $clinic,
            'doctors' => $doctors,
            'doctorProfiles' => $doctorProfiles,
            'nurses' => $nurses,
            'nurseProfiles' => $nurseProfiles
        ));
    }

    public function bespeakAction(Request $request, $clinicId)
    {
        $user = $this->getCurrentUser();
        $clinic = $this->getClinicService()->getClinic($clinicId);
        $community = $this->getCommunityService()->getCommunity($clinic['communityId']);

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $this->getClinicService()->bespeakClinic($fields, $clinicId);

            return $this->createJsonResponse(array(
                'status' => true
                )
            );
        }

        $records = $this->getClinicReservationRecordService()->searchReservationRecords(
            array(
                'userId' => $user['id'],
                'clinicId' => $clinicId,
            ),
            array('createdTime' => 'DESC'),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/clinic/bespeak/index.html.twig', array(
            'clinic' => $clinic,
            'user' => $user,
            'community' => $community,
            'records' => $records
        ));
    }

    protected function getClinicService()
    {
        return $this->createService('CustomBundle:Clinic:ClinicService');
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getFileService()
    {
        return $this->createService('Content:FileService');
    }

    protected function getClinicReservationRecordService()
    {
        return $this->createService('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }
}