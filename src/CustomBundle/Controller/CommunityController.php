<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class CommunityController extends BaseController
{
    public function matchCommunityAction(Request $request)
    {
        $queryFields = $request->query->get('q');

        $communitys = $this->getCommunityService()->findCommunitiesByLikeName($queryFields);

        $data = array();

        foreach ($communitys as $community) {
            $data[] = array(
                'id' => $community['id'],
                'name' => "{$community['province']}/{$community['city']}/{$community['district']}--" . $community['name'],
                'region' => "{$community['province']}/{$community['city']}/{$community['district']}"
            );
        }

        return $this->createJsonResponse($data);
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}