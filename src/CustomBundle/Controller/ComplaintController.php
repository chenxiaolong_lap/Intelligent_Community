<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;
use AppBundle\Common\ArrayToolkit;

class ComplaintController extends BaseController
{
    public function indexAction(Request $request)
    {
        $community = $this->getCommunity();
        $conditions = array(
            'communityId' => $community['id']
        );
        $fields = $request->query->all();
        $conditions = array_merge($conditions, $fields);

        $count = $this->getComplaintService()->countComplaints($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $complaints = $this->getComplaintService()->searchComplaints(
            $conditions,
            array(
                'createdTime' => 'DESC'
            ),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $userIds = ArrayToolkit::column($complaints, 'userId');
        $users = $this->getUserService()->findUsersByIds(array_unique($userIds));
        $users = ArrayToolkit::index($users, 'id');
        $this->secretUserInfo($users);

        return $this->render('@Custom/complaint-box/index.html.twig', array(
            'paginator' => $paginator,
            'count' => $count,
            'complaints' => $complaints,
            'users' => $users
        ));
    }

    public function showAction($id)
    {
        $complaint = $this->getComplaintService()->getComplaint($id);

        if (empty($complaint)) {
            throw  $this->createNotFoundException("没有查询到#{$id}的投诉信息");
        }

        return $this->render('@Custom/complaint-box/show-modal.html.twig', array(
            'complaint' => $complaint
        ));
    }

    public function createAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $community = $this->getCommunity();

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $fields['communityId'] = $community['id'];
            $fields['userId'] = $user['id'];

            $this->getComplaintService()->createComplaint($fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/complaint-box/create-modal.html.twig');
    }

    protected function secretUserInfo(&$users)
    {
        foreach ($users as &$user) {
            $user['nickname'] = mb_substr($user['nickname'], 0, 2, 'UTF-8') . '***';
        }

        return $users;
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        return $community;
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getComplaintService()
    {
        return $this->createService('CustomBundle:Complaint:ComplaintService');
    }
}