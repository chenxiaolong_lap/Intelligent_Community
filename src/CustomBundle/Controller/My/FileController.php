<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;
use Biz\Activity\Type\Download;
use Biz\File\Service\UploadFileService;
use Codeages\Biz\Framework\Context\Biz;
use CustomBundle\Common\FileToolkit;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;

class FileController extends BaseController
{
    protected $dir;

    public function __construct()
    {
        $this->dir = realpath(__DIR__.'/../../../../app/data/udisk');
    }

    public function indexAction(Request $request)
    {
        $community = $this->getCommunity();
        $conditions = array(
            'targetType' => 'community',
            'targetId' => $community['id']
        );

        $fields = $request->query->all();

        $paginator = new Paginator(
            $request,
            $this->getUploadFileService()->countUseFile($conditions),
            20
        );

        $files = $this->getUploadFileService()->searchFiles(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($files, 'updatedUserId'));

        return $this->render('@Custom/file-block/index.html.twig', array(
            'files' => $files,
            'users' => ArrayToolkit::index($users, 'id'),
            'paginator' => $paginator,
            'now' => time(),
            'id' => $community['id']
        ));
    }

    public function downloadAction($id)
    {
        $file = $this->getUploadFileService()->getFile($id);
        $filePath = $this->dir . '/' . $file['hashId'];

        return FileToolkit::downLoadFile($filePath);
    }

    public function batchDownloadAction(Request $request)
    {
        $fileIds = $request->query->get('fileIds');

        if (empty($fileIds)) {
            return $this->createJsonResponse(array(
                'status' => false,
                'message' => '至少选择一个文件'
            ));
        }

        return $this->createJsonResponse(array(
            'status' => true,
            'goto' => $this->generateUrl('my_community_files_batch_download_submit', array('fileIds' => $fileIds))
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $file = $this->getUploadFileService()->getFile($id);

        if (empty($file)) {
            return $this->createMessageResponse('warning', '该文件不存在');
        }

        $this->getUploadFileService()->deleteFile($id);

        return $this->createJsonResponse(array('status' => true, 'message' => '删除成功'));
    }

    public function batchDownloadSubmitAction(Request $request)
    {
        $fileIds = $request->query->get('fileIds');

        if (empty($fileIds)) {
            return $this->createJsonResponse(array(
                'status' => false,
                'message' => '至少选择一个文件'
            ));
        }

        $files = $this->getUploadFileService()->findFilesByIds($fileIds);

        if (count($files) == 1) {
            $filePath = $this->dir . '/' . $files[0]['hashId'];

            return FileToolkit::downLoadFile($filePath);
        }

        $filePathArray = array();
        foreach ($files as $file) {
            $filePath = $this->dir . '/' . $file['hashId'];
            array_push($filePathArray, $filePath);
        }

        return FileToolkit::batchDownloadFiles($filePathArray, '');
    }

    public function batchDeleteAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $fileIds = $request->request->get('fileIds');

            if (!is_array($fileIds)) {
                $this->getUploadFileService()->deleteFile($fileIds);
            } else {
                $this->getUploadFileService()->deleteFiles($fileIds);
            }

            return $this->createJsonResponse(array('status' => true, 'message' => '删除成功'));
        }
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getDownloadService(Biz $biz)
    {
        return new Download($biz);
    }

    /**
     * @return UploadFileService
     */
    protected function getUploadFileService()
    {
        return $this->getBiz()->service('File:UploadFileService');
    }

    public function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}