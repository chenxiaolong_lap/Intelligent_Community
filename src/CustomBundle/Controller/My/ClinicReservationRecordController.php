<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class ClinicReservationRecordController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $fields = $request->query->all();
        $community = $this->getCommunity();

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        $clinics = $this->getClinicService()->findClinicsByCommunityId($community['id']);

        $conditions = array(
            'keyword' => '',
            'keywordType' => '',
            'clinicIds' => !empty($clinics) ? ArrayToolkit::column($clinics, 'id') : array(-1),
        );

        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'clinicName') {
                $clinics = $this->getClinicService()->searchClinics(
                    array('name' => $conditions['keyword']),
                    array(),
                    0,
                    PHP_INT_MAX
                );

                $conditions['clinicIds'] = !empty($clinics) ? array_intersect($conditions['clinicIds'], ArrayToolkit::column($clinics, 'id')) : array(-1);
            }

            if ($conditions['keywordType'] == 'nickname') {
                $users = $this->getUserService()->findUsersByLikeNickname($conditions['keyword']);

                $conditions['userIds'] = !empty($users) ? ArrayToolkit::column($users, 'id') : array(-1);
            }
        }

        $count = $this->getClinicReservationRecordService()->countReservationRecord($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $records = $this->getClinicReservationRecordService()->searchReservationRecords(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $userIds = ArrayToolkit::column($records, 'userId');
        $users = $this->getUserService()->searchUsers(
            array('ids' => $userIds),
            array(),
            0,
            PHP_INT_MAX
        );

        $communityIds = ArrayToolkit::column($clinics, 'communityId');
        $communities = $this->getCommunityService()->searchCommunity(
            array('ids' => $communityIds),
            array(),
            0,
            PHP_INT_MAX
        );

        $users = ArrayToolkit::index($users, 'id');
        $clinics = ArrayToolkit::index($clinics, 'id');
        $communities = ArrayToolkit::index($communities, 'id');

        return $this->render('@Custom/my/clinic/record/index.html.twig', array(
            'paginator' => $paginator,
            'recordCount' => $count,
            'records' => $records,
            'clinics' => $clinics,
            'communities' => $communities,
            'users' => $users,
        ));
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getClinicService()
    {
        return $this->createService('CustomBundle:Clinic:ClinicService');
    }

    protected function getClinicReservationRecordService()
    {
        return $this->createService('CustomBundle:ClinicReservationRecord:ClinicReservationRecordService');
    }
}