<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use CustomBundle\Controller\Admin\PropertyController as BasePropertyController;
use AppBundle\Common\Paginator;

class PropertyController extends BasePropertyController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $fields = $request->query->all();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->needCommunity($userProfile);
        $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
            array('communityId' => $community['id']),
            array(),
            0,
            PHP_INT_MAX
        );
        $housingEstateIds = ArrayToolkit::column($housingEstates, 'id');

        $conditions = array(
            'keywordType' => '',
            'keyword' => '',
            'communityId' => $community['id'],
            'housingEstateIds' => $housingEstateIds,
        );
        $conditions = array_merge($conditions, $fields);

        $conditions = $this->queryByHousingEstate($conditions);
        $conditions = $this->queryByUser($conditions);

        if ((isset($conditions['keywordType']) && in_array($conditions['keywordType'],array('propertyName', 'servicePhone')) && !empty($conditions['keyword']))) {
            if ($conditions['keywordType'] == 'propertyName') {
                $conditions['name'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'servicePhone') {
                $conditions['servicePhone'] = $conditions['keyword'];
            }
        }

        if (!empty($conditions['housingEstateIds'])) {
            $propertyCount = $this->getPropertyService()->CountProperty($conditions);
            $paginator = new Paginator(
                $this->get('request'),
                $propertyCount,
                20
            );
            $properties = $this->getPropertyService()->searchProperty(
                $conditions,
                array('createdTime' => 'DESC'),
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
        } else {
            $propertyCount = 0;
            $properties = array();
            $paginator = new Paginator(
                $this->get('request'),
                $propertyCount,
                1
            );
        }

        $housingEstates = ArrayToolkit::index($housingEstates, 'id');

        return $this->render('@Custom/my/property/index.html.twig', array(
            'community' => $community,
            'propertyCount' => $propertyCount,
            'properties' => $properties,
            'paginator' => $paginator,
            'housingEstates' => $housingEstates
        ));
    }

    public function showAction($id)
    {
        $property = $this->getPropertyService()->getProperty($id);

        $directors = $this->getUserService()->findUsersByIds($property['directorIds']);
        $housingEstate = $this->getHousingEstateService()->getHousingEstate($property['housingEstateId']);
        $community = $this->getCommunityService()->getCommunity($housingEstate['communityId']);

        $userCount = $this->getUserService()->searchUserProfiles(
            array(
                'province' => $community['province'],
                'city' => $community['city'],
                'district' => $community['district'],
                'community' => $community['name'],
                'housingEstate' => $housingEstate['name']
            ),
            array(),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/admin/property/show-modal.html.twig', array(
            'directors' => $directors,
            'property' => $property,
            'userCount' => $userCount,
            'housingEstate' => $housingEstate,
            'community' => $community,
        ));
    }


    public function queryByHousingEstate($conditions)
    {
        if ((isset($conditions['keywordType']) && $conditions['keywordType'] == 'housingEstateName' && !empty($conditions['keyword']))) {
            $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
                array('name' => $conditions['keyword']),
                array(),
                0,
                PHP_INT_MAX
            );
            $housingEstateIds = ArrayToolkit::column($housingEstates, 'id');

            if (empty($housingEstateIds)) {
                $conditions['housingEstateIds'] = array();
            }
        }

        return $conditions;
    }

    public function queryByUser($conditions)
    {
        if ((isset($conditions['keywordType']) && $conditions['keywordType'] == 'director' && !empty($conditions['keyword']))) {
            $users = $this->getUserService()->searchUsers(
                array('nickname' => $conditions['keyword']),
                array(),
                0,
                PHP_INT_MAX
            );

            $userIds = ArrayToolkit::column($users, 'id');

            $conditions['directorIds'] = $userIds;
        }

        return $conditions;
    }

    protected function needCommunity($user)
    {
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $user['province'],
            $user['city'],
            $user['district'],
            $user['community']
        );

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        return $community;
    }

    public function queryCountUserAction($province, $city, $district, $community, $housingEstate)
    {
        $fields = array(
            'province' => $province,
            'city' => $city,
            'district' => $district,
            'community' => $community,
            'housingEstate' => $housingEstate
        );

        $count = $this->getUserService()->searchUserProfileCount(
            $fields,
            array(),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/my/housing-estate/query-user-count.html.twig', array('count' => $count));
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getHousingEstateService()
    {
        return $this->createService('CustomBundle:HousingEstate:HousingEstateService');
    }

    public function getPropertyService()
    {
        return $this->createService('CustomBundle:Property:PropertyService');
    }
}