<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class CommunityController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        if (empty($community)) {
            return $this->createMessageResponse('info', '对不起，你的信息不足，暂未查询到你所属的社区，请完成补充你的信息。', '', 5, $this->generateUrl('settings'));
        }

        return $this->render('@Custom/my/index.html.twig', array(
            'user' => $userProfile,
            'community' => $community
        ));
    }

    public function communityNameAction()
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        return $this->render('@Custom/my/nav-header-community-name.html.twig', array(
            'user' => $userProfile
        ));
    }

    public function settingAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $community = $this->getCommunityService()->updateCommunity($community['id'], $fields);

            $this->setFlashMessage('success', 'site.save.success');

            return $this->redirect($this->generateUrl('my_community_setting'));
        }

        return $this->render('@Custom/my/setting/index.html.twig', array(
            'community' => $community
        ));
    }

    public function mapCommunityAction()
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        return $this->createJsonResponse($community);
    }

    public function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    public function getArticleService()
    {
        return $this->createService('CustomBundle:Article:ArticleService');
    }

    public function getCommunityAnnouncementService()
    {
        return $this->createService('CustomBundle:CommunityAnnouncement:CommunityAnnouncementService');
    }
}