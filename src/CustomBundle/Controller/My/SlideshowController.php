<?php

namespace CustomBundle\Controller\My;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;

class SlideshowController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        $slideshowCount = $this->getSlideshowService()->countSlideshow(array('communityId' => $community['id']));
        $paginator = new Paginator(
            $this->get('request'),
            $slideshowCount,
            1
        );

        $slideshow = $this->getSlideshowService()->searchSlideshow(
            array('communityId' => $community['id']),
            array('serial_number' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('@Custom/my/slideshow/index.html.twig', array(
            'slideshow' => !empty($slideshow) ? $slideshow[0] : null,
            'community' => $community,
            'slideshowCount' => $slideshowCount,
            'paginator' => $paginator,
        ));
    }

    public function editAction(Request $request, $id)
    {

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $page = $request->request->get('page');
            $slideshow = $this->getSlideshowService()->updateSlideshow($id, $fields);
            $this->setFlashMessage('success', 'site.save.success');

            return $this->redirect($this->generateUrl('my_community_slideshow_manage', array('page' => $page)));
        }
    }

    public function createAction()
    {
        $user = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        $fields = array(
            'communityId' => $community['id'],
            'userId' => $user['id'],
        );

        try {
            $slideshow = $this->getSlideshowService()->createSlideshow($fields);

            if (!empty($slideshow)) {
                return $this->createJsonResponse(array('status' => true, 'slideshow' => $slideshow));
            } else {
                return $this->createJsonResponse(array('status' => false, 'message' => '创建失败'));
            }

        } catch (\Exception $e) {
            return $this->createJsonResponse(array(
                'status' => false,
                'message' => $e->getMessage(),
            ));
        }
    }

    public function uploadAction(Request $request, $id)
    {
        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();
            $slideshow = $this->getSlideshowService()->changeSlideshowPicture($id, $data['images'][0]);
            $slideshow['filePath'] = $this->get('web.twig.extension')->getFilePath($slideshow['picturePath']);
            $slideshow['fileUrl'] = $this->get('web.twig.extension')->getFurl($slideshow['picturePath']);

            return $this->createJsonResponse(array('status' => true, 'slideshow' => $slideshow));
        }

        $slideshow = $this->getSlideshowService()->getSlideshow($id);
        $fileId = $request->getSession()->get('fileId');

        list($pictureUrl, $naturalSize, $scaledSize) = $this->getFileService()->getImgFileMetaInfo($fileId, 480, 370);

        return $this->render(
            '@Custom/my/slideshow/picture-cover-modal.html.twig',
            array(
                'slideshow' => $slideshow,
                'pictureUrl' => $pictureUrl,
                'naturalSize' => $naturalSize,
                'scaledSize' => $scaledSize,
            )
        );
    }

    public function slideshowAction()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        $slideshows = $this->getSlideshowService()->searchSlideshow(
            array('communityId' => $community['id']),
            array('serial_number' => 'ASC'),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/my/carousel.html.twig', array('slideshows' => $slideshows));
    }

    protected function getSlideshowService()
    {
        return $this->createService('CustomBundle:Slideshow:SlideshowService');
    }

    public function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getFileService()
    {
        return $this->createService('Content:FileService');
    }
}