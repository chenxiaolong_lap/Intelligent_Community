<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;

class AnnouncementController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $fields = $request->query->all();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        $conditions = array(
            'keywordType' => '',
            'keyword' => '',
            'keywordUserType' => '',
            'communityId' => $community['id'],
        );
        $conditions = array_merge($conditions, $fields);

        if ($conditions['keywordType'] == 'title' && !empty($conditions['keyword'])) {
            $conditions['title'] = $conditions['keyword'];
        }

        if (!empty($conditions['status'])) {
            if ($conditions['status'] == '未显示') {
                $conditions['startTime_GT'] = time();
            } elseif ($conditions['status'] == '显示中') {
                $conditions['endTime_GE'] = time();
                $conditions['startTime_LE'] = time();
            } elseif ($conditions['status'] == '已结束') {
                $conditions['endTime_LT'] = time();
            }
        }

        if (!empty($conditions['type'])) {
            $conditions['targetType'] = $conditions['type'];
        }

        $announcementCount = $this->getCommunityAnnouncementService()->countCommunityAnnouncement($conditions);

        $paginator = new Paginator(
            $this->get('request'),
            $announcementCount,
            20
        );

        $announcements = $this->getCommunityAnnouncementService()->searchCommunityAnnouncement(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('@Custom/my/announcement/index.html.twig', array(
            'community' => $community,
            'user' => $userProfile,
            'announcements' => $announcements,
            'announcementCount' => $announcementCount,
            'paginator' => $paginator
        ));
    }

    public function createAction(Request $request, $communityId)
    {
        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $fields['communityId'] = $communityId;
            $this->getCommunityAnnouncementService()->createCommunityAnnouncement($fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/my/announcement/create-modal.html.twig', array(
            'communityId' => $communityId
        ));
    }

    public function editAction(Request $request, $id)
    {
        $announcement = $this->getCommunityAnnouncementService()->getCommunityAnnouncement($id);

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $this->getCommunityAnnouncementService()->updateCommunityAnnouncement($id, $fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->render('@Custom/my/announcement/create-modal.html.twig', array(
            'announcement' => $announcement
        ));
    }

    public function previewAction($id)
    {
        $announcement = $this->getCommunityAnnouncementService()->getCommunityAnnouncement($id);

        return $this->render('@Custom/my/announcement/preview.html.twig', array(
            'announcement' => $announcement
        ));
    }

    public function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getCommunityAnnouncementService()
    {
        return $this->createService('CustomBundle:CommunityAnnouncement:CommunityAnnouncementService');
    }
}