<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class AdviceController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        $conditions = array(
            'keywordType' => '',
            'keyword' => ''
        );
        $fields = $request->query->all();
        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keywordType']) && !empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'nickname') {
                $users = $this->getUserService()->findUsersByLikeNickname($conditions['keyword']);
                $conditions['userIds'] = !empty($users) ? ArrayToolkit::column($users, 'id') : array(-1);
            }
        }

        $count = $this->getAdviceService()->countAdvices($conditions);

        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $advices = $this->getAdviceService()->searchAdvices(
            $conditions,
            array(
                'createdTime' => 'DESC'
            ),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($advices, 'userId'));
        $users = ArrayToolkit::index($users, 'id');

        return $this->render('@Custom/my/advice-box/index.html.twig', array(
            'advices' => $advices,
            'count' => $count,
            'paginator' => $paginator,
            'users' => $users
        ));
    }

    public function showAction($id)
    {
        $advice = $this->getAdviceService()->getAdvice($id);

        $this->getAdviceService()->updateAdvice($id, array('status' => 'saw'));

        return $this->render('@Custom/my/advice-box/show-modal.html.twig', array(
            'advice' => $advice
        ));
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getAdviceService()
    {
        return $this->createService('CustomBundle:Advice:AdviceService');
    }
}