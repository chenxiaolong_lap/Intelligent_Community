<?php

namespace CustomBundle\Controller\My;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;
use AppBundle\Common\ArrayToolkit;

class UserController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $fields = $request->query->all();

        $conditions = array(
            'keywordType' => '',
            'keyword' => '',
            'keywordUserType' => '',
            'province' => $userProfile['province'],
            'city' => $userProfile['city'],
            'district' => $userProfile['district'],
            'community' => $userProfile['community'],
        );

        $conditions = array_merge($conditions, $fields);
        $conditions = $this->fillOrgCode($conditions);

        //根据mobile,社区,查询user_profile获得userIds

        if ((isset($conditions['keywordType']) && !empty($conditions['keyword'])) || (!empty($conditions['province']) && !empty($conditions['community']))) {
            if (!empty($conditions['keyword']) && $conditions['keywordType'] == 'verifiedMobile') {
                $conditions['mobile'] = $conditions['keyword'];
            }

            if (!empty($conditions['keyword']) && $conditions['keywordType'] == 'nickname') {
                $conditions['nickname'] = $conditions['keyword'];
            }

            if (!empty($conditions['keyword']) && $conditions['keywordType'] == 'email') {
                $conditions['email'] = $conditions['keyword'];
            }

            if (!empty($conditions['keyword']) && $conditions['keywordType'] == 'truename') {
                $conditions['truename'] = $conditions['keyword'];
            }

            if (!empty($conditions['keyword']) && $conditions['keywordType'] == 'housingEstate') {
                $conditions['housingEstateLike'] = $conditions['keyword'];
            }

            $profilesCount = $this->getUserService()->searchUserProfileCount($conditions);
            $userProfiles = $this->getUserService()->searchUserProfiles(
                $conditions,
                array('id' => 'DESC'),
                0,
                $profilesCount
            );
            unset($conditions['mobile']);
            $userIds = ArrayToolkit::column($userProfiles, 'id');

            if (!empty($userIds)) {
                unset($conditions['keywordType']);
                unset($conditions['keyword']);
                $conditions['userIds'] = $userIds;
                $userCount = $this->getUserService()->countUsers($conditions);
                $paginator = new Paginator(
                    $this->get('request'),
                    $userCount,
                    20
                );

                $users = $this->getUserService()->searchUsers(
                    $conditions,
                    array('createdTime' => 'DESC'),
                    $paginator->getOffsetCount(),
                    $paginator->getPerPageCount()
                );
            } else {
                $userCount = 0;
                $paginator = new Paginator(
                    $this->get('request'),
                    $userCount,
                    1
                );

                $users = array();
            }
        }

        $app = $this->getAppService()->findInstallApp('UserImporter');

        $showUserExport = false;

        if (!empty($app) && array_key_exists('version', $app)) {
            $showUserExport = version_compare($app['version'], '1.0.2', '>=');
        }

        $userIds = ArrayToolkit::column($users, 'id');
        $profiles = $this->getUserService()->findUserProfilesByIds($userIds);

        return $this->render('@Custom/my/user/user-index.html.twig', array(
                'users' => $users,
                'userCount' => $userCount,
                'paginator' => $paginator,
                'profiles' => $profiles,
                'showUserExport' => $showUserExport,
            )
        );
    }

    public function showAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);
        $profile = $this->getUserService()->getUserProfile($id);
        $profile['title'] = $user['title'];
        $fields = $this->getFields();

        return $this->render('@Custom/my/user/show-modal.html.twig', array(
            'user'         => $user,
            'profile'      => $profile,
            'fields'       => $fields,
        ));
    }

    public function editAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);

        $profile = $this->getUserService()->getUserProfile($user['id']);
        $profile['title'] = $user['title'];

        if ($request->getMethod() === 'POST') {
            $profile = $request->request->all();

            if (!((strlen($user['verifiedMobile']) > 0) && isset($profile['mobile']))) {
                $profile = $this->getUserService()->updateUserProfile($user['id'], $profile);
                $this->getLogService()->info('user', 'edit', "管理员编辑用户资料 {$user['nickname']} (#{$user['id']})", $profile);
            } else {
                return $this->createJsonResponse(array(
                    'success' => false,
                    'message' => 'user.settings.profile.unable_change_bind_mobile',
                ));
            }

            return $this->createJsonResponse(array(
                'success' => true,
            ));
        }
        $fields      = $this->getFields();

        return $this->render('admin/user/edit-modal.html.twig', array(
            'user'        => $user,
            'profile'     => $profile,
            'fields'      => $fields,
        ));
    }

    public function setCommunityRoleAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);

        if ($request->getMethod() == 'POST') {
            $communityRole = $request->request->get('communityRole');

            $updateProfile = $this->getUserService()->setCommunityRole($id, $communityRole);

            return $this->createJsonResponse(array('status' => true, 'message' => '设置用户社区角色成功'));
        }

        return $this->render('@Custom/admin/user/community-role-modal.html.twig', array(
            'user' => $user
        ));
    }

    protected function getFields()
    {
        $fields = $this->getUserFieldService()->getEnabledFieldsOrderBySeq();

        for ($i = 0; $i < count($fields); ++$i) {
            if (strstr($fields[$i]['fieldName'], 'textField')) {
                $fields[$i]['type'] = 'text';
            }

            if (strstr($fields[$i]['fieldName'], 'varcharField')) {
                $fields[$i]['type'] = 'varchar';
            }

            if (strstr($fields[$i]['fieldName'], 'intField')) {
                $fields[$i]['type'] = 'int';
            }

            if (strstr($fields[$i]['fieldName'], 'floatField')) {
                $fields[$i]['type'] = 'float';
            }

            if (strstr($fields[$i]['fieldName'], 'dateField')) {
                $fields[$i]['type'] = 'date';
            }
        }

        return $fields;
    }

    protected function getAppService()
    {
        return $this->createService('CloudPlatform:AppService');
    }

    protected function getUserFieldService()
    {
        return $this->createService('User:UserFieldService');
    }
}