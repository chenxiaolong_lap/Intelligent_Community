<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class ComplaintController extends BaseController
{
    public function indexAction(Request $request)
    {
        $conditions = array(
            'keywordType' => '',
            'keyword' => ''
        );
        $fields = $request->query->all();
        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keywordType']) && !empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'nickname') {
                $users = $this->getUserService()->findUsersByLikeNickname($conditions['keyword']);
                $conditions['userIds'] = !empty($users) ? ArrayToolkit::column($users, 'id') : array(-1);
            }
        }

        $count = $this->getComplaintService()->countComplaints($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $complaints = $this->getComplaintService()->searchComplaints(
            $conditions,
            array(
                'createdTime' => 'DESC'
            ),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $userIds = ArrayToolkit::column($complaints, 'userId');
        $users = $this->getUserService()->findUsersByIds(array_unique($userIds));
        $users = ArrayToolkit::index($users, 'id');

        return $this->render('@Custom/my/complaint-box/index.html.twig', array(
            'paginator' => $paginator,
            'count' => $count,
            'complaints' => $complaints,
            'users' => $users
        ));
    }

    public function showAction($id)
    {
        $complaint = $this->getComplaintService()->getComplaint($id);

        if (empty($complaint)) {
            throw  $this->createNotFoundException("没有查询到#{$id}的投诉信息");
        }

        if ($complaint['status'] == 'nosee') {
            $this->getComplaintService()->updateComplaint($id, array('status' => 'pending'));
        }

        return $this->render('@Custom/my/complaint-box/show-modal.html.twig', array(
            'complaint' => $complaint
        ));
    }

    public function handleAction(Request $request, $id)
    {
        $complaint = $this->getComplaintService()->getComplaint($id);
        $user = $this->getCurrentUser();

        if (empty($complaint)) {
            throw $this->createNotFoundException("对不起，没有#{$id}的投诉信");
        }

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $fields['principalId'] = $user['id'];
            $fields['processTime'] = time();
            $fields['status'] = 'processed';
            $this->getComplaintService()->updateComplaint($id, $fields);

            return $this->createJsonResponse(array('status' => true));
        }

        return $this->createMessageResponse('error', '未知错误');
    }

    protected function getComplaintService()
    {
        return $this->createService('CustomBundle:Complaint:ComplaintService');
    }
}