<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class CommunityActivityController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $fields = $request->query->all();
        $community = $this->getCommunity();

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        $conditions = array(
            'keywordType' => '',
            'keyword' => '',
            'keywordUserType' => '',
            'communityId' => $community['id'],
        );
        $conditions = array_merge($conditions, $fields);

        if ($conditions['keywordType'] == 'title' && !empty($conditions['keyword'])) {
            $conditions['title'] = $conditions['keyword'];
        }

        if (!empty($conditions['status'])) {
            if ($conditions['status'] == '未开始') {
                $conditions['startTime_GT'] = time();
            } elseif ($conditions['status'] == '进行中') {
                $conditions['endTime_GE'] = time();
                $conditions['startTime_LE'] = time();
            } elseif ($conditions['status'] == '已结束') {
                $conditions['endTime_LT'] = time();
            }
        }

        if (!empty($conditions['targetType'])) {
            $conditions['type'] = $conditions['targetType'];
        }

        $activityCount = $this->getCommunityActivityService()->countCommunityActivity($conditions);

        $paginator = new Paginator(
            $this->get('request'),
            $activityCount,
            20
        );

        $activities = $this->getCommunityActivityService()->searchCommunityActivity(
            $conditions,
            array('updatedTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('@Custom/my/activity/index.html.twig', array(
            'paginator' => $paginator,
            'activityCount' => $activityCount,
            'activities' => $activities,
            'community' => $community,
        ));
    }

    public function createAction(Request $request)
    {
        $community = $this->getCommunity();

        if ($request->getMethod() == 'POST') {
            $formData = $request->request->all();
            $formData['communityId'] = $community['id'];

            $activity = $this->getCommunityActivityService()->createCommunityActivity($formData);
            $this->setFlashMessage('success', 'site.save.success');

            return $this->redirect($this->generateUrl('my_community_activity_edit', array('id' => $activity['id'])));
        }

        return $this->render('@Custom/my/activity/activity-modal.html.twig', array(

        ));
    }

    public function editAction(Request $request, $id)
    {
        $community = $this->getCommunity();

        if ($request->getMethod() == 'POST') {
            $formData = $request->request->all();

            $this->getCommunityActivityService()->updateCommunityActivity($id, $formData);

            $this->setFlashMessage('success', 'site.save.success');

            return $this->redirect($this->generateUrl('my_community_activity_edit',  array('id' => $id)));
        }

        $activity = $this->getCommunityActivityService()->getCommunityActivity($id);

        $userIds = ArrayToolkit::column($activity, 'invited');
        $users = $this->getUserService()->findUsersByIds($userIds);

        return $this->render('@Custom/my/activity/activity-modal.html.twig', array(
            'activity' => $activity,
            'users' => $users,
        ));
    }

    public function preview($id)
    {
        $activity = $this->getCommunityActivityService()->getActivity($id);

        return $this->render('@Custom/my/activity/preview.html.twig', array(
            'activity' => $activity
        ));
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getCommunityActivityService()
    {
        return $this->createService('CustomBundle:CommunityActivity:CommunityActivityService');
    }
}