<?php

namespace CustomBundle\Controller\My;

use AppBundle\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Controller\Admin\ArticleController as BaseArticleController;

class ArticleController extends BaseArticleController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $community = $this->getCommunity();

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        $conditions = $request->query->all();
        $conditions['communityId'] = $community['id'];
        $categoryId = 0;

        if (!empty($conditions['categoryId'])) {
            $conditions['includeChildren'] = true;
            $categoryId = $conditions['categoryId'];
        }

        $conditions = $this->fillOrgCode($conditions);

        $paginator = new Paginator(
            $request,
            $this->getArticleService()->countArticles($conditions),
            20
        );

        $articles = $this->getArticleService()->searchArticles(
            $conditions,
            'normal',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );
        $categoryIds = ArrayToolkit::column($articles, 'categoryId');
        $categories = $this->getCategoryService()->findCategoriesByIds($categoryIds);
        $categoryTree = $this->getCategoryService()->getCategoryTree();

        return $this->render('@Custom/my/article/index.html.twig', array(
            'articles' => $articles,
            'categories' => $categories,
            'paginator' => $paginator,
            'categoryTree' => $categoryTree,
            'categoryId' => $categoryId,
        ));
    }

    public function createAction(Request $request)
    {
        $community = $this->getCommunity();

        if ($request->getMethod() == 'POST') {
            $formData = $request->request->all();
            $formData['communityId'] = $community['id'];

            $article['tags'] = array_filter(explode(',', $formData['tags']));

            $article = $this->getArticleService()->createArticle($formData);

            $attachment = $request->request->get('attachment');
            $this->getUploadFileService()->createUseFiles($attachment['fileIds'], $article['id'], $attachment['targetType'], $attachment['type']);

            $this->setFlashMessage('success', 'site.save.success');
            return $this->redirect($this->generateUrl('my_community_article_edit', array('id' => $article['id'])));

        }

        $categoryTree = $this->getCategoryService()->getCategoryTree();

        return $this->render('@Custom/my/article/article-modal.html.twig', array(
            'categoryTree' => $categoryTree,
            'category' => array('id' => 0, 'parentId' => 0),
        ));
    }

    public function editAction(Request $request, $id)
    {
        $article = $this->getArticleService()->getArticle($id);

        if (empty($article)) {
            throw $this->createNotFoundException('文章已删除或者未发布！');
        }

        $tags = $this->getTagService()->findTagsByOwner(array(
            'ownerType' => 'article',
            'ownerId' => $id,
        ));

        $tagNames = ArrayToolkit::column($tags, 'name');

        $categoryId = $article['categoryId'];
        $category = $this->getCategoryService()->getCategory($categoryId);

        $categoryTree = $this->getCategoryService()->getCategoryTree();

        if ($request->getMethod() == 'POST') {
            $formData = $request->request->all();
            $article = $this->getArticleService()->updateArticle($id, $formData);

            $attachment = $request->request->get('attachment');

            $this->getUploadFileService()->createUseFiles($attachment['fileIds'], $article['id'], $attachment['targetType'], $attachment['type']);

            $this->setFlashMessage('success', 'site.save.success');
            return $this->redirect($this->generateUrl('my_community_article_edit', array('id' => $article['id'])));
        }

        return $this->render('@Custom/my/article/article-modal.html.twig', array(
            'article' => $article,
            'categoryTree' => $categoryTree,
            'category' => $category,
            'tagNames' => $tagNames,
        ));
    }



    public function setArticlePropertyAction(Request $request, $id, $property)
    {
        $this->getArticleService()->setArticleProperty($id, $property);

        return $this->createJsonResponse(true);
    }

    public function cancelArticlePropertyAction(Request $request, $id, $property)
    {
        $this->getArticleService()->cancelArticleProperty($id, $property);

        return $this->createJsonResponse(true);
    }

    public function trashAction(Request $request, $id)
    {
        $this->getArticleService()->trashArticle($id);

        return $this->createJsonResponse(true);
    }

    public function thumbRemoveAction(Request $request, $id)
    {
        $this->getArticleService()->removeArticlethumb($id);

        return $this->createJsonResponse(true);
    }

    public function deleteAction(Request $request)
    {
        $ids = $request->request->get('ids', array());
        $id = $request->query->get('id', null);

        if (!empty($id)) {
            array_push($ids, $id);
        }

        $result = $this->getArticleService()->deleteArticlesByIds($ids);

        if ($result) {
            return $this->createJsonResponse(array('status' => false));
        } else {
            return $this->createJsonResponse(array('status' => true));
        }
    }

    public function publishAction(Request $request, $id)
    {
        $this->getArticleService()->publishArticle($id);

        return $this->createJsonResponse(true);
    }

    public function unpublishAction(Request $request, $id)
    {
        $this->getArticleService()->unpublishArticle($id);

        return $this->createJsonResponse(true);
    }

    public function showUploadAction(Request $request)
    {
        return $this->render('@Custom/my/article/article-picture-modal.html.twig', array(
            'pictureUrl' => '',
        ));
    }

    public function pictureCropAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $options = $request->request->all();
            $files = $this->getArticleService()->changeIndexPicture($options['images']);

            foreach ($files as $key => $file) {
                $files[$key]['file']['url'] = $this->get('web.twig.extension')->getFilePath($file['file']['uri']);
            }

            return new JsonResponse($files);
        }

        $fileId = $request->getSession()->get('fileId');
        list($pictureUrl, $naturalSize, $scaledSize) = $this->getFileService()->getImgFileMetaInfo($fileId, 270, 270);

        return $this->render('@Custom/my/article/article-picture-crop-modal.html.twig', array(
            'pictureUrl' => $pictureUrl,
            'naturalSize' => $naturalSize,
            'scaledSize' => $scaledSize,
        ));
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }
}