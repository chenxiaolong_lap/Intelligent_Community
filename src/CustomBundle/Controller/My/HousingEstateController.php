<?php

namespace CustomBundle\Controller\My;

use CustomBundle\Controller\Admin\HousingEstateController as BaseHousingEstateController;
use AppBundle\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;

class HousingEstateController extends BaseHousingEstateController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $fields = $request->query->all();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $community = $this->needCommunity($userProfile);

        $conditions = array(
            'keywordType' => '',
            'keyword' => '',
            'communityId' => $community['id']
        );
        $conditions = array_merge($conditions, $fields);

        if (isset($conditions['keywordType']) && !empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'housingEstateName') {
                $conditions['name'] = $conditions['keyword'];
            }
        }

        $housingEstateCount = $this->getHousingEstateService()->CountHousingEstate($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $housingEstateCount,
            20
        );
        $housingEstates = $this->getHousingEstateService()->searchHousingEstate(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('@Custom/my/housing-estate/index.html.twig', array(
            'community' => $community,
            'housingEstateCount' => $housingEstateCount,
            'housingEstates' => $housingEstates,
            'paginator' => $paginator
        ));
    }

    protected function needCommunity($user)
    {
        $community = $this->getCommunityService()->getCommunityByRegionAndName(
            $user['province'],
            $user['city'],
            $user['district'],
            $user['community']
        );

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        return $community;
    }

    public function queryCountUserAction($province, $city, $district, $community, $housingEstate)
    {
        $fields = array(
            'province' => $province,
            'city' => $city,
            'district' => $district,
            'community' => $community,
            'housingEstate' => $housingEstate
        );

        $count = $this->getUserService()->searchUserProfileCount(
            $fields,
            array(),
            0,
            PHP_INT_MAX
        );

        return $this->render('@Custom/my/housing-estate/query-user-count.html.twig', array('count' => $count));
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getHousingEstateService()
    {
        return $this->createService('CustomBundle:HousingEstate:HousingEstateService');
    }
}