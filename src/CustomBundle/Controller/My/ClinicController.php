<?php

namespace CustomBundle\Controller\My;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;

class ClinicController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $fields = $request->query->all();
        $community = $this->getCommunity();

        if (empty($community)) {
            throw $this->createAccessDeniedException('对不起，你的信息不足，暂未查询到你所属的社区');
        }

        $conditions = array(
            'keyword' => '',
            'keywordType' => '',
            'communityId' => $community['id']
        );

        $conditions = array_merge($conditions, $fields);

        if (!empty($conditions['keyword'])) {
            if ($conditions['keywordType'] == 'name') {
                $conditions['name'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'phone') {
                $conditions['phone'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'email') {
                $conditions['email'] = $conditions['keyword'];
            }

            if ($conditions['keywordType'] == 'doctorName') {
                $users = $this->getUserService()->findUsersByLikeNickname($conditions['keyword']);
                $conditions['doctorIds'] = !empty($users) ? ArrayToolkit::column($users, 'id') : array(-1);
            }
        }

        $count = $this->getClinicService()->countClinic($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $clinics = $this->getClinicService()->searchClinics(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $communityIds = ArrayToolkit::column($clinics, 'communityId');

        $communities = $this->getCommunityService()->searchCommunity(
            array('ids' => $communityIds),
            array(),
            0,
            PHP_INT_MAX
        );
        $communities = ArrayToolkit::index($communities, 'id');

        return $this->render('@Custom/my/clinic/index.html.twig', array(
            'paginator' => $paginator,
            'clinicCount' => $count,
            'clinics' => $clinics,
            'communities' => $communities,
        ));
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getClinicService()
    {
        return $this->createService('CustomBundle:Clinic:ClinicService');
    }
}