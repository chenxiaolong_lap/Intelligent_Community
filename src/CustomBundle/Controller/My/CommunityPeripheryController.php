<?php

namespace CustomBundle\Controller\My;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class CommunityPeripheryController extends BaseController
{
    public function indexAction(Request $request, $filter = 'clinic')
    {
        if (empty($filter)) {
            throw $this->createAccessDeniedException('对不起参数错误');
        }

        return $this->forward('CustomBundle:'.ucfirst($filter).':list', array('request' => $request, 'filter' => $filter));
    }
}