<?php

namespace CustomBundle\Controller;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Common\Paginator;

class ScoreController extends BaseController
{
    public function createAction(Request $request, $targetType, $targetId, $sourceId)
    {
        $user = $this->getCurrentUser();
        $conditions = array(
            'targetType' => $targetType,
            'targetId' => $targetId,
            'sourceId' => $sourceId
        );

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $fields = array_merge($fields, $conditions);

            $this->getScoreService()->createScore($fields);

            return $this->createJsonResponse(array(
                'status' => true
            ));
        }

        return $this->render('@Custom/score/create-modal.html.twig', array(
            'targetType' =>$targetType,
            'targetId' => $targetId,
            'sourceId' => $sourceId
        ));
    }

    public function listOfClinicAction(Request $request, $clinicId)
    {
        $conditions = $request->query->all();
        $conditions['targetId'] = $clinicId;

        if (!empty($conditions['nickname'])) {
            $user = $this->getUserService()->getUserByNickname($conditions['nickname']);
            $conditions['userId'] = !empty($user) ? $user['id'] : 0;
        }

        $count = $this->getScoreService()->countScores($conditions);
        $paginator = new Paginator(
            $this->get('request'),
            $count,
            20
        );

        $scores = $this->getScoreService()->searchScores(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('@Custom/clinic/show-tabs/score.html.twig', array(
            'paginator' => $paginator,
            'scores' => $scores,
            'count' => $count,
            'clinicId' => $clinicId
        ));
    }

    protected function getScoreService()
    {
        return $this->createService('CustomBundle:Score:ScoreService');
    }
}