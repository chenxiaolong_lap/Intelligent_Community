<?php

namespace CustomBundle\Controller;

use AppBundle\Common\ArrayToolkit;
use AppBundle\Common\Paginator;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class CommunityActivityController extends BaseController
{
    public function indexAction(Request $request, $type = '')
    {
        $community = $this->getCommunity();

        $conditions = $request->query->all();
        $conditions = array_merge($conditions, array('communityId' => $community['id']));

        if ($type == '消防演习') {
            $conditions['type'] = '消防演习';
        } elseif ($type == '社区活动') {
            $conditions['type'] = '社区活动';
        }

        if (!empty($conditions['filter'])) {
            if ($conditions['filter'] == 'noStart') {
                $conditions['startTime_GT'] = time();
            } elseif ($conditions['filter'] == 'underway') {
                $conditions['endTime_GE'] = time();
                $conditions['startTime_LE'] = time();
            } elseif ($conditions['filter'] == 'finish') {
                $conditions['endTime_LT'] = time();
            }
        }

        $paginator = new Paginator(
            $this->get('request'),
            $this->getCommunityActivityService()->countCommunityActivity($conditions),
            $this->setting('article.pageNums', 20)
        );

        $activities = $this->getCommunityActivityService()->searchCommunityActivity(
            $conditions,
            array('startTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render(
            '@Custom/community-activity/index.html.twig',
            array(
                'activities' => $activities,
                'paginator' => $paginator,
                'filter' => !empty($conditions['filter']) ? $conditions['filter'] : '',
                'targetType' => $type
            )
        );
    }

    public function postReplyAction(Request $request, $id, $postId)
    {
        $fields = $request->request->all();
        $fields['content'] = $this->autoParagraph($fields['content']);
        $fields['targetId'] = $id;
        $fields['targetType'] = 'community-activity';
        $fields['parentId'] = $postId;

        $post = $this->getThreadService()->createPost($fields);

        return $this->render(
            'thread/subpost-item.html.twig',
            array(
                'post' => $post,
                'author' => $this->getCurrentUser(),
                'service' => $this->getThreadService(),
            )
        );
    }

    protected function autoParagraph($text)
    {
        if (trim($text) !== '') {
            $text = htmlspecialchars($text, ENT_NOQUOTES, 'UTF-8');
            $text = preg_replace("/\n\n+/", "\n\n", str_replace(array("\r\n", "\r"), "\n", $text));
            $texts = preg_split('/\n\s*\n/', $text, -1, PREG_SPLIT_NO_EMPTY);
            $text = '';

            foreach ($texts as $txt) {
                $text .= '<p>'.nl2br(trim($txt, "\n"))."</p>\n";
            }

            $text = preg_replace('|<p>\s*</p>|', '', $text);
        }

        return $text;
    }

    public function defaultIndexBlockAction()
    {
        $community = $this->getCommunity();
        $conditions = array();
        $conditions['communityId'] = $community['id'];

        $conditions['startTime_GT'] = time();
        $noStartActivities = $this->getCommunityActivityService()->searchCommunityActivity(
            $conditions,
            array('startTime' => 'DESC'),
            0,
            7
        );
        unset($conditions['startTime_GT']);

        $conditions['endTime_GE'] = time();
        $conditions['startTime_LE'] = time();
        $underwayActivities = $this->getCommunityActivityService()->searchCommunityActivity(
            $conditions,
            array('startTime' => 'ASC'),
            0,
            7
        );
        unset($conditions['endTime_GE']);
        unset($conditions['startTime_LE']);

        $conditions['endTime_LT'] = time();
        $finishActivities = $this->getCommunityActivityService()->searchCommunityActivity(
            $conditions,
            array('endTime' => 'DESC'),
            0,
            7
        );

        return $this->render('@Custom/my/default/activity/block.html.twig', array(
            'noStartActivities' => $noStartActivities,
            'underActivities' => $underwayActivities,
            'finishActivities' => $finishActivities,
        ));
    }

    public function showAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();
        $activity = $this->getCommunityActivityService()->getCommunityActivity($id);

        $conditions = array(
            'targetId' => $id,
            'targetType' => 'community-activity',
            'parentId' => 0,
        );

        $postNum = $this->getThreadService()->searchPostsCount($conditions);
        $paginator = new Paginator(
            $request,
            $postNum,
            10
        );

        $posts = $this->getThreadService()->searchPosts(
            $conditions,
            array('createdTime' => 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($posts, 'userId'));


        if (!in_array($user['id'], $activity['participant'])) {
            $isSign = true;
        } else {
            $isSign = false;
        }

        $article = $activity;
        $article['body'] = $activity['content'];

        return $this->render('@Custom/my/activity/preview.html.twig', array(
            'activity' => $activity,
            'posts' => $posts,
            'users' => $users,
            'postNum' => $postNum,
            'paginator' => $paginator,
            'service' => $this->getThreadService(),
            'article' => $article,
            'isSign' => $isSign
        ));
    }

    public function postAction(Request $request, $id)
    {
        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $post['content'] = $fields['content'];
            $post['targetType'] = 'community-activity';
            $post['targetId'] = $id;

            $user = $this->getCurrentUser();

            if (!$user->isLogin()) {
                throw $this->createAccessDeniedException('用户没有登录,不能评论!');
            }

            $post = $this->getThreadService()->createPost($post);

            return $this->render(
                'thread/part/post-item.html.twig',
                array(
                    'post' => $post,
                    'author' => $user,
                    'service' => $this->getThreadService(),
//                    'postReplyUrl' => $this->generateUrl(
//                        'article_post_reply',
//                        array('articleId' => $id, 'postId' => $post['id'])
//                    ),
                )
            );
        }
    }

    public function signAction(Request $request, $activityId)
    {
        $user = $this->getCurrentUser();
        $activity = $this->getCommunityActivityService()->getCommunityActivity($activityId);

        if (in_array($user['id'], $activity['participant'])) {
            throw $this->createAccessDeniedException('你已经签过到了');
        }

        $this->getCommunityActivityService()->sign($user, $activityId);

        return $this->createJsonResponse(array(
            'status' => true
        ));
    }

    protected function getCommunityService()
    {
        return $this->createService('CustomBundle:Community:CommunityService');
    }

    protected function getCommunityActivityService()
    {
        return $this->createService('CustomBundle:CommunityActivity:CommunityActivityService');
    }

    protected function getThreadService()
    {
        return $this->getBiz()->service('Thread:ThreadService');
    }

    protected function getCommunity()
    {
        $user = $this->getCurrentUser();

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->getCommunityService()->getCommunityByRegionAndName(
            $userProfile['province'],
            $userProfile['city'],
            $userProfile['district'],
            $userProfile['community']
        );
    }
}