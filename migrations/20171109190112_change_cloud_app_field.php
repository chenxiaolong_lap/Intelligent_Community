<?php

use Phpmig\Migration\Migration;

class ChangeCloudAppField extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $biz = $this->getContainer();
        $connection = $biz['db'];
        if ($this->isFieldExist('cloud_app', 'type')) {
            $connection->exec("ALTER TABLE `cloud_app` change `type` `type` enum('core','plugin','theme') NOT NULL DEFAULT 'plugin' COMMENT '应用类型(core系统，plugin插件应用, theme主题应用)'");
        }
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }

    protected function isFieldExist($table, $filedName)
    {
        $biz = $this->getContainer();

        $sql = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $biz['db']->fetchAssoc($sql);

        return empty($result) ? false : true;
    }
}
