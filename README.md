##软件说明

* 智慧社区网站基于edusoho开源网站框架进行二次开发
* 智慧社区的所有解释权归本人所有
* 本项目不能作为商业应用
* 二次开发在src/CustomBundle下所有内容


EduSoho网络课堂
[![Build Status](https://travis-ci.org/edusoho/edusoho.svg?branch=master)](https://www.edusoho.com)
==============

EduSoho是杭州阔知网络科技有限公司推出的开源、免费的互联网教育产品，帮助企业、个人快速建立互联网教育网站。本开源软件简洁易用，定制性强，是开展互联网教学、销售、推广的最好的选择。

* EduSoho官网：[http://www.edusoho.com/](http://www.edusoho.com/)
* EduSoho演示站：[http://demo.edusoho.com](http://demo.edusoho.com)

## 安装使用

* 安装教程： [http://www.qiqiuyu.com/course/explore/install](http://www.qiqiuyu.com/course/explore/install)
* 官方QQ群：280645300
* **安装注意事项** ：在安装EduSoho时，请将您的程序运行根目录配置到web下，否则会有课程视频、资料泄漏等安全问题。即正确访问您的EduSoho的方式是： http://www.youdomain.com/ 。 如是 http://www.yourdomain.com/web/ ，请按照官网教程修改您的配置。

## 版权协议

请查阅[EduSoho开源协议V1.0版本](https://github.com/EduSoho/EduSoho/wiki/EduSoho%E5%BC%80%E6%BA%90%E5%8D%8F%E8%AE%AEV1.0%E7%89%88%E6%9C%AC)。

